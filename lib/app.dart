import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:fstore/common/config.dart';
import 'package:get_it/get_it.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

import 'app_init.dart';
import 'common/constants.dart';
import 'common/styles.dart';
import 'common/tools.dart';
import 'generated/l10n.dart';
import 'models/index.dart';
import 'route.dart';
import 'routes/route_observer.dart';
import 'services/index.dart';
import 'tabbar.dart';
import 'widgets/common/internet_connectivity.dart';
import 'widgets/firebase/firebase_analytics_wapper.dart';
import 'widgets/firebase/firebase_cloud_messaging_wapper.dart';
import 'widgets/firebase/one_signal_wapper.dart';
import 'package:http/http.dart' as http;


//START NEW by Luthfan ALwafi
GetIt locator = GetIt.instance;
//END NEW by Luthfan Alwafi

class App extends StatefulWidget {
  App();

  static final GlobalKey<NavigatorState> navigatorKey = GlobalKey();

  @override
  State<StatefulWidget> createState() {
    return AppState();
  }
}

class AppState extends State<App>
    implements FirebaseCloudMessagingDelegate, UserModelDelegate {
  final _app = AppModel();
  final _product = ProductModel();
  final _wishlist = WishListModel();
  final _shippingMethod = ShippingMethodModel();
  final _paymentMethod = PaymentMethodModel();
  final _advertisementModel = Ads();
  final _order = OrderModel();
  final _recent = RecentModel();
  final _blog = BlogModel();
  final _user = UserModel();
  final _filterModel = FilterAttributeModel();
  final _filterTagModel = FilterTagModel();
  final _categoryModel = CategoryModel();
  final _tagModel = TagModel();
  final _taxModel = TaxModel();
  final _pointModel = PointModel();

  // ---------- Vendor -------------
  StoreModel _storeModel;
  VendorShippingMethodModel _vendorShippingMethodModel;

  CartInject cartModel = CartInject();
  bool isFirstSeen = false;
  bool isLoggedIn = false;

  FirebaseAnalyticsAbs firebaseAnalyticsAbs;

  //START NEW by Luthfan Alwafi
  String domain = serverConfig['url'];
  final NavigationService _navigationService = locator<NavigationService>();
  FirebaseMessaging _firebaseMessaging;
  String _session;
  Map <String, dynamic> _dealToday, _minMax;
  int minPrice, maxPrice, minQty, maxQty, minCartPrice, maxCartPrice, maxItem;
  String minPriceMsg, maxPriceMsg, minQtyMsg, maxQtyMsg, minCartPriceMsg, maxCartPriceMsg, maxItemMsg;
  //END NEW by Luthfan Alwafi

  void checkInternetConnection() {
    if (kIsWeb || isMacOS || isWindow) {
      return;
    }
    MyConnectivity.instance.initialise();
    MyConnectivity.instance.myStream.listen((onData) {
      printLog("[App] internet issue change: $onData");
    });
  }

  @override
  void initState() {
    printLog("[AppState] initState");

    if (kIsWeb) {
      printLog("[AppState] init WEB");
      firebaseAnalyticsAbs = FirebaseAnalyticsWeb();
    } else {
      firebaseAnalyticsAbs = FirebaseAnalyticsWapper()..init();

      Future.delayed(
        const Duration(seconds: 1),
        () {
          printLog("[AppState] init mobile modules ..");
          checkInternetConnection();

          _user.delegate = this;

          if (isMobile) {
            FirebaseCloudMessagagingWapper()
              ..init()
              ..delegate = this;
          }

          // OneSignalWapper()..init();
          printLog("[AppState] register modules .. DONE");
        },
      );
    }

    super.initState();

    //START NEW by Luthfan Alwafi
    clear_cache();
    getSessionCode();
    // getMinMax();
    _firebaseMessaging = FirebaseMessaging();
    _firebaseMessaging.subscribeToTopic('all');
    getNotification();
    //END NEW by Luthfan Alwafi

  }

  void _saveMessage(Map<String, dynamic> message) {
    if (message.containsKey('data')) {
      _app.deeplink = message['data'];
    }

    FStoreNotification a = FStoreNotification.fromJsonFirebase(message);
    final id = message['notification'] != null
        ? message['notification']['tag']
        : message['data']['google.message_id'];

    a.saveToLocal(id);
  }

  @override
  void onLaunch(Map<String, dynamic> message) {
    printLog('[app.dart] onLaunch Pushnotification: $message');

    _saveMessage(message);
  }

  @override
  void onMessage(Map<String, dynamic> message) {
    printLog('[app.dart] onMessage Pushnotification: $message');

    _saveMessage(message);
  }

  @override
  void onResume(Map<String, dynamic> message) {
    printLog('[app.dart] onResume Pushnotification: $message');

    _saveMessage(message);
  }

  void updateDeviceToken(Users user) {
    FirebaseMessaging().getToken().then((token) async {
      try {
        await Services().updateUserInfo({"deviceToken": token}, user.cookie);
      } catch (e) {
        printLog(e);
      }
    });
  }

  @override
  onLoaded(Users user) {
    updateDeviceToken(user);
  }

  @override
  onLoggedIn(Users user) {
    updateDeviceToken(user);
  }

  @override
  onLogout(Users user) async {
    try {
      await Services().updateUserInfo({"deviceToken": ""}, user.cookie);
    } catch (e) {
      printLog(e);
    }
  }

  /// Build the App Theme
  ThemeData getTheme(context) {
    printLog("[AppState] build Theme");

    var appModel = Provider.of<AppModel>(context);
    var isDarkTheme = appModel.darkTheme ?? false;

    if (appModel.appConfig == null) {
      /// This case is loaded first time without config file
      return buildLightTheme(appModel.langCode);
    }

    if (isDarkTheme) {
      return buildDarkTheme(appModel.langCode).copyWith(
        primaryColor: HexColor(
          appModel.appConfig["Setting"]["MainColor"],
        ),
      );
    }
    return buildLightTheme(appModel.langCode).copyWith(
      primaryColor: HexColor(appModel.appConfig["Setting"]["MainColor"]),
    );
  }

  //START NEW by Luthfan Alwafi
  void clear_cache() async {
    var appDir = (await getTemporaryDirectory()).path;
    // if (appDir.existsSync()) {
    //   appDir.deleteSync(recursive: true);
    // };
    // await Directory(appDir).delete(recursive: true);
    try {
      await Directory(appDir).delete(recursive: true);
    }
    catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      printLog(e);
    }
  }

  void getSessionCode() async {
    final response = await http.get('$domain/wp-json/wcff/v2/fields/checkout');
    var _listData = jsonDecode(response.body);
    setState(() {
      _dealToday = _listData;
    });
    
    _session = _dealToday['wcccf_event_code']['placeholder'];
    // _session = 'WEB210218';
    store.set("session", _session);
  }

  void navigate (Map<String, dynamic> message) {
    var data = message['data'];
    var page = data['payload'];
    if (page != null) {
      _navigationService.navigateTo(page);
    }
    else {
      _navigationService.navigateTo('notify');
    }
  }

  void getMinMax() async {
    final response = await http.get(
        '$domain/wp-json/wcff/v1/minmax/message?section_id=wc_minmax_message_settings');
    var _listData = jsonDecode(response.body);
    setState(() {
      _minMax = _listData;
    });

    minPrice = _minMax['generalSettings']['min_price'];
    maxPrice = _minMax['generalSettings']['max_price'];
    minQty = _minMax['generalSettings']['min_quantity'];
    maxQty = _minMax['generalSettings']['max_quantity'];

    minPriceMsg =
        _minMax['generalSettings']['min_price_message'] + ' $minPrice';
    maxPriceMsg =
        _minMax['generalSettings']['max_price_message'] + ' $maxPrice';
    minQtyMsg = _minMax['generalSettings']['min_qty_message'] + ' $minQty';
    maxQtyMsg = _minMax['generalSettings']['max_qty_message'] + ' $maxQty';

    minCartPrice = _minMax['settingsMinMaxCart']['min_cart_total_price'];
    maxCartPrice = _minMax['settingsMinMaxCart']['max_cart_total_price'];

    minCartPriceMsg =
        _minMax['settingsMinMaxCart']['min_cart_message'] + ' $minCartPrice';
    maxCartPriceMsg =
        _minMax['settingsMinMaxCart']['max_cart_message'] + ' $maxCartPrice';

    maxItem = _minMax['maxQtyApps']['max_qty_apps'];

    maxItemMsg = _minMax['maxQtyApps']['max_qty_apps_message'];
    
    store.set("minPrice", minPrice);
    store.set("maxPrice", maxPrice);
    store.set("minQty", minQty);
    store.set("maxQty", maxQty);
    store.set("minPriceMsg", minPriceMsg);
    store.set("maxPriceMsg", maxPriceMsg);
    store.set("minQtyMsg", minQtyMsg);
    store.set("maxQtyMsg", maxQtyMsg);
    store.set("minCartPrice", minCartPrice);
    store.set("maxCartPrice", maxCartPrice);
    store.set("minCartPriceMsg", minCartPriceMsg);
    store.set("maxCartPriceMsg", maxCartPriceMsg);
    store.set("maxItem", maxItem);
    store.set('maxItemMsg', maxItemMsg);
  }

  void getNotification() async {
    if (Platform.isIOS) iOSPermission();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        // delegate?.onMessage(message);
        _saveMessage(message);
      },
      onResume: (Map<String, dynamic> message) async {
        // delegate?.onResume(message);
        _saveMessage(message);
        Future.delayed(const Duration(seconds: 1), () {
          navigate(message);
        });
      },
      onLaunch: (Map<String, dynamic> message) async {
        // delegate?.onLaunch(message);
        _saveMessage(message);
        Future.delayed(const Duration(seconds: 1), () {
          navigate(message);
        });
      },
    );
  }

  void iOSPermission() {
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true, provisional: false));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {});
  }
  //END NEW by Luthfan Alwafi

  @override
  Widget build(BuildContext context) {
    printLog("[AppState] build");
    return ChangeNotifierProvider<AppModel>(
      create: (context) => _app,
      child: Consumer<AppModel>(
        builder: (context, value, child) {
          if (value.vendorType == VendorType.multi &&
              _storeModel == null &&
              _vendorShippingMethodModel == null) {
            _storeModel = StoreModel();
            _vendorShippingMethodModel = VendorShippingMethodModel();
          }
          return MultiProvider(
            providers: [
              Provider<ProductModel>.value(value: _product),
              Provider<WishListModel>.value(value: _wishlist),
              Provider<ShippingMethodModel>.value(value: _shippingMethod),
              Provider<PaymentMethodModel>.value(value: _paymentMethod),
              Provider<OrderModel>.value(value: _order),
              Provider<RecentModel>.value(value: _recent),
              Provider<UserModel>.value(value: _user),
              ChangeNotifierProvider<FilterAttributeModel>(
                  create: (_) => _filterModel),
              ChangeNotifierProvider<FilterTagModel>(
                  create: (_) => _filterTagModel),
              ChangeNotifierProvider<CategoryModel>(
                  create: (_) => _categoryModel),
              ChangeNotifierProvider(create: (_) => _tagModel),
              ChangeNotifierProvider(create: (_) => cartModel.model),
              ChangeNotifierProvider(create: (_) => BlogModel()),
              ChangeNotifierProvider(create: (_) => _blog),
              ChangeNotifierProvider(create: (_) => _advertisementModel),
              Provider<TaxModel>.value(value: _taxModel),
              if (value.vendorType == VendorType.multi) ...[
                ChangeNotifierProvider<StoreModel>(create: (_) => _storeModel),
                Provider<VendorShippingMethodModel>.value(
                    value: _vendorShippingMethodModel),
              ],
              Provider<PointModel>.value(value: _pointModel),
            ],
            child: MaterialApp(
              debugShowCheckedModeBanner: false,
              navigatorKey: App.navigatorKey,
              locale: Locale(value.langCode, ""),
              navigatorObservers: [
                MyRouteObserver(),
                ...firebaseAnalyticsAbs.getMNavigatorObservers()
              ],
              localizationsDelegates: [
                S.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
                DefaultCupertinoLocalizations.delegate,
              ],
              supportedLocales: S.delegate.supportedLocales,
              home: Scaffold(
                body: AppInit(
                  onNext: () => MainTabs(),
                ),
              ),
              routes: Routes.getAll(),
              onGenerateRoute: Routes.getRouteGenerate,
              theme: getTheme(context),
              // navigatorKey: locator<NavigationService>().navigationKey,
            ),
          );
        },
      ),
    );
  }
}

//START NEW by Luthfan Alwafi
final GlobalState store = GlobalState.instance;

class GlobalState {
  final Map<dynamic, dynamic> _data = <dynamic, dynamic>{};

  static GlobalState instance = GlobalState._();
  GlobalState._();

  set(dynamic key, dynamic value) => _data[key] = value;
  get(dynamic key) => _data[key];
}

final GlobalKey<NavigatorState> _navigationKey = GlobalKey<NavigatorState>();

class NavigationService {
  
  GlobalKey<NavigatorState> get navigationKey => _navigationKey;

  void pop() {
    return _navigationKey.currentState.pop();
  }
  Future<dynamic> navigateTo(String routeName, {dynamic arguments}) {
    return _navigationKey.currentState
        .pushNamed(routeName, arguments: arguments);
  }
}
//END NEW by Luthfan Alwafi
