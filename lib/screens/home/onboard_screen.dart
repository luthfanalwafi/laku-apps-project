import 'dart:math';

import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:liquid_swipe/liquid_swipe.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../common/config.dart' as config;
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../generated/l10n.dart';


class OnBoardScreen extends StatefulWidget {
  final appConfig;

  OnBoardScreen(this.appConfig);

  @override
  _OnBoardScreenState createState() => _OnBoardScreenState();
}

class _OnBoardScreenState extends State<OnBoardScreen> 

//START NEW by Luthfan Alwafi
with SingleTickerProviderStateMixin {
//END NEW by Luthfan Alwafi

  final isRequiredLogin = config.kLoginSetting['IsRequiredLogin'];
  int page = 0;

  //START NEW By Luthfan Alwafi
  AnimationController controller;
  Animation titleAnimation, imgAnimation, descAnimation;
  //END NEW by Luthfan Alwafi

  @override
  void initState() {
    super.initState();


    controller = AnimationController(vsync: this, duration: const Duration(milliseconds: 5000));
    titleAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(controller);
    imgAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: controller, 
        curve: const Interval(0.3, 1.0, curve: Curves.ease),
        
      ),
    );
    descAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: controller, 
        curve: const Interval(0.6, 1.0, curve: Curves.ease),
      ),
    );
    controller.forward();
  }
    
  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }
  //END NEW By Luthfan Alwafi

  List<Slide> getSlides() {
    final List<Slide> slides = [];
    final data = widget.appConfig["OnBoarding"] != null
        ? widget.appConfig["OnBoarding"]["data"]
        : config.onBoardingData;

    // Widget loginWidget = Column(
    //   crossAxisAlignment: CrossAxisAlignment.stretch,
    //   children: <Widget>[
    //     Image.asset(
    //       kOrderCompleted,
    //       fit: BoxFit.fitWidth,
    //     ),
    //     Padding(
    //       padding: const EdgeInsets.only(top: 20),
    //       child: Row(
    //         mainAxisAlignment: MainAxisAlignment.center,
    //         children: <Widget>[
    //           GestureDetector(
    //             child: Text(
    //               S.of(context).signIn,
    //               style: const TextStyle(
    //                 color: kTeal400,
    //                 fontSize: 20.0,
    //               ),
    //             ),
    //             onTap: () async {
    //               SharedPreferences prefs =
    //                   await SharedPreferences.getInstance();
    //               await prefs.setBool('seen', true);
    //               await Navigator.pushNamed(context, RouteList.login);
    //             },
    //           ),
    //           const Text(
    //             "    |    ",
    //             style: TextStyle(color: kTeal400, fontSize: 20.0),
    //           ),
    //           GestureDetector(
    //             child: Text(
    //               S.of(context).signUp,
    //               style: const TextStyle(
    //                 color: kTeal400,
    //                 fontSize: 20.0,
    //               ),
    //             ),
    //             onTap: () async {
    //               SharedPreferences prefs =
    //                   await SharedPreferences.getInstance();
    //               await prefs.setBool('seen', true);
    //               await Navigator.pushNamed(context, RouteList.register);
    //             },
    //           ),
    //         ],
    //       ),
    //     ),
    //   ],
    // );

    //START NEW by Luthfan Alwafi
    Widget loginWidget = AnimatedBuilder(
      animation: titleAnimation,
      builder: (context, widget) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Opacity(
              opacity: titleAnimation.value,
              child: Container(
                alignment: Alignment.center,
                child: Text(
                  data[0]['title'],
                  style: textStyle.copyWith(
                    fontWeight: FontWeight.bold,
                    fontSize: 25.0,
                    color: Colors.white,
                    fontFamily: 'Bahnschrift'
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 30.0,
            ),
            Opacity(
              opacity: imgAnimation.value,
              child: Image.asset(
                kWelcome,
                height: 270.0,
              ),
            ),
            const SizedBox(
              height: 20.0,
            ),
            Container(
              padding: const EdgeInsets.only(left: 40.0, right: 30.0),
              child: Opacity(
                opacity: descAnimation.value,
                child: Text(
                  data[0]['desc'],
                  style: textStyle.copyWith(
                    color: Colors.white,
                    height: 1.5,
                    fontFamily: 'Bahnschrift'
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 30.0,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                    child: Text(
                      S.of(context).signIn,
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 20.0,
                      ),
                    ),
                    onTap: () async {
                      SharedPreferences prefs =
                          await SharedPreferences.getInstance();
                      await prefs.setBool('seen', true);
                      await Navigator.pushNamed(context, RouteList.login);
                    },
                  ),
                  const Text(
                    "    |    ",
                    style: TextStyle(color: Colors.white, fontSize: 20.0),
                  ),
                  GestureDetector(
                    child: Text(
                      S.of(context).signUp,
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 20.0,
                      ),
                    ),
                    onTap: () async {
                      SharedPreferences prefs =
                          await SharedPreferences.getInstance();
                      await prefs.setBool('seen', true);
                      await Navigator.pushNamed(context, RouteList.register);
                    },
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
    // END NEW By Luthfan Alwafi

    // for (int i = 0; i < data.length; i++) {
    //   Slide slide = Slide(
    //     marginTitle: const EdgeInsets.only(
    //       top: 125.0,
    //       bottom: 50.0,
    //     ),
    //     maxLineTextDescription: 2,
    //     styleTitle: const TextStyle(
    //       fontWeight: FontWeight.bold,
    //       fontSize: 25.0,
    //       color: kGrey900,
    //     ),
    //     backgroundColor: Colors.white,
    //     marginDescription: const EdgeInsets.fromLTRB(20.0, 75.0, 20.0, 0),
    //     styleDescription: const TextStyle(
    //       fontSize: 15.0,
    //       color: kGrey600,
    //     ),
    //     foregroundImageFit: BoxFit.fitWidth,
    //   );

    //   if (i == 2) {
    //     slide.centerWidget = loginWidget;
    //   } else {
    //     slide.pathImage = data[i]['image'];
    //   }
    //   slides.add(slide);
    // }

    //START NEW by Luthfan Alwafi
    for (int i = 0; i < data.length; i++) {
      Slide slide = Slide(
          marginTitle: const EdgeInsets.only(
            top: 10.0,
            bottom: 30.0,
          ),
          maxLineTextDescription: 9,
          styleTitle: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25.0,
            color: Colors.white,
          ),
          backgroundColor: kColorPrimary,
          marginDescription: const EdgeInsets.fromLTRB(20.0, 75.0, 20.0, 0),
          styleDescription: const TextStyle(
            fontSize: 15.0,
            color: Colors.white,
          ),
          heightImage: 300.0);

      if (i == 0) {
        slide.centerWidget = loginWidget;
      } else {
        slide.pathImage = data[i]['image'];
      }
      slides.add(slide);
    }
    //END NEW by Luthfan Alwafi

    return slides;
  }

  static const textStyle = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w400,
  );

  getPages(List data) {
    return [
      for (int i = 0; i < data.length; i++)
        Container(
          color: HexColor(data[i]['background']),
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Image.asset(
                data[i]['image'],
                fit: BoxFit.cover,
              ),
              const SizedBox(height: 10),
              Column(
                children: <Widget>[
                  Text(
                    data[i]['title'],
                    style: textStyle.copyWith(
                      fontSize: 25,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  const SizedBox(height: 20),
                  Text(
                    data[i]['desc'],
                    style: textStyle,
                  ),
                ],
              ),
              const SizedBox(height: 20),
            ],
          ),
        )
    ];
  }

  Widget _buildDot(int index) {
    double selectedness = Curves.easeOut.transform(
      max(
        0.0,
        1.0 - ((page ?? 0) - index).abs(),
      ),
    );
    double zoom = 1.0 + (2.0 - 1.0) * selectedness;
    return Container(
      width: 25.0,
      child: Center(
        child: Material(
          color: Colors.white,
          type: MaterialType.circle,
          child: Container(
            width: 8.0 * zoom,
            height: 8.0 * zoom,
          ),
        ),
      ),
    );
  }

  void pageChangeCallback(int lpage) {
    setState(() {
      page = lpage;
    });
  }

  @override
  Widget build(BuildContext context) {
    String boardType = widget.appConfig['OnBoarding'] != null
        ? widget.appConfig['OnBoarding']['layout']
        : null;

    switch (boardType) {
      case 'liquid':
        return MaterialApp(
          home: Scaffold(
            body: Stack(
              children: <Widget>[
                LiquidSwipe(
                  fullTransitionValue: 200,
                  enableSlideIcon: true,
                  enableLoop: true,
                  positionSlideIcon: 0.5,
                  onPageChangeCallback: pageChangeCallback,
                  waveType: WaveType.liquidReveal,
                  pages: getPages(widget.appConfig['OnBoarding']['data']),
                ),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    children: <Widget>[
                      const Expanded(child: SizedBox()),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: List<Widget>.generate(5, _buildDot),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      default:
        return IntroSlider(
          slides: getSlides(),
          styleNameSkipBtn: const TextStyle(color: kGrey900),
          styleNameDoneBtn: const TextStyle(color: kGrey900),
          namePrevBtn: S.of(context).prev,
          nameSkipBtn: S.of(context).skip,
          nameNextBtn: S.of(context).next,
          nameDoneBtn: isRequiredLogin ? '' : S.of(context).done,
          isShowDoneBtn: !isRequiredLogin,
          onDonePress: () async {
            if (isRequiredLogin) {
              return;
            }
            SharedPreferences prefs = await SharedPreferences.getInstance();
            await prefs.setBool('seen', true);
            await Navigator.pushReplacementNamed(context, RouteList.dashboard);
          },
        );
    }
  }
}
