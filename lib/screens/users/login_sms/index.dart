import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../generated/l10n.dart';
import '../../../models/app_model.dart';
import '../../../models/user_model.dart';
import '../../../services/service_config.dart';
import '../../../widgets/common/login_animation.dart';
import '../../../widgets/common/webview.dart';
import '../forgot_password.dart';
import '../registration.dart';


class LoginEmail extends StatefulWidget {
  final bool fromCart;

  //START NEW by Luthfan ALwafi
  final Function onLoginSuccess;

  LoginEmail({this.fromCart = false, this.onLoginSuccess});
  //END NEW by Luthfan Alwafi

  // LoginSMS({this.fromCart = false});

  @override
  _LoginEmailState createState() => _LoginEmailState();
}

class _LoginEmailState extends State<LoginEmail> with TickerProviderStateMixin {
  AnimationController _loginButtonController;
  // final TextEditingController _controller = TextEditingController(text: '');

  // CountryCode countryCode;
  // String phoneNumber;
  // String _phone;
  bool isLoading = false;

  // final StreamController<PhoneAuthCredential> _verifySuccessStream =
  //     StreamController<PhoneAuthCredential>.broadcast();

  //START NEW by Luthfan ALwafi
  var parentContext;
  final _auth = FirebaseAuth.instance;
  final TextEditingController username = TextEditingController();
  final TextEditingController password = TextEditingController();
  //END NEW by Luthfan Alwafi

  @override
  void initState() {
    super.initState();
    _loginButtonController = AnimationController(
      duration: const Duration(milliseconds: 3000),
      vsync: this,
    );

    // _phone = '';
    // countryCode = CountryCode(
    //   code: LoginSMSConstants.countryCodeDefault,
    //   dialCode: LoginSMSConstants.dialCodeDefault,
    //   name: LoginSMSConstants.nameDefault,
    // );
    // _controller.addListener(() {
    //   if (_controller.text != _phone && _controller.text != '') {
    //     _phone = _controller.text;
    //     onPhoneNumberChange(
    //       _phone,
    //       '${countryCode.dialCode}$_phone',
    //       countryCode.code,
    //     );
    //   }
    // });
  }

  @override
  void dispose() {
    _loginButtonController.dispose();
    super.dispose();
  }

  Future<Null> _playAnimation() async {
    try {
      setState(() {
        isLoading = true;
      });
      await _loginButtonController.forward();
    } on TickerCanceled {
      printLog('[_playAnimation] error');
    }
  }

  Future<Null> _stopAnimation() async {
    try {
      await _loginButtonController.reverse();
      setState(() {
        isLoading = false;
      });
    } on TickerCanceled {
      printLog('[_stopAnimation] error');
    }
  }

  void _failMessage(message, context) {
    /// Showing Error messageSnackBarDemo
    /// #MKNote
    /// banyak mantan karyawan
    /// Ability so close message
    final snackBar = SnackBar(
      content: Text('⚠️: $message'),
      duration: const Duration(seconds: 30),
      action: SnackBarAction(
        label: S.of(context).close,
        onPressed: () {
          // Some code to undo the change.
        },
      ),
    );

    Scaffold.of(context)
      // ignore: deprecated_member_use
      ..removeCurrentSnackBar()
      // ignore: deprecated_member_use
      ..showSnackBar(snackBar);
  }

  // void onPhoneNumberChange(
  //   String number,
  //   String internationalizedPhoneNumber,
  //   String isoCode,
  // ) {
  //   if (internationalizedPhoneNumber.isNotEmpty) {
  //     phoneNumber = internationalizedPhoneNumber;
  //   } else {
  //     phoneNumber = null;
  //   }
  // }

  // Future<void> _loginSMS(context) async {
  //   if (phoneNumber == null) {
  //     Tools.showSnackBar(Scaffold.of(context), S.of(context).pleaseInput);
  //   } else {
  //     await _playAnimation();
  //     final PhoneCodeAutoRetrievalTimeout autoRetrieve = (String verId) {
  //       _stopAnimation();
  //     };

  //     final PhoneCodeSent smsCodeSent = (String verId, [int forceCodeResend]) {
  //       _stopAnimation();

  //       Navigator.push(
  //         context,
  //         MaterialPageRoute(
  //           builder: (context) => VerifyCode(
  //             fromCart: widget.fromCart,
  //             verId: verId,
  //             phoneNumber: phoneNumber,
  //             verifySuccessStream: _verifySuccessStream.stream,
  //           ),
  //         ),
  //       );
  //     };

  //     final PhoneVerificationCompleted verifiedSuccess =
  //         _verifySuccessStream.add;

  //     final PhoneVerificationFailed veriFailed =
  //         (FirebaseAuthException exception) {
  //       _stopAnimation();
  //       _failMessage(exception.message, context);
  //     };

  //     await FirebaseAuth.instance.verifyPhoneNumber(
  //       phoneNumber: phoneNumber,
  //       codeAutoRetrievalTimeout: autoRetrieve,
  //       codeSent: smsCodeSent,
  //       timeout: const Duration(seconds: 120),
  //       verificationCompleted: verifiedSuccess,
  //       verificationFailed: veriFailed,
  //     );
  //   }
  // }

  //START NEW by Luthfan ALwafi
  Future launchForgetPassworddWebView(String url) async {
    await Navigator.push(
      context,
      MaterialPageRoute<void>(
        builder: (BuildContext context) =>
            WebView(url: url, title: S.of(context).resetPassword),
        fullscreenDialog: true,
      ),
    );
  }

  void launchForgetPasswordURL(String url) async {
    if (url != null) {
      /// show as webview
      await launchForgetPassworddWebView(url);
    } else {
      /// show as native
      await Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ForgotPassword()),
      );
    }
  }

  void _welcomeMessage(user, context) {
    if (widget.onLoginSuccess != null) {
      widget.onLoginSuccess(context);
    } else {
      if (widget.fromCart) {
        Navigator.of(context).pop(user);
      } else {
        if (user.name != null) {
          final snackBar = SnackBar(
            content: Text(S.of(context).welcome + ' ${user.name} !'),
          );
          Scaffold.of(context).showSnackBar(snackBar);
        }
        // if (kLayoutWeb) {
        //   Navigator.of(context).pushReplacementNamed('/home-screen');
        // } else {
        //   Navigator.of(context).pushReplacementNamed('/home');
        // }
        else {
          Navigator.of(context).pushReplacementNamed('/home');
        }
      }
    }
  }

  _login(context) async {
    if (username.text.isEmpty || password.text.isEmpty) {
      var snackBar = SnackBar(content: Text(S.of(context).pleaseInput));
      Scaffold.of(context).showSnackBar(snackBar);
    } else {
      await _playAnimation();
      await Provider.of<UserModel>(context, listen: false).login(
        username: username.text.trim(),
        password: password.text.trim(),
        success: (user) {
          _stopAnimation();
          _welcomeMessage(user, context);
          _auth
              .signInWithEmailAndPassword(
            email: username.text,
            password: username.text,
          )
              .catchError(
            (onError) {
              if (onError.code == 'ERROR_USER_NOT_FOUND') {
                _auth
                    .createUserWithEmailAndPassword(
                  email: username.text,
                  password: username.text,
                )
                    .then((_) {
                  _auth.signInWithEmailAndPassword(
                    email: username.text,
                    password: username.text,
                  );
                });
              }
            },
          );
        },
        fail: (message) {
          _stopAnimation();
          _failMessage(message, context);
        },
      );
    }
  }
  //END NEW by Luthfan Alwafi

  @override
  Widget build(BuildContext context) {

    //START NEW by Luthfan Alwafi
    parentContext = context;
    // final appModel = Provider.of<AppModel>(context);
    final screenSize = MediaQuery.of(context).size;
    String forgetPasswordUrl = Config().forgetPassword;
    //END NEW by Luthfan Alwafi

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            } else {
              Navigator.of(context).pushNamed(RouteList.home);
            }
          },
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        elevation: 0.0,
      ),
      body: SafeArea(
        child: Builder(
          // builder: (context) => Stack(
          //   children: [
          //     ListenableProvider.value(
          //       value: Provider.of<UserModel>(context),
          //       child: Consumer<UserModel>(
          //         builder: (context, model, child) {
          //           return SingleChildScrollView(
          //             padding: const EdgeInsets.symmetric(horizontal: 30.0),
          //             child: Column(
          //               children: <Widget>[
          //                 const SizedBox(height: 80.0),
          //                 Column(
          //                   children: <Widget>[
          //                     Row(
          //                       mainAxisAlignment: MainAxisAlignment.center,
          //                       children: <Widget>[
          //                         Container(
          //                           height: 40.0,
          //                           child: Image.asset(kLogo),
          //                         ),
          //                       ],
          //                     ),
          //                   ],
          //                 ),
          //                 const SizedBox(height: 120.0),
          //                 Row(
          //                   crossAxisAlignment: CrossAxisAlignment.center,
          //                   children: <Widget>[
          //                     CountryCodePicker(
          //                       onChanged: (country) {
          //                         setState(() {
          //                           countryCode = country;
          //                         });
          //                       },
          //                       // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
          //                       initialSelection: countryCode.code,
          //                       //Get the country information relevant to the initial selection
          //                       onInit: (code) {
          //                         countryCode = code;
          //                       },
          //                     ),
          //                     const SizedBox(width: 8.0),
          //                     Expanded(
          //                       child: TextField(
          //                         decoration: InputDecoration(
          //                             labelText: S.of(context).phone),
          //                         keyboardType: TextInputType.phone,
          //                         controller: _controller,
          //                       ),
          //                     )
          //                   ],
          //                 ),
          //                 const SizedBox(
          //                   height: 60.0,
          //                 ),
          //                 StaggerAnimation(
          //                   titleButton: S.of(context).sendSMSCode,
          //                   buttonController: _loginButtonController.view,
          //                   onTap: () {
          //                     if (!isLoading) {
          //                       _loginSMS(context);
          //                     }
          //                   },
          //                 ),
          //               ],
          //             ),
          //           );
          //         },
          //       ),
          //     ),
          //   ],
          // ),

          //START NEW by Luthfan ALwafi
          builder: (context) => GestureDetector(
            onTap: () => Utils.hideKeyboard(context),
            child: Stack(
              children: [
                ListenableProvider.value(
                  value: Provider.of<UserModel>(context),
                  child: Consumer<UserModel>(builder: (context, model, child) {
                    return SingleChildScrollView(
                      padding: const EdgeInsets.symmetric(horizontal: 30.0),
                      child: Container(
                        alignment: Alignment.center,
                        width: screenSize.width /
                            (2 / (screenSize.height / screenSize.width)),
                        constraints: const BoxConstraints(maxWidth: 700),
                        child: Column(
                          children: <Widget>[
                            const SizedBox(height: 80.0),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                        height: 100.0,
                                        child: Image.asset(
                                          Theme.of(context).backgroundColor ==
                                                  Colors.white
                                              ? kLogoImage
                                              : kLogoDark,
                                        )),
                                  ],
                                ),
                              ],
                            ),
                            const SizedBox(height: 80.0),
                            TextField(
                                controller: username,
                                decoration: InputDecoration(
                                  labelText: S.of(parentContext).username,
                                )),
                            const SizedBox(height: 12.0),
                            Stack(children: <Widget>[
                              TextField(
                                obscureText: true,
                                controller: password,
                                decoration: InputDecoration(
                                  labelText: S.of(parentContext).password,
                                ),
                              ),
                              Positioned(
                                right: 4,
                                left: null,
                                bottom: 20,
                                child: GestureDetector(
                                  child: Text(
                                    " " + S.of(context).reset,
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor),
                                  ),
                                  onTap: () {
                                    launchForgetPasswordURL(forgetPasswordUrl);
                                  },
                                ),
                              )
                            ]),
                            const SizedBox(
                              height: 50.0,
                            ),
                            StaggerAnimation(
                              titleButton: S.of(context).signInWithEmail,
                              buttonController: _loginButtonController.view,
                              onTap: () {
                                if (!isLoading) {
                                  _login(context);
                                }
                              },
                            ),
                            const SizedBox(
                              height: 30.0,
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(S.of(context).dontHaveAccount),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                RegistrationScreen(),
                                          ),
                                        );
                                      },
                                      child: Text(
                                        " ${S.of(context).signup}",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Theme.of(context).primaryColor,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    );
                  }),
                ),
              ],
            ),
          ),
          //END NEW by Luthfan Alwafi

        ),
      ),
    );
  }
}

class PrimaryColorOverride extends StatelessWidget {
  const PrimaryColorOverride({Key key, this.color, this.child})
      : super(key: key);

  final Color color;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Theme(
      child: child,
      data: Theme.of(context).copyWith(primaryColor: color),
    );
  }
}
