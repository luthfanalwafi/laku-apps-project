import 'package:flutter/material.dart';
import 'package:launch_review/launch_review.dart';
import 'package:provider/provider.dart';

import '../../common/config/general.dart';
import '../../generated/l10n.dart';
import '../../models/app_model.dart';


class UpdateVersion extends StatefulWidget {
  final bool canPop;

  UpdateVersion({this.canPop = true});

  @override
  State<StatefulWidget> createState() {
    return UpdateVersionState();
  }
}

class UpdateVersionState extends State<UpdateVersion> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final config = Provider.of<AppModel>(context).appConfig;
    String newestVer = config['AppsVersion']['version'];

    return Stack(
      children: [
        Scaffold(
          backgroundColor: Theme.of(context).backgroundColor,
          appBar: AppBar(
            elevation: 0.5,
            leading: widget.canPop
                ? IconButton(
                    icon: const Icon(
                      Icons.arrow_back_ios,
                      size: 22,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  )
                : Container(),
            title: Text(
              S.of(context).updateApp,
              style: TextStyle(color: Theme.of(context).accentColor),
            ),
            backgroundColor: Theme.of(context).backgroundColor,
          ),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: <Widget>[
                const SizedBox(height: 20),
                Image.asset(
                  Theme.of(context).backgroundColor == Colors.white
                      ? 'assets/images/logo_square.png'
                      : 'assets/images/logo_square_dark.png',
                  width: 120,
                  height: 170,
                ),
                Text(
                  S.of(context).currentlyVersion + ' is ' + kcurrentlyVer,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 14,
                    color: Theme.of(context).accentColor,
                  ),
                ),
                const SizedBox(height: 10),
                Text(
                  S.of(context).newestVersion + ' is ' + newestVer,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 14,
                    color: Theme.of(context).accentColor,
                  ),
                ),
                const SizedBox(height: 30),
                Row(
                  children: [
                    kcurrentlyVer == newestVer
                        ? Expanded(
                            child: Text(
                              S.of(context).uptodate,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Theme.of(context).accentColor,
                              ),
                            ),
                          )
                        : Expanded(
                            child: ButtonTheme(
                              height: 45,
                              child: RaisedButton(
                                child: Text(
                                  S.of(context).updateApp.toUpperCase(),
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                color: Theme.of(context).primaryColor,
                                textColor: Colors.white,
                                onPressed: () {
                                  LaunchReview.launch(
                                    writeReview: false,
                                    androidAppId: 'com.laku.kainprinting',
                                    iOSAppId: '1460654977',
                                  );
                                },
                              ),
                            ),
                          ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
