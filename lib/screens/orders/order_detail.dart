import 'dart:convert';

import 'package:country_pickers/country_pickers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../generated/l10n.dart';
import '../../models/index.dart'
    show AppModel, Order, OrderModel, OrderNote, Product, UserModel;
import '../../services/index.dart';


class OrderDetail extends StatefulWidget {
  final Order order;
  final VoidCallback onRefresh;

  OrderDetail({this.order, this.onRefresh});

  @override
  _OrderDetailState createState() => _OrderDetailState();
}

class _OrderDetailState extends State<OrderDetail> {
  final services = Services();
  String tracking;
  Order order;

  //START NEW by Luthfan Alwafi
  List<dynamic> _tracking = List(), _trackData = List();
  String _trackingNumber, trackingStatus;
  //END NEW by Luthfan Alwafi

  @override
  void initState() {
    super.initState();
    getTracking();
    order = widget.order;
  }

  void getTracking() async{
    // services.getAllTracking().then((onValue) {
    //   if (onValue != null && onValue.trackings != null) {
    //     for (var track in onValue.trackings) {
    //       if (track.orderId == order.number) {
    //         setState(() {
    //           tracking = track.trackingNumber;
    //         });
    //       }
    //     }
    //   }
    // });

    //START NEW by Luthfan Alwafi
    await services.getAllTracking().then((onValue) {
      if (onValue != null && onValue.trackings != null) {
        setState(() {
          tracking = order.id;
        });
      }
    });

    if (tracking == '20435') {
      tracking = '28230';
    }
    final response = await http.get('https://laku.com/wp-json/wcff/v2/tracks/waybills?order_id=' + tracking);
    if (response.body != '') {
      var _listData = jsonDecode(response.body);
      setState(() {
        _tracking = _listData;
      });

      _trackingNumber = _tracking[0]['awbNumber'];
      _trackData = _tracking[0]['connotes'][0]['tracks'];
      
      trackingStatus = _tracking[0]['status'];
      // if (trackingStatus == 'DELIVERED') {
      //   await services.updateOrder(order.id, status: "shipped");
      //   widget.onRefresh();
      // }

      if (_tracking[0]['status'].toUpperCase() == 'VOID') {
        if (order.status == 'failed') return;
        order.status = 'failed';
        await services.updateOrder(order.id, status: "failed");
        widget.onRefresh();
      }
    }
    //END NEW by Luthfan Alwafi

  }

  void cancelOrder() {
    Services().widget.cancelOrder(context, order).then((onValue) {
      setState(() {
        order = onValue;
      });
    });
  }

  void createRefund() {
    if (order.status == 'refunded') return;
    services.updateOrder(order.id, status: 'refunded').then((onValue) {
      setState(() {
        order = onValue;
      });
      Provider.of<OrderModel>(context, listen: false).getMyOrder(
          userModel: Provider.of<UserModel>(context, listen: false));
    });
  }

  @override
  Widget build(BuildContext context) {
    final userModel = Provider.of<UserModel>(context);
    final currencyRate = Provider.of<AppModel>(context).currencyRate;

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              size: 20,
              color: Theme.of(context).accentColor,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        title: Text(
          S.of(context).orderNo + " #${order.number}",
          style: TextStyle(color: Theme.of(context).accentColor),
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            for (var item in order.lineItems)
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 15),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(child: Text(item.name)),
                    const SizedBox(
                      width: 15,
                    ),
                    Text("x${item.quantity}"),
                    const SizedBox(width: 20),
                    Text(
                      Tools.getCurrencyFormatted(item.total, currencyRate),
                      style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontWeight: FontWeight.w600),
                    ),
                    const SizedBox(
                      width: 10.0,
                    ),
                    if (!kPaymentConfig['EnableShipping'] ||
                        !kPaymentConfig['EnableAddress'])
                      DownloadButton(item.productId)
                  ],
                ),
              ),
            Container(
              decoration:
                  BoxDecoration(color: Theme.of(context).primaryColorLight),
              padding: const EdgeInsets.all(15),
              margin: const EdgeInsets.symmetric(vertical: 10),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        S.of(context).subtotal,
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                      Text(
                        Tools.getCurrencyFormatted(
                            order.lineItems.fold(
                                0, (sum, e) => sum + double.parse(e.total)),
                            currencyRate),
                        style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontWeight: FontWeight.w600),
                      )
                    ],
                  ),
                  const SizedBox(height: 10),
                  (order.shippingMethodTitle != null &&
                          kPaymentConfig['EnableShipping'])
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              S.of(context).shippingMethod,
                              style: TextStyle(
                                  color: Theme.of(context).accentColor),
                            ),
                            Text(
                              order.shippingMethodTitle,
                              style: TextStyle(
                                color: Theme.of(context).accentColor,
                                fontWeight: FontWeight.w600,
                              ),
                            )
                          ],
                        )
                      : Container(),
                  const SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        S.of(context).totalTax,
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                      Text(
                        Tools.getCurrencyFormatted(
                            order.totalTax, currencyRate),
                        style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontWeight: FontWeight.w600,
                        ),
                      )
                    ],
                  ),
                  const SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        S.of(context).total,
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                      Text(
                        Tools.getCurrencyFormatted(order.total, currencyRate),
                        style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontWeight: FontWeight.w600,
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            // tracking != null ? const SizedBox(height: 20) : Container(),
            // tracking != null
            //     ? GestureDetector(
            //         child: Align(
            //           alignment: Alignment.topLeft,
            //           child: Row(
            //             children: <Widget>[
            //               Text("${S.of(context).trackingNumberIs} "),
            //               Text(
            //                 tracking,
            //                 style: TextStyle(
            //                     color: Theme.of(context).primaryColor),
            //               )
            //             ],
            //           ),
            //         ),
            //         onTap: () {
            //           return Navigator.push(
            //             context,
            //             MaterialPageRoute(
            //               builder: (context) => WebviewScaffold(
            //                 url: "${afterShip['tracking_url']}/$tracking",
            //                 appBar: AppBar(
            //                   leading: GestureDetector(
            //                     child: const Icon(Icons.arrow_back_ios),
            //                     onTap: () {
            //                       Navigator.of(context).pop();
            //                     },
            //                   ),
            //                   title: Text(S.of(context).trackingPage),
            //                 ),
            //               ),
            //             ),
            //           );
            //         },
            //       )
            //     : Container(),

            //START NEW by Luthfan Alwafi
            const SizedBox(height: 10),
            _trackingNumber != null
                ? const SizedBox(
                    height: 5.0,
                  )
                : Container(),
            _trackingNumber != null
                ? Row(
                    children: <Widget>[
                      Text("${S.of(context).trackingNumberIs} "),
                      Text(
                        _trackingNumber,
                        style: TextStyle(color: Theme.of(context).primaryColor),
                      )
                    ],
                  )
                : Container(),
            //END NEW by Luthfan Alwafi

            // Services().widget.renderOrderTimelineTracking(context, order),
            // const SizedBox(height: 20),

            //START NEW by Luthfan Alwafi
            Services().widget.renderOrderTimelineTracking(context, order, trackingStatus),
            _trackingNumber != null
              ? Container(
                  color: Theme.of(context).primaryColor.withOpacity(0.1),
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.centerLeft,
                  padding: const EdgeInsets.only(left : 20.0, right: 20.0, bottom: 30.0),
                )
              : Container(
                  height: 20,
                  color: Theme.of(context).primaryColor.withOpacity(0.1),
                ),
            _trackingNumber != null
              ? Container(
                  child: Container(
                    alignment: Alignment.centerLeft,
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.only(bottom: 20.0, left: 20.0, right: 20.0),
                    color: Theme.of(context).primaryColor.withOpacity(0.1),
                    child: Column(
                      children: <Widget>[
                        Container(
                          child : Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              S.of(context).trackingStatus,
                              style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18.0,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 15.0),
                        Column(
                          children: List.generate(
                            _trackData.length,
                            (index) => Container(
                              width: MediaQuery.of(context).size.width,
                              child: Container(
                                child: Card(
                                  elevation: 5.0,
                                  margin: const EdgeInsets.only(bottom: 15.0),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5.0)),
                                  child: Column(
                                    children: <Widget>[
                                      const SizedBox(height: 10.0),
                                      // Container(
                                      //   alignment: Alignment.centerLeft,
                                      //   padding: EdgeInsets.symmetric(
                                      //       horizontal: 15.0, vertical: 5.0),
                                      //   child: Text(
                                      //     (_trackData[index]['date']).contains('.') ?
                                      //     (_trackData[index]['date'])
                                      //         .substring(0, _trackData[index]['date'].indexOf('.'))
                                      //         .replaceAll('T', '  ')
                                      //     : (_trackData[index]['date'])
                                      //         .replaceAll('T', '  '),
                                      //     style: TextStyle(
                                      //       fontSize: 12.0,
                                      //     ),
                                      //   ),
                                      // ),
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 15.0, vertical: 5.0),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text(
                                              (_trackData[index]['date']).contains('.') ?
                                              DateFormat('dd MMM yyyy.hh:mm:ss').format(
                                              DateTime.parse(
                                              (_trackData[index]['date']).replaceAll('T', ' ')
                                              .substring(0, (_trackData[index]['date']).replaceAll('T', ' ').indexOf('.')))
                                              .add(const Duration(hours: 7)))
                                              .toString()
                                              .split('.')[0]
                                              : DateFormat('dd MMM yyyy.hh:mm:ss').format(
                                              DateTime.parse(
                                              (_trackData[index]['date']).replaceAll('T', ' '))
                                              .add(const Duration(hours: 7)))
                                              .toString()
                                              .split('.')[0],
                                              style: const TextStyle(
                                                fontSize: 12.0,
                                                fontWeight: FontWeight.w900,
                                              ),
                                            ),
                                            Text(
                                              (_trackData[index]['date']).contains('.') ?
                                              DateFormat('dd MMM yyyy.hh:mm:ss').format(
                                              DateTime.parse(
                                              (_trackData[index]['date']).replaceAll('T', ' ')
                                              .substring(0, (_trackData[index]['date']).replaceAll('T', ' ').indexOf('.')))
                                              .add(const Duration(hours: 7)))
                                              .toString()
                                              .split('.')[1]
                                              : DateFormat('dd MMM yyyy.hh:mm:ss').format(
                                              DateTime.parse(
                                              (_trackData[index]['date']).replaceAll('T', ' '))
                                              .add(const Duration(hours: 7)))
                                              .toString()
                                              .split('.')[1],
                                              style: const TextStyle(
                                                fontSize: 12.0,
                                                fontWeight: FontWeight.w900,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
                                        child: Text(
                                          _trackData[index]['status'],
                                          style: const TextStyle(
                                            height: 1.5,
                                          ),
                                        ),
                                      ),
                                      _trackData[index]['handover'] == null ? Container()
                                        : Column(
                                          children: <Widget>[
                                            Container(
                                              alignment: Alignment.centerLeft,
                                              padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 2.0),
                                              child: Text(
                                                'Name : ' + _trackData[index]['handover']['name'],
                                                style: const TextStyle(
                                                  fontSize: 12.0,
                                                ),
                                              ),
                                            ),
                                            Container(
                                              alignment: Alignment.centerLeft,
                                              padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 2.0),
                                              child: Text(
                                                'Relation : ' + _trackData[index]['handover']['relation'],
                                                style: const TextStyle(
                                                  fontSize: 12.0,
                                                ),
                                              ),
                                            ),
                                            Container(
                                              alignment: Alignment.centerLeft,
                                              padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 2.0),
                                              child: Text(
                                                'Note : ' + _trackData[index]['handover']['note'],
                                                style: const TextStyle(
                                                  fontSize: 12.0,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      const Divider(
                                        thickness: 2.0,
                                      ),
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 15.0, vertical: 5.0),
                                        child: Text(
                                          _trackData[index]['branch'] + ', ' +
                                              _trackData[index]['location'],
                                          style: const TextStyle(
                                              fontWeight: FontWeight.w900),
                                        ),
                                      ),
                                      const SizedBox(height: 10.0),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ).toList(),
                        ),
                      ],
                    ),
                  ),
                )
              : Container(),
            //END NEW by Luthfan Alwafi

            /// Render the Cancel and Refund
            if (kPaymentConfig['EnableRefundCancel'])
              Services().widget.renderButtons(order, cancelOrder, createRefund),

            const SizedBox(height: 20),

            if (order.billing != null) ...[
              Text(S.of(context).shippingAddress,
                  style: const TextStyle(
                      fontSize: 18, fontWeight: FontWeight.bold)),
              const SizedBox(height: 10),
              Text(
                ((order.billing.apartment?.isEmpty ?? true)
                        ? ''
                        : '${order.billing.apartment} ') +
                    ((order.billing.block?.isEmpty ?? true)
                        ? ''
                        : '${(order.billing.apartment?.isEmpty ?? true) ? '' : '- '} ${order.billing.block}, ') +
                    order.billing.street +
                    ", " +
                    order.billing.city +
                    ", " +
                    getCountryName(order.billing.country),
              ),
            ],
            if (order.status == "processing" &&
                kPaymentConfig['EnableRefundCancel'])
              Column(
                children: <Widget>[
                  const SizedBox(height: 30),
                  Row(
                    children: [
                      Expanded(
                        child: ButtonTheme(
                          height: 45,
                          child: RaisedButton(
                              textColor: Colors.white,
                              color: HexColor("#056C99"),
                              onPressed: refundOrder,
                              child: Text(
                                  S.of(context).refundRequest.toUpperCase(),
                                  style: const TextStyle(
                                      fontWeight: FontWeight.w700))),
                        ),
                      )
                    ],
                  ),
                  const SizedBox(height: 10),
                ],
              ),
            const SizedBox(
              height: 20,
            ),
            FutureBuilder<List<OrderNote>>(
              future: services.getOrderNote(
                  userModel: userModel, orderId: order.id),
              builder: (context, snapshot) {
                if (!snapshot.hasData) return Container();
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      S.of(context).orderNotes,
                      style: const TextStyle(
                          fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(height: 10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: List.generate(
                        snapshot.data.length,
                        (index) {
                          return Padding(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                CustomPaint(
                                  painter: BoxComment(
                                      color: Theme.of(context).primaryColor),
                                  child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 10,
                                          right: 10,
                                          top: 15,
                                          bottom: 25),
                                      child: HtmlWidget(
                                        snapshot.data[index].note,
                                        textStyle: const TextStyle(
                                            color: Colors.white,
                                            fontSize: 13,
                                            height: 1.2),
                                      ),
                                    ),
                                  ),
                                ),
                                Text(
                                  formatTime(DateTime.parse(
                                      snapshot.data[index].dateCreated)),
                                  style: const TextStyle(fontSize: 13),
                                )
                              ],
                            ),
                            padding: const EdgeInsets.only(bottom: 15),
                          );
                        },
                      ),
                    ),
                  ],
                );
              },
            ),

            const SizedBox(height: 50)
          ],
        ),
      ),
    );
  }

  String getCountryName(country) {
    try {
      return CountryPickerUtils.getCountryByIsoCode(country).name;
    } catch (err) {
      return country;
    }
  }

  Future<void> refundOrder() async {
    _showLoading();
    try {
      await services.updateOrder(order.id, status: "refunded");
      _hideLoading();
      widget.onRefresh();
      Navigator.of(context).pop();
    } catch (err) {
      _hideLoading();

      Tools.showSnackBar(Scaffold.of(context), err.toString());
    }
  }

  void _showLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Center(
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white30,
                borderRadius: BorderRadius.circular(5.0)),
            padding: const EdgeInsets.all(50.0),
            child: kLoadingWidget(context),
          ),
        );
      },
    );
  }

  void _hideLoading() {
    Navigator.of(context).pop();
  }

  String formatTime(DateTime time) {
    return "${time.day}/${time.month}/${time.year}";
  }
}

class BoxComment extends CustomPainter {
  final Color color;

  BoxComment({this.color});

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();

    paint.color = color;
    var path = Path();
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height - 10);
    path.lineTo(30, size.height - 10);
    path.lineTo(20, size.height);
    path.lineTo(20, size.height - 10);
    path.lineTo(0, size.height - 10);
    path.close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

class DownloadButton extends StatefulWidget {
  final String id;

  DownloadButton(this.id);

  @override
  _DownloadButtonState createState() => _DownloadButtonState();
}

class _DownloadButtonState extends State<DownloadButton> {
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    final services = Services();
    return InkWell(
      onTap: () async {
        setState(() {
          isLoading = true;
        });

        Product product = await services.getProduct(widget.id);
        setState(() {
          isLoading = false;
        });
        await Share.share(product.files[0]);
      },
      child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 8,
          vertical: 4,
        ),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor.withOpacity(0.2),
          borderRadius: BorderRadius.circular(3),
        ),
        child: Row(
          children: <Widget>[
            isLoading
                ? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      width: 15.0,
                      height: 15.0,
                      child: Center(
                        child: kLoadingWidget(context),
                      ),
                    ),
                  )
                : Icon(
                    Icons.file_download,
                    color: Theme.of(context).primaryColor,
                  ),
            Text(
              S.of(context).download,
              style: TextStyle(
                color: Theme.of(context).primaryColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
