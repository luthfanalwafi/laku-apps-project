import 'dart:convert';
import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../generated/l10n.dart';
import '../../models/index.dart' show AppModel, Order, OrderModel, UserModel;
import '../../screens/base.dart';
import '../../services/index.dart';
import '../users/login.dart';
import 'order_detail.dart';


class MyOrders extends StatefulWidget {
  @override
  _MyOrdersState createState() => _MyOrdersState();
}

class _MyOrdersState extends BaseScreen<MyOrders> {
  final RefreshController _refreshController = RefreshController();

  //START NEW By Luthfan Alwafi
  String url = serverConfig["url"];
  String paymentConfirmURL = serverConfig["url"] + '/confirmation/';
  String paymentMethodURL =
      serverConfig["url"] + '/metode-pembayaran-yang-tersedia/';
  List<Map<String, Object>> lineItems = [];
  //END NEW By Luthfan Alwafi

  @override
  void afterFirstLayout(BuildContext context) {
    // refreshMyOrders();

    //START NEW by Luthfan Alwafi
    if (context != null) {
      refreshMyOrders();
      getPayConfUrl();
    }
    //END NEW by Luthfan Alwafi
  }

  Future<void> _onRefresh() async {
    await Provider.of<OrderModel>(context, listen: false)
        .getMyOrder(userModel: Provider.of<UserModel>(context, listen: false));
    await Future.delayed(const Duration(milliseconds: 1000));
    _refreshController.refreshCompleted();
  }

  Future<void> _onLoading() async {
    await Provider.of<OrderModel>(context, listen: false)
        .loadMore(userModel: Provider.of<UserModel>(context, listen: false));
    await Future.delayed(const Duration(milliseconds: 1000));
    _refreshController.loadComplete();
  }

  @override
  Widget build(BuildContext context) {
    bool isLoggedIn = Provider.of<UserModel>(context).loggedIn;

    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          // leading: IconButton(
          //     icon: const Icon(Icons.arrow_back_ios, size: 20),
          //     onPressed: () {
          //       Navigator.of(context).pop();
          //     }),
          title: Text(
            S.of(context).orderHistory,
            style: TextStyle(color: Theme.of(context).accentColor),
          ),
          backgroundColor: Theme.of(context).backgroundColor,
          elevation: 0.0,
        ),
        body: ListenableProvider.value(
            value: Provider.of<OrderModel>(context),
            child: Consumer<OrderModel>(builder: (context, model, child) {

              //START NEW By Luthfan Alwafi
              if (model.errMsg != null) {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        kEmptyOrder,
                        height: 200.0,
                      ),
                      Text(
                        S.of(context).noOrders,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(height: 10.0),
                      Text(
                        S.of(context).loginFirst,
                      ),
                      const SizedBox(height: 20.0),
                      Container(
                        width: 220.0,
                        child: RaisedButton(
                          color: Theme.of(context).primaryColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            child: Text(
                              S.of(context).login.toUpperCase(),
                              style: const TextStyle(
                                fontSize: 18.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => LoginScreen(),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                );
              }
              //END NEW By Luthfan Alwafi

              if (model.myOrders == null) {
                return Center(
                  child: kLoadingWidget(context),
                );
              }
              if (!isLoggedIn) {
                final LocalStorage storage = LocalStorage('data_order');
                var orders = storage.getItem('orders');
                var listOrder = [];
                // for (var i in orders) {
                //   listOrder.add(Order.fromStrapiJson(i));
                // }
                for (var i in orders) {
                  listOrder.add(Order.fromJson(i));
                }
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 15, horizontal: 15),
                      child: Text("${listOrder.length} ${S.of(context).items}"),
                    ),
                    Expanded(
                      child: ListView.builder(
                          padding: const EdgeInsets.symmetric(horizontal: 15),
                          itemCount: listOrder.length,
                          itemBuilder: (context, index) {
                            return OrderItem(
                              order: listOrder[listOrder.length - index - 1],
                              onRefresh: () {},
                            );
                          }),
                    )
                  ],
                );
              }

              if (model.myOrders != null && model.myOrders.isEmpty) {
                // return Center(child: Text(S.of(context).noOrders));

                //START NEW By Luthfan Alwafi
                return Center(
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          kEmptyOrder,
                          height: 200.0,
                          // fit: BoxFit.cover,
                        ),
                        Text(
                          S.of(context).noOrders,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
                //END NEW By Luthfan Alwafi

              }

              return Stack(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(

                        //START NEW by Luthfan Alwafi
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        //END NEW by Luthfan Alwafi

                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 15, horizontal: 15),
                            child: Text(
                                "${model.myOrders.length} ${S.of(context).items}"),
                          ),

                          //START NEW by Luthfan Alwafi
                          Row(
                            children: <Widget>[
                              Container(
                                child: InkWell(
                                  child: Text(
                                    S.of(context).paymentMethod,
                                    style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize:
                                          MediaQuery.of(context).size.width * 0.03,
                                    ),
                                  ),
                                  onTap: () => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => WebviewScaffold(
                                        url: paymentMethodURL,
                                        withJavascript: true,
                                        appCacheEnabled: true,
                                        appBar: AppBar(
                                          leading: IconButton(
                                            icon: const Icon(Icons.arrow_back),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                          title: Text(
                                            S.of(context).paymentMethod,
                                            style: TextStyle(
                                              color: Theme.of(context).accentColor,
                                            ),
                                          ),
                                          backgroundColor:
                                              Theme.of(context).backgroundColor,
                                          elevation: 0.0,
                                        ),
                                        withZoom: true,
                                        withLocalStorage: true,
                                        hidden: true,
                                        initialChild: Container(
                                            child: kLoadingWidget(context)),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 15.0),
                                child: InkWell(
                                  child: Text(
                                    S.of(context).paymentConfirmation,
                                    style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize:
                                          MediaQuery.of(context).size.width * 0.03,
                                    ),
                                  ),
                                  // onTap:  () =>  launch(paymentConfirmURL),
                                  onTap: () => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => WebviewScaffold(
                                        url: paymentConfirmURL,
                                        withJavascript: true,
                                        appCacheEnabled: true,
                                        appBar: AppBar(
                                          leading: IconButton(
                                            icon: const Icon(Icons.arrow_back),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                          title: Text(
                                            S.of(context).paymentConfirmation,
                                            style: TextStyle(
                                              color: Theme.of(context).accentColor,
                                            ),
                                          ),
                                          backgroundColor:
                                              Theme.of(context).backgroundColor,
                                          elevation: 0.0,
                                        ),
                                        withZoom: true,
                                        withLocalStorage: true,
                                        hidden: true,
                                        initialChild: Container(
                                            child: kLoadingWidget(context)),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          //END NEW by Luthfan Alwafi
                          
                        ],
                      ),
                      Expanded(
                        child: SmartRefresher(
                          enablePullDown: true,
                          enablePullUp: !model.endPage,
                          onRefresh: _onRefresh,
                          onLoading: _onLoading,
                          header: const WaterDropHeader(),
                          footer: kCustomFooter(context),
                          controller: _refreshController,
                          child: ListView.builder(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 15),
                              itemCount: model.myOrders.length,
                              itemBuilder: (context, index) {
                                return OrderItem(
                                  order: model.myOrders[index],
                                  onRefresh: refreshMyOrders,
                                );
                              }),
                        ),
                      )
                    ],
                  ),
                  model.isLoading
                      ? Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          color: Colors.black.withOpacity(0.2),
                          child: Center(
                            child: kLoadingWidget(context),
                          ),
                        )
                      : Container()
                ],
              );
            })));
  }

  // void refreshMyOrders() {
  //   Provider.of<OrderModel>(context, listen: false)
  //       .getMyOrder(userModel: Provider.of<UserModel>(context, listen: false));
  // }

  //START NEW by Luthfan Alwafi
  void refreshMyOrders() async {
    await Provider.of<OrderModel>(context, listen: false)
        .getMyOrder(userModel: Provider.of<UserModel>(context, listen: false));
  }
  
  void getPayConfUrl() async {
    // final cartModel = Provider.of<CartModel>(context, listen: false);
    final userModel = Provider.of<UserModel>(context, listen: false);
    // var params = Order().toJson(cartModel, userModel.user != null ? userModel.user.id : null, true);
    var params = {
      "line_items": lineItems,
      "customer_id": userModel.user != null ? userModel.user.id : null,
      "token": userModel.user != null ? userModel.user.cookie : null,
    };
    try {
      var str = convert.jsonEncode(params);
      var bytes = convert.utf8.encode(str);
      var base64Str = convert.base64.encode(bytes);

      final http.Response response =
          await http.post("$url/wp-json/api/flutter_user/checkout",
              body: convert.jsonEncode({
                "order": base64Str,
              }));
      var body = convert.jsonDecode(response.body);
      if (response.statusCode == 200 && body is String) {
        paymentConfirmURL = paymentConfirmURL + "?code=$body&mobile=true";
        print(paymentConfirmURL);
      } else {
        var message = body["message"];
        throw Exception(
            message != null ? message : "Can't save the order to website");
      }
    } catch (err) {
      rethrow;
    }
  }
  //END NEW by Luthfan Alwafi

}

class OrderItem extends StatefulWidget {
  final Order order;
  final VoidCallback onRefresh;

  OrderItem({this.order, this.onRefresh});

  @override
  _OrderItemState createState() => _OrderItemState();
}

class _OrderItemState extends State<OrderItem> {

  //START NEW by Luthfan Alwafi
  final services = Services();
  String trackingNumber, trackingStatus;
  List<dynamic> _tracking = List();

  @override
  void initState() {
    super.initState();
    getTracking();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void getTracking() async {
    trackingNumber = widget.order.id;
    final response = await http.get(
        'https://laku.com/wp-json/wcff/v2/tracks/waybills?order_id=' +
            trackingNumber);
    if (response.body != '') {
      var _listData = jsonDecode(response.body);
      setState(() {
        _tracking = _listData;
      });

      

      // if (_tracking[0]['status'].toUpperCase() == 'DELIVERED') {
      //   if (widget.order.status == 'shipped') return;
      //   widget.order.status = 'shipped';
      //   await services.updateOrder(widget.order.id, status: "shipped");
      // }

      trackingStatus = _tracking[0]['status'];

      if (_tracking[0]['status'].toUpperCase() == 'VOID') {
        if (widget.order.status == 'failed') return;
        widget.order.status = 'failed';
        await services.updateOrder(widget.order.id, status: "failed");
      }
    }
  }
  //END NEW by Luthfan Alwafi
  
  @override
  Widget build(BuildContext context) {
    final currencyRate = Provider.of<AppModel>(context).currencyRate;
    return Column(
      children: <Widget>[
        InkWell(
          onTap: () {
            if (widget.order.statusUrl != null) {
              launch(widget.order.statusUrl);
            } else {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => OrderDetail(
                          order: widget.order,
                          onRefresh: widget.onRefresh,
                        )),
              );
            }
          },
          child: Container(
            height: 40,
            padding: const EdgeInsets.only(left: 10),
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColorLight,
                borderRadius: BorderRadius.circular(3)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text("#${widget.order.number}",
                    style: TextStyle(
                        color: Theme.of(context).accentColor,
                        fontWeight: FontWeight.bold)),
                const Icon(Icons.arrow_right),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(S.of(context).orderDate),
              Text(
                // DateFormat("dd/MM/yyyy").format(widget.order.createdAt),

                //START NEW by Luthfan Alwafi
                DateFormat("dd MMM yyyy").format(widget.order.createdAt),
                //END NEW by Luthfan Alwafi

                style: const TextStyle(fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        if (widget.order.status != null)
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(S.of(context).status),
                Text(
                  // widget.order.status.toUpperCase(),

                  //START NEW by Luthfan Alwafi
                  trackingStatus == 'DELIVERED'
                    ? 'COMPLETED'
                    : widget.order.status.toUpperCase() == 'COMPLETED'
                    ? 'ON DELIVERY'
                    : widget.order.status.toUpperCase(),
                  //END NEW by Luthfan Alwafi

                  style: TextStyle(
                    
                      //START NEW by Luthfan Alwafi
                      color: trackingStatus == 'DELIVERED'
                        ? HexColor(kOrderStatusColor['shipped'])
                      //END NEW by Luthfan Alwafi

                        : kOrderStatusColor[widget.order.status] != null
                        ? HexColor(kOrderStatusColor[widget.order.status])
                        : Theme.of(context).accentColor,
                      fontWeight: FontWeight.bold),
                )
              ],
            ),
          ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(S.of(context).paymentMethod),
              const SizedBox(
                width: 15,
              ),
              Expanded(
                  child: Text(
                widget.order.paymentMethodTitle,
                style: const TextStyle(fontWeight: FontWeight.bold),
                textAlign: TextAlign.right,
              ))
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(S.of(context).total),
              Text(
                Tools.getCurrencyFormatted(widget.order.total, currencyRate),
                style: const TextStyle(fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        const SizedBox(height: 20)
      ],
    );
  }
}
