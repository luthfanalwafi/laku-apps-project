import 'dart:async';
import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:inspireui/widgets/flash_dialog/flash.dart';
import 'package:provider/provider.dart';

import '../../app.dart';
import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../generated/l10n.dart';
import '../../models/index.dart' show AppModel, CartModel, Product, UserModel;
import '../../services/index.dart';
import '../../tabbar.dart';
import '../../widgets/product/cart_item.dart';
import '../../widgets/product/product_bottom_sheet.dart';
import '../users/login.dart';
import 'cart_sumary.dart';
import 'empty_cart.dart';
import 'wishlist.dart';


class MyCart extends StatefulWidget {
  final PageController controller;
  final bool isModal;
  final bool isBuyNow;

  MyCart({this.controller, this.isModal, this.isBuyNow = false});

  @override
  _MyCartState createState() => _MyCartState();
}

class _MyCartState extends State<MyCart> with SingleTickerProviderStateMixin {
  bool isLoading = false;
  String errMsg = '';

  //START NEW by Luthfan Alwafi
  String _session;
  Map<String, dynamic> _dealToday;
  int minPrice, maxPrice, minQty, maxQty, minCartPrice, maxCartPrice;
  String minPriceMsg,
      maxPriceMsg,
      minQtyMsg,
      maxQtyMsg,
      minCartPriceMsg,
      maxCartPriceMsg;
  bool containFree = false;
  //END NEW by Luthfan Alwafi

  // List<Widget> _createShoppingCartRows(CartModel model, BuildContext context) {
  //   return model.productsInCart.keys.map(
  //     (key) {
  //       String productId = Product.cleanProductID(key);
  //       Product product = model.getProductById(productId);

  //       return ShoppingCartRow(
  //         product: product,
  //         variation: model.getProductVariationById(key),
  //         quantity: model.productsInCart[key],
  //         options: model.productsMetaDataInCart[key],
  //         onRemove: () => model.removeItemFromCart(key),
  //         onChangeQuantity: (val) {
  //           String message = Provider.of<CartModel>(context, listen: false)
  //               .updateQuantity(product, key, val, context: context);
  //           if (message.isNotEmpty) {
  //             final snackBar = SnackBar(
  //               content: Text(message),
  //               duration: const Duration(seconds: 1),
  //             );
  //             Future.delayed(
  //                 const Duration(milliseconds: 300),
  //                 // ignore: deprecated_member_use
  //                 () => Scaffold.of(context).showSnackBar(snackBar));
  //           }
  //         },
  //       );
  //     },
  //   ).toList();
  // }

  //START NEW by Luthfan Alwafi
  List<Widget> _createShoppingCartRows(CartModel model, BuildContext context) {

    containFree = model.item.toString().endsWith('FREEFREEFREEFREE');
    List<dynamic> productInCart = model.item.keys.map((key) {
      String productId = Product.cleanProductID(key);
      return productId;
    }).toList();
    for (int i = 0; i < productInCart.length; i ++) {
      containFree = productInCart[i].endsWith('FREEFREEFREEFREE') ? true : containFree;
    }
    
    return model.productsInCart.keys.map(
      (key) {
        String productId = Product.cleanProductID(key);
        Product product = model.getProductById(productId);

        return ShoppingCartRow(
          product: product,
          variation: model.getProductVariationById(key),
          quantity: model.productsInCart[key],
          options: model.productsMetaDataInCart[key],
          onRemove: () {
            if (!containFree || productId.endsWith('FREEFREEFREEFREE')) {
              model.removeItemFromCart(key);
            } else {
              showDialog(
                context: context,
                barrierDismissible: false,
                builder: (BuildContext context) {
                  AlertDialog alert = AlertDialog(
                    title: Text(S.of(context).info),
                    content: Text(
                      S.of(context).removeWarning
                      + '\n'
                      + S.of(context).areYouSure,
                      style: const TextStyle(
                        height: 1.5,
                      ),
                    ),
                    actions: <Widget>[
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          S.of(context).no,
                          style: const TextStyle(
                            color: kColorSecondary,
                          ),
                        ),
                      ),
                      FlatButton(
                        onPressed: () {
                          for (int i = 0; i < productInCart.length; i++) {
                            if (productInCart[i].endsWith('FREEFREEFREEFREE')) {
                              model.removeItemFromCart(productInCart[i]);
                            }
                          }
                          model.removeItemFromCart(key);
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          S.of(context).yes,
                          style: const TextStyle(
                            color: kColorSecondary,
                          ),
                        ),
                      ),
                    ],
                  );
                  return alert;
                },
              );
            }
          },
          onChangeQuantity: (val) {
            String message = Provider.of<CartModel>(context, listen: false)
                .updateQuantity(product, key, val, context: context);
            if (message.isNotEmpty) {
              final snackBar = SnackBar(
                content: Text(message),
                duration: const Duration(seconds: 1),
              );
              Future.delayed(
                  const Duration(milliseconds: 300),
                  // ignore: deprecated_member_use
                  () => Scaffold.of(context).showSnackBar(snackBar));
            }
          },
          token: productId,
        );
      },
    ).toList();
  }
  //END NEW by Luthfan Alwafi

  //START NEW by Luthfan Alwafi
  void getSessionCode() async {
    final respose =
        await http.get('https://laku.com/wp-json/wcff/v2/fields/checkout');
    var _listData = jsonDecode(respose.body);
    setState(() {
      _dealToday = _listData;
    });

    _session = _dealToday['wcccf_event_code']['placeholder'];
    // _session = 'APP200731';
    store.set("session", _session);
  }
  //END NEW by Luthfan Alwafi

  _loginWithResult(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => LoginScreen(
          fromCart: true,
        ),
        fullscreenDialog: kIsWeb,
      ),
    );

    if (result != null && result.name != null) {
      Tools.showSnackBar(
          Scaffold.of(context), S.of(context).welcome + " ${result.name} !");

      setState(() {});
    }
  }

  //START NEW by Luthfan Alwafi
  @override
  void initState() {
    super.initState();
    getSessionCode();
  }

  @override
  void dispose() {
    FlashHelper.dispose();
    super.dispose();
  }
  //END NEW by Luthfan Alwafi

  @override
  Widget build(BuildContext context) {
    printLog("[Cart] build");

    final localTheme = Theme.of(context);
    final screenSize = MediaQuery.of(context).size;
    var productDetail =
        Provider.of<AppModel>(context).appConfig['Setting']['ProductDetail'];
    var layoutType =
        productDetail ?? (kProductDetail['layout'] ?? 'simpleType');
    CartModel cartModel = Provider.of<CartModel>(context);

    //START NEW by Luthfan Alwafi
    final config = Provider.of<AppModel>(context).appConfig;
    final event = config['event'];
    //END NEW by Luthfan Alwafi

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      // floatingActionButton: FloatingActionButton.extended(
      //   // onPressed: cartModel.calculatingDiscount ? null : () => onCheckout(cartModel),

      //   //START NEW by Luthfan Alwafi
      //   onPressed: cartModel.calculatingDiscount
      //       ? null
      //       : () {
      //           List<dynamic> listProduct =
      //               cartModel.productsInCart.keys.map((key) {
      //             String productId = Product.cleanProductID(key);
      //             Product product = cartModel.getProductById(productId);
      //             String spec = product.spec.toString();
      //             return spec;
      //           }).toList();

      //           double totalValue = cartModel.getSubTotal();
      //           // double min = double.parse(minCartPrice);

      //           if (listProduct.toString().contains('Expired')) {
      //             showDialog(
      //               context: context,
      //               barrierDismissible: false,
      //               builder: (BuildContext context) {
      //                 AlertDialog alert = AlertDialog(
      //                   title: Text(S.of(context).cartContainExpired),
      //                   content: Text(S.of(context).cartCleared),
      //                   actions: <Widget>[
      //                     FlatButton(
      //                       onPressed: () {
      //                         Navigator.of(context).pop();
      //                         cartModel.clearCart();
      //                       },
      //                       child: Text(
      //                         S.of(context).ok.toUpperCase(),
      //                       ),
      //                     ),
      //                   ],
      //                 );
      //                 return alert;
      //               },
      //             );
      //           } else if (totalValue > 1 &&
      //               totalValue < store.get("minCartPrice")) {
      //             showDialog(
      //                 context: context,
      //                 barrierDismissible: false,
      //                 builder: (BuildContext context) {
      //                   AlertDialog alert = AlertDialog(
      //                     title: const Text('Ooops!'),
      //                     content: Text(
      //                       store.get("minCartPriceMsg"),
      //                     ),
      //                     actions: <Widget>[
      //                       FlatButton(
      //                         onPressed: () {
      //                           Navigator.of(context).pop();
      //                         },
      //                         child: Text(S.of(context).ok.toUpperCase()),
      //                       ),
      //                     ],
      //                   );
      //                   return alert;
      //                 });
      //           } else if (cartModel.productsInCart.length >
      //               store.get('maxItem')) {
      //             showDialog(
      //                 context: context,
      //                 barrierDismissible: false,
      //                 builder: (BuildContext context) {
      //                   AlertDialog alert = AlertDialog(
      //                     title: const Text('Ooops!'),
      //                     content: Text(
      //                       store.get("maxItemMsg"),
      //                     ),
      //                     actions: <Widget>[
      //                       FlatButton(
      //                         onPressed: () {
      //                           Navigator.of(context).pop();
      //                         },
      //                         child: Text(S.of(context).ok.toUpperCase()),
      //                       ),
      //                     ],
      //                   );
      //                   return alert;
      //                 });
      //           } else {
      //             onCheckout(cartModel);
      //           }
      //         },
      //   //END NEW by Luthfan Alwafi

      //   isExtended: true,
      //   backgroundColor: Theme.of(context).primaryColor,
      //   foregroundColor: Colors.white,
      //   materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      //   // icon: const Icon(Icons.payment, size: 20),

      //   //START NEW by Luthfan Alwafi
      //   icon: cartModel.totalCartQuantity > 0
      //       ? const Icon(Icons.payment, size: 20)
      //       : const Icon(Icons.shopping_cart, size: 20),
      //   //END NEW by Luthfan Alwafi

      //   label: cartModel.totalCartQuantity > 0
      //       ? (isLoading
      //           ? Text(S.of(context).loading.toUpperCase())
      //           : Text(S.of(context).checkout.toUpperCase()))
      //       : Text(
      //           S.of(context).startShopping.toUpperCase(),
      //         ),
      // ),
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      appBar: AppBar(
        backgroundColor: Theme.of(context).backgroundColor,
        leading: widget.isModal == true
            ? IconButton(
                onPressed: () {
                  if (widget.isBuyNow) {
                    Navigator.of(context).pop();
                    return;
                  }
                  if (Navigator.of(context).canPop() &&
                      layoutType != "simpleType") {
                    Navigator.of(context).pop();
                  } else {
                    ExpandingBottomSheet.of(context, isNullOk: true)?.close();
                  }
                },
                icon: const Icon(
                  // Icons.close,

                  //START NEW by Luthfan Alwafi
                  Icons.arrow_back,
                  //END NEW by Luthfan Alwafi

                  size: 22,
                ),
              )
            : Container(),
        title: Text(
          S.of(context).myCart,
          style: const TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
        ),
      ),
      body: Consumer<CartModel>(
        builder: (context, model, child) {
          return GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);
              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: Container(
              decoration:
                  BoxDecoration(color: Theme.of(context).backgroundColor),
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 80.0),
                  child: Column(
                    children: [
                      if (model.totalCartQuantity > 0)
                        Container(
                          margin: const EdgeInsets.only(top: 10.0),
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColorLight),
                          child: Padding(
                            padding:
                                const EdgeInsets.only(right: 15.0, top: 4.0),
                            child: Container(
                              width: screenSize.width,
                              child: Container(
                                width: screenSize.width /
                                    (2 /
                                        (screenSize.height / screenSize.width)),
                                child: Row(
                                  children: [
                                    const SizedBox(
                                      width: 25.0,
                                    ),
                                    Text(
                                      S.of(context).total.toUpperCase(),
                                      style: localTheme.textTheme.subtitle1
                                          .copyWith(
                                              fontWeight: FontWeight.w600,
                                              color: Theme.of(context)
                                                  .primaryColor,
                                              fontSize: 14),
                                    ),
                                    const SizedBox(width: 8.0),
                                    Text(
                                      '${model.totalCartQuantity} ${S.of(context).items}',
                                      style: TextStyle(
                                          color:
                                              Theme.of(context).primaryColor),
                                    ),
                                    Expanded(
                                      child: Align(
                                        alignment: Alignment.centerRight,
                                        child: RaisedButton(
                                          child: Text(
                                            S
                                                .of(context)
                                                .clearCart
                                                .toUpperCase(),
                                            style: const TextStyle(
                                                color: Colors.redAccent,
                                                fontSize: 12),
                                          ),
                                          onPressed: () {
                                            if (model.totalCartQuantity > 0) {
                                              model.clearCart();
                                            }
                                            setState(() {});
                                          },
                                          color: Theme.of(context)
                                              .primaryColorLight,
                                          textColor: Colors.white,
                                          elevation: 0.1,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      if (model.totalCartQuantity > 0)
                        const Divider(
                          height: 1,
                          indent: 25,
                        ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          const SizedBox(height: 16.0),
                          if (model.totalCartQuantity > 0)
                            Column(
                              children: _createShoppingCartRows(model, context),
                            ),
                          if (model.totalCartQuantity > 0)
                            ShoppingCartSummary(model: model),
                          if (model.totalCartQuantity == 0) EmptyCart(),
                          if (errMsg.isNotEmpty)
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 10),
                              child: Text(
                                errMsg,
                                style: const TextStyle(color: Colors.red),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          const SizedBox(height: 4.0),
                          WishList()
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),

      //START NEW by Luthfan ALwafi
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(
          left: 10.0,
          right: 10.0,
          top: 10.0,
          bottom: 30.0,
        ),
        child: cartModel.totalCartQuantity > 0
            ? Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ButtonTheme(
                    height: 50.0,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Row(
                        children: [
                          const Icon(Icons.shopping_cart),
                          const SizedBox(width: 10.0),
                          Text(
                            S.of(context).backToShop.toUpperCase(),
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      color: Theme.of(context).primaryColor,
                      textColor: Colors.white,
                      elevation: 0.1,
                      onPressed: () {
                        if (widget.isModal == true) {
                          try {
                            ExpandingBottomSheet.of(context).close();
                          } catch (e) {
                            Navigator.of(context)
                                .pushNamed(RouteList.dashboard);
                          }
                        } else {
                          MainTabControlDelegate.getInstance().tabAnimateTo(0);
                        }
                      },
                    ),
                  ),
                  ButtonTheme(
                    height: 50.0,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: cartModel.totalCartQuantity > 0
                          ? (isLoading
                              ? Row(
                                  children: [
                                    const Icon(Icons.payment),
                                    const SizedBox(width: 10.0),
                                    Text(
                                      S.of(context).loading.toUpperCase(),
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                )
                              : Row(
                                  children: [
                                    const Icon(Icons.payment),
                                    const SizedBox(width: 10.0),
                                    Text(
                                      S.of(context).checkout.toUpperCase(),
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ))
                          : Container(
                            width: 30.0,
                              child: Row(
                                children: [
                                  const Icon(Icons.shopping_cart),
                                  const SizedBox(width: 10.0),
                                  Text(
                                    S.of(context).startShopping.toUpperCase(),
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                          ),
                      color: Theme.of(context).primaryColor,
                      textColor: Colors.white,
                      elevation: 0.1,
                      onPressed: cartModel.calculatingDiscount
                          ? null
                          : () {
                              var itemInCart = cartModel.item;
                              List<dynamic> cartContainExpired = itemInCart.keys.map((key) {
                                String productId = Product.cleanProductID(key);
                                Product product = cartModel.getProductById(productId);
                                String isExpired = 'Expired : ' + product.spec[productId]['IsExpired'].toString();
                                return isExpired;
                              }).toList();

                              double totalValue = cartModel.getSubTotal();
                              // double min = double.parse(minCartPrice);

                              List<dynamic> itemsCategory = itemInCart.keys.map((key) {
                                String productId = Product.cleanProductID(key);
                                Product product = productId.endsWith('FREEFREEFREEFREE')
                                  ? cartModel.getProductById(productId)
                                  : null;
                                String category = '';
                                if (product != null ) {
                                  for (int i = 0; i < product.categories.length; i++) {
                                    category = category + product.categories[i] + '|';
                                  }
                                }
                                return category;
                              }).toList();
                              itemsCategory.removeWhere((item) => item == '');

                              List<dynamic> itemsID = itemInCart.keys.map((key) {
                                String productId = Product.cleanProductID(key);
                                Product product = productId.endsWith('FREEFREEFREEFREE')
                                  ? cartModel.getProductById(productId)
                                  : null;
                                String id = product != null 
                                  ? product.id
                                  : '';
                                return id;
                              }).toList();
                              itemsID.removeWhere((item) => item == '');
                              
                              bool eventExpired = false;
                              String eventName = event != null
                                ? event['name']
                                : '';
                              String eventCategory = '';
                              String eventID = '';

                              if (eventName == 'buyCateX_GetCateX' || eventName == 'buyCateX_GetCateY') {
                                eventCategory = event['category'] != null 
                                  ? event['category']
                                  : event['categoryX'];
                                for (int i = 0; i < itemsCategory.length; i++) {
                                  eventExpired = !itemsCategory[i].contains(eventCategory)
                                    ? true
                                    : eventExpired;
                                }
                              } else if (eventName == 'buyX_GetX' || eventName == 'buyX_GetY') {
                                eventID = event['item'] != null
                                  ? event['item']
                                  : event['itemX'];
                                for (int i = 0; i < itemsID.length; i++) {
                                  eventExpired = itemsID[i] != eventID
                                    ? true
                                    : eventExpired;
                                }
                              } else {
                                eventExpired = true;
                              }

                              if (cartContainExpired.toString().contains('true')) {
                                showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context) {
                                    AlertDialog alert = AlertDialog(
                                      title: Text(
                                          S.of(context).cartContainExpired),
                                      content: Text(S.of(context).cartCleared),
                                      actions: <Widget>[
                                        FlatButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                            cartModel.clearCart();
                                          },
                                          child: Text(
                                            S.of(context).ok.toUpperCase(),
                                          ),
                                        ),
                                      ],
                                    );
                                    return alert;
                                  },
                                );
                              } else if (eventExpired) {
                                showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context) {
                                    AlertDialog alert = AlertDialog(
                                      title: Text(
                                          S.of(context).cartContainExpiredFree),
                                      content: Text(S.of(context).cartCleared),
                                      actions: <Widget>[
                                        FlatButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                            cartModel.clearCart();
                                          },
                                          child: Text(
                                            S.of(context).ok.toUpperCase(),
                                          ),
                                        ),
                                      ],
                                    );
                                    return alert;
                                  },
                                );
                              } 
                              // else if (totalValue > 1 &&
                              //     totalValue < store.get("minCartPrice")) {
                              //   showDialog(
                              //       context: context,
                              //       barrierDismissible: false,
                              //       builder: (BuildContext context) {
                              //         AlertDialog alert = AlertDialog(
                              //           title: const Text('Ooops!'),
                              //           content: Text(
                              //             store.get("minCartPriceMsg"),
                              //           ),
                              //           actions: <Widget>[
                              //             FlatButton(
                              //               onPressed: () {
                              //                 Navigator.of(context).pop();
                              //               },
                              //               child: Text(
                              //                   S.of(context).ok.toUpperCase()),
                              //             ),
                              //           ],
                              //         );
                              //         return alert;
                              //       });
                              // } 
                              // else if (cartModel.productsInCart.length >
                              //     store.get('maxItem')) {
                              //   showDialog(
                              //       context: context,
                              //       barrierDismissible: false,
                              //       builder: (BuildContext context) {
                              //         AlertDialog alert = AlertDialog(
                              //           title: const Text('Ooops!'),
                              //           content: Text(
                              //             store.get("maxItemMsg"),
                              //           ),
                              //           actions: <Widget>[
                              //             FlatButton(
                              //               onPressed: () {
                              //                 Navigator.of(context).pop();
                              //               },
                              //               child: Text(
                              //                   S.of(context).ok.toUpperCase()),
                              //             ),
                              //           ],
                              //         );
                              //         return alert;
                              //       });
                              // // } else if () {

                              // } 
                              else {
                                onCheckout(cartModel);
                              }
                            },
                    ),
                  ),
                ],
              )
            : Container(
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: ButtonTheme(
                  height: 50.0,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Icon(Icons.shopping_cart),
                        const SizedBox(width: 10.0),
                        Text(
                          S.of(context).startShopping.toUpperCase(),
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                    elevation: 0.1,
                    onPressed: () => onCheckout(cartModel),
                  ),
                ),
              ),
      ),
      //END NEW by Luthfan Alwafi

    );
  }

  void onCheckout(model) {
    bool isLoggedIn = Provider.of<UserModel>(context, listen: false).loggedIn;
    final currencyRate =
        Provider.of<AppModel>(context, listen: false).currencyRate;
    final currency = Provider.of<AppModel>(context, listen: false).currency;

    if (isLoading) return;

    if (kCartDetail['minAllowTotalCartValue'] != null) {
      if (kCartDetail['minAllowTotalCartValue'].toString().isNotEmpty) {
        double totalValue = model.getSubTotal();
        String minValue = Tools.getCurrencyFormatted(
            kCartDetail['minAllowTotalCartValue'], currencyRate,
            currency: currency);
        if (totalValue < kCartDetail['minAllowTotalCartValue'] &&
            model.totalCartQuantity > 0) {
          showFlash(
            context: context,
            duration: const Duration(seconds: 3),
            builder: (context, controller) {
              return SafeArea(
                child: Flash(
                  borderRadius: BorderRadius.circular(3.0),
                  backgroundColor: Theme.of(context).errorColor,
                  controller: controller,
                  style: FlashStyle.grounded,
                  position: FlashPosition.top,
                  horizontalDismissDirection:
                      HorizontalDismissDirection.horizontal,
                  child: FlashBar(
                    icon: const Icon(
                      Icons.check,
                      color: Colors.white,
                    ),
                    message: Text(
                      'Total order\'s value must be at least $minValue',
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                ),
              );
            },
          );

          return;
        }
      }
    }

    if (model.totalCartQuantity == 0) {
      if (widget.isModal == true) {
        try {
          ExpandingBottomSheet.of(context).close();
        } catch (e) {
          Navigator.of(context).pushNamed(RouteList.dashboard);
        }
      } else {
        MainTabControlDelegate.getInstance().tabAnimateTo(0);
      }
    } else if (isLoggedIn || kPaymentConfig['GuestCheckout'] == true) {
      doCheckout();
    } else {
      _loginWithResult(context);
    }
  }

  Future<void> doCheckout() async {
    showLoading();

    await Services().widget.doCheckout(context, success: () async {
      hideLoading('');
      await widget.controller.animateToPage(1,
          duration: const Duration(milliseconds: 250), curve: Curves.easeInOut);
    }, error: (message) async {
      if (message ==
          Exception("Token expired. Please logout then login again")
              .toString()) {
        setState(() {
          isLoading = false;
        });
        //logout
        final userModel = Provider.of<UserModel>(context, listen: false);
        final _auth = FirebaseAuth.instance;
        await userModel.logout();
        await _auth.signOut();
        _loginWithResult(context);
      } else {
        hideLoading(message);
        Future.delayed(const Duration(seconds: 3), () {
          setState(() => errMsg = '');
        });
      }
    }, loading: (isLoading) {
      setState(() {
        this.isLoading = isLoading;
      });
    });
  }

  void showLoading() {
    setState(() {
      isLoading = true;
      errMsg = '';
    });
  }

  void hideLoading(error) {
    setState(() {
      isLoading = false;
      errMsg = error;
    });
  }
}
