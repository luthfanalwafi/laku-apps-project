import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../app.dart';
import '../../../common/constants.dart';
import '../../../generated/l10n.dart';
import '../../../models/entities/product.dart';
import '../../../models/index.dart'
    show
        AppModel,
        CategoryModel,
        FilterAttributeModel,
        FilterTagModel,
        SearchModel;
import '../../../services/index.dart';
import '../../../widgets/common/auto_hide_keyboard.dart';
import 'filters/filter_search.dart';


class SearchResult extends StatefulWidget {
  SearchResult();
  @override
  State<StatefulWidget> createState() => _SearchResultState();
}

class _SearchResultState<T> extends State<SearchResult> {
  final _searchFieldNode = FocusNode();
  RefreshController _refreshController;
  TextEditingController searchController;
  bool isVisibleSearch = false;
  SearchModel get _searchModel => Provider.of<SearchModel>(context, listen: false);
  String keyword;

  @override
  void initState() {
    super.initState();
    keyword = store.get('searchKeyword');
    searchController = TextEditingController(text: keyword);
    Future.delayed(const Duration(milliseconds: 500), () {
      _onSubmit(keyword);
    });
    _refreshController = RefreshController(initialRefresh: false);
  }

  @override
  void dispose() {
    _searchFieldNode?.dispose();
    searchController.dispose();
    _refreshController.dispose();
    // _searchModel.dispose();
    super.dispose();
  }

  List<Widget> _buildActions() {
    return <Widget>[
      searchController.text.isEmpty
          ? IconButton(
              tooltip: 'Search',
              icon: const Icon(Icons.search),
              color: Colors.white,
              onPressed: () => _onSubmit(searchController.text),
            )
          : IconButton(
              tooltip: 'Clear',
              icon: const Icon(Icons.clear),
              color: Colors.white,
              onPressed: () {
                setState(() {
                  searchController.clear();
                  _searchFieldNode.requestFocus();
                });
              },
            ),
    ];
  }

  void _onSubmit(String name) {
    searchController.text = name;
    setState(() {
      _searchModel.loadProduct(
        name: name, 
        isRefresh: true,
      );
    });

    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  void close() {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {

    assert(debugCheckHasMaterialLocalizations(context));
    var theme = Theme.of(context);
    theme = Theme.of(context).copyWith(
      primaryColor: Colors.white,
      primaryIconTheme: theme.primaryIconTheme.copyWith(color: Colors.grey),
      primaryColorBrightness: Brightness.light,
      primaryTextTheme: theme.textTheme,
    );
    final String searchFieldLabel =
    MaterialLocalizations.of(context).searchFieldLabel;
    String routeName = isIos ? '' : searchFieldLabel;

    Widget buildGridViewProduct({
      int crossAxisCount,
      double childAspectRatio,
      double widthContent,
      List<Product> products,
    }) {
      return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: crossAxisCount,
          childAspectRatio: childAspectRatio,
        ),
        cacheExtent: 500.0,
        itemCount: products.length,
        padding: const EdgeInsets.only(top: 20.0),
        itemBuilder: (context, i) {
          return Services().widget.renderProductCardView(
            item: products[i],
            showCart: true,
            showHeart: true,
            width: widthContent,
            ratioProductImage: 1.2 * 0.8,
            marginRight: 10.0,
            showProgressBar: false,
          );
        },
      );
    }

    return Semantics(
      explicitChildNodes: true,
      scopesRoute: true,
      namesRoute: true,
      label: routeName,
      child: Scaffold(
        backgroundColor: theme.backgroundColor,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          iconTheme: theme.primaryIconTheme,
          textTheme: theme.primaryTextTheme,
          brightness: theme.primaryColorBrightness,
          titleSpacing: 0,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios, size: 20),
            onPressed: close,
            color: Colors.white,
          ),
          title: Container(
            child: Row(children: [
              Expanded(
                child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColorLight,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  margin: const EdgeInsets.only(left: 15, right: 15, bottom: 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      const SizedBox(width: 10),
                      Expanded(
                        child: TextField(
                          controller: searchController,
                          decoration: InputDecoration(
                            fillColor: Theme.of(context).accentColor,
                            hintText: S.of(context).searchForItems,
                            enabledBorder: InputBorder.none,
                            border: InputBorder.none,
                          ),
                          autofocus: false,
                          onSubmitted: _onSubmit,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ]),
          ),
          actions: _buildActions(),
        ),
        body: AutoHideKeyboard(
          child: Column(
            children: [
              Container(
                color: Theme.of(context).primaryColor,
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(
                      left: 15, 
                      top: 10.0,
                      bottom: 15.0,
                    ),
                    child: Container(
                      height: 32,
                      child: FilterSearch(
                        onChange: (searchFilter) {
                          _searchModel.searchByFilter(
                            searchFilter,
                            searchController.text,
                          );
                        },
                      ),
                    ),
                  ),
                ),
              ),
              // const SizedBox(height: 20),
              Expanded(
                child: Consumer<SearchModel>(
                  builder: (_, model, __) {
                    final _products = model.products;

                    if (_products == null) {
                      return kLoadingWidget(context);
                    }

                    if (_products.isEmpty) {
                      return Center(
                        child: Text(S.of(context).noProduct),
                      );
                    }

                    return SmartRefresher(
                      header: MaterialClassicHeader(
                        backgroundColor: Theme.of(context).backgroundColor,
                      ),
                      enablePullDown: true,
                      enablePullUp: !model.isEnd,
                      controller: _refreshController,
                      onRefresh: () => _searchModel.loadProduct(
                        name: searchController.text, 
                        isRefresh: true,
                      ),
                      onLoading: () async {
                        await _searchModel.loadProduct(name: searchController.text);
                        _refreshController.refreshCompleted();
                        _refreshController.loadComplete();
                      },
                      footer: kCustomFooter(context),
                      child: buildGridViewProduct(
                        childAspectRatio: 0.65,
                        crossAxisCount: 2,
                        products: _products,
                        widthContent: MediaQuery.of(context).size.width,
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
