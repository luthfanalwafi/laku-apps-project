import 'dart:async';
import 'dart:convert';
import 'dart:math' as math;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fstore/common/constants.dart';
import 'package:fstore/models/cart/cart_base.dart';
import 'package:fstore/models/category_model.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:quiver/strings.dart';

import '../../common/config.dart';
import '../../common/constants/loading.dart';
import '../../common/packages.dart' show FlashHelper;
import '../../common/styles.dart';
import '../../common/tools.dart';
import '../../generated/l10n.dart' show S;
import '../../models/index.dart'
    show AppModel, Product, ProductAttribute, ProductModel, ProductVariation;
import '../../services/index.dart';
import '../../widgets/common/webview.dart';

class ProductVariant extends StatefulWidget {
  final Product product;

  ProductVariant(this.product);

  @override
  StateProductVariant createState() => StateProductVariant(product);
}

class StateProductVariant extends State<ProductVariant> {
  Product product;
  ProductVariation productVariation;

  StateProductVariant(this.product);

  final services = Services();
  Map<String, String> mapAttribute = {};
  List<ProductVariation> variations = [];

  int quantity = 1;

  //START NEW by Luthfan Alwafi
  final Map<String, Product> item = {};
  final name_controller = TextEditingController();

  String domain = serverConfig['url'];
  String _url;
  var token;

  String _material = '',
      _finishing = '',
      _ukuran = '',
      _sudut = '',
      _font = '',
      _fontResult,
      _boxPengaman = '';
  String _ukuranLabel = '',
      _sudutLabel = '',
      _fontLabel = '',
      _fontPrintLabel,
      _boxPengamanLabel = '';
  double font_size = 15.0;
  String _namaType;
  String _ukuranHint, _sudutHint, _fontHint, _fontPrintHint;
  String _ukuranChoices, _sudutChoices, _fontChoices, _namaChoices;
  String _id, _wcc, _ukuranDefault, _fontDefault;
  int _ukuranIndex, _fontIndex;
  List<dynamic> _specification = List();
  List<dynamic> _priceList = List();
  List<dynamic> _listUkuran = List(),
      _listSudut = List(),
      _listFont = List(),
      _listNama = List(),
      _listWcc = List();
  final List<dynamic>
      _listUkuranKeys = List(),
      _listSudutKeys = List(),
      _listFontKeys = List(),
      _listNamaKeys = List();
  bool _mateVis = false,
      _ukuranVis = false,
      _sudutCompVis = false,
      _fontVis = false,
      _namaVis = false,
      _boxVis = false;
  bool _boxChecked = false;
  bool _isLoading = false;
  bool _free = false;
  int freeQty;
  var lstGroupedProduct = [];
  bool isError = false;
  bool isChanged = false;
  //END NEW by Luthfan Alwafi

  //START NEW by Luthfan Alwafi
  void getSpecification() async {
    _url = '$domain/wp-json/wcff/v2/fields/product/';
    _isLoading = true;
    _id = product.id;
    final response = await http.get(_url + _id);
    var _listData = jsonDecode(response.body);
    setState(() {
      _specification = _listData;
    });

    product.productPrice = product.price;

    if (_specification.length.toString() == '0') {
      product.productToken = product.id;
      print("no fields");
    } else {
      _wcc = _specification[0].toString();
      _listWcc = _wcc.split(': {');
      for (int i = 0; i < _listWcc.length; i++) {
        if (_listWcc[i].contains('wccpf')) {
          _listWcc[i] = _listWcc[i].substring(_listWcc[i].indexOf('wccpf_'));
          if (_listWcc[i].contains(',')) {
            _listWcc[i] = _listWcc[i].substring(0, _listWcc[i].indexOf(','));
          }
          if (_listWcc[i].contains('}')) {
            _listWcc[i] = _listWcc[i].substring(0, _listWcc[i].indexOf('}'));
          }
        } else {
          _listWcc[i] = _listWcc[i].replaceAll(_listWcc[i], '');
        }
      }

      _listWcc.removeWhere((item) => item == '');
      _listWcc = _listWcc.toSet().toList();

      print(_listWcc);

      for (int i = 0; i < _listWcc.length; i++) {
        if (_listWcc[i].contains('deal')) {
          product.dealToday = _specification[0][_listWcc[i]]['placeholder'];
        } else if (_listWcc[i].contains('box')) {
          _BoxVisibility(true);
          _boxPengamanLabel = _specification[0][_listWcc[i]]['label'];
          if (_boxChecked) {
            _boxPengaman = _boxPengamanLabel +
                ' : ' +
                (_specification[0][_listWcc[i]]['choices']).split('|')[1];
            if (_boxPengaman.contains(';')) {
              _boxPengaman =
                  _boxPengaman.substring(0, _boxPengaman.indexOf(';'));
            }
            product.box = _boxPengaman;
          } else {
            product.box = '';
          }
        } else if (_listWcc[i].contains('event') &&
            _specification[0][_listWcc[i]]['type'] != 'hidden') {
          _MaterialVisibility(true);

          _material =
              (_specification[0][_listWcc[i]]['message']).split(', ')[0];
          _finishing =
              (_specification[0][_listWcc[i]]['message']).split(', ')[1];
          product.material = _material;
          product.finishing = _finishing;
        } else if (_listWcc[i].contains('ukuran') ||
            _listWcc[i].contains('size') &&
                _specification[0][_listWcc[i]]['type'] != 'hidden') {
          _UkuranVisibility(true);

          _ukuranLabel = _specification[0][_listWcc[i]]['label'];
          _ukuranChoices = _specification[0][_listWcc[i]]['choices'];
          _ukuranHint = _specification[0][_listWcc[i]]['placeholder'];
          _listUkuran = _ukuranChoices.split(';');
          _ukuranDefault = _specification[0][_listWcc[i]]['default_value'];

          for (int i = 0; i < _listUkuran.length; i++) {
            if (_listUkuran[i].contains(_ukuranDefault)) {
              _ukuranIndex = i;
            }
          }

          for (int i = 0; i < _listUkuran.length; i++) {
            if (_listUkuran[i] != '') {
              _listUkuranKeys.add(_listUkuran[i].split('|'));
            }
            _listUkuran[i] =
                _listUkuran[i].substring(_listUkuran[i].indexOf('|') + 1);
          }

          _listUkuran.removeWhere((item) => item == '');
          _listUkuranKeys.removeWhere((item) => item == '');

          _ukuran = _listUkuran[_ukuranIndex];

          if (_specification[0][_listWcc[i]].toString().contains('pric')) {
            _priceList =
                _specification[0][_listWcc[i]]['pricing_rules'].toList();
            for (int a = 0; a < _priceList.length; a++) {
              if (_ukuran.contains(_priceList[a]['expected_value'])) {
                product.productPrice = _priceList[a]['amount'];
              }
            }
          }

          product.ukuran = '';
          product.ukuran = _ukuranLabel + ' : ' + _ukuran;
        } else if ((_listWcc[i].contains('sudut') ||
                _listWcc[i].contains('posisi')) &&
            _specification[0][_listWcc[i]]['type'] != 'hidden') {
          _SudutCompVisibility(true);

          _sudutLabel = _specification[0][_listWcc[i]]['label'];
          _sudutChoices = _specification[0][_listWcc[i]]['choices'];
          _sudutHint = _specification[0][_listWcc[i]]['placeholder'];
          _listSudut = _sudutChoices.split(';');

          for (int i = 0; i < _listSudut.length; i++) {
            if (_listSudut[i] != '') {
              _listSudutKeys.add(_listSudut[i].split('|'));
            }
            _listSudut[i] =
                _listSudut[i].substring(_listSudut[i].indexOf('|') + 1);
          }

          _listSudut.removeWhere((item) => item == '');
          _listSudutKeys.removeWhere((item) => item == '');

          product.sudut = '';
          _sudut = _listSudut[0];
          product.sudut = _sudutLabel + ' : ' + _sudut;
        } else if (_listWcc[i].contains('font') &&
            _specification[0][_listWcc[i]]['type'] != 'hidden') {
          _FontVisibility(true);
          if (_sudutCompVis == true) {
            _FontVisibility(false);
          }

          _fontLabel = _specification[0][_listWcc[i]]['label'];
          _fontChoices = _specification[0][_listWcc[i]]['choices'];
          _fontHint = _specification[0][_listWcc[i]]['placeholder'];
          _listFont = _fontChoices.split(';');
          _fontDefault = _specification[0][_listWcc[i]]['default_value'];

          for (int i = 0; i < _listFont.length; i++) {
            if (_listFont[i].contains(_fontDefault)) {
              _fontIndex = i;
            }
          }

          for (int i = 0; i < _listFont.length; i++) {
            if (_listFont[i] != '') {
              _listFontKeys.add(_listFont[i].split('|'));
            }
            _listFont[i] =
                _listFont[i].substring(_listFont[i].indexOf('|') + 1);
          }

          _listFont.removeWhere((item) => item == '');
          _listFontKeys.removeWhere((item) => item == '');

          product.font = '';
          _font = _listFont[_fontIndex];
        } else if (_listWcc[i].contains('nama') &&
            _specification[0][_listWcc[i]]['type'] != 'hidden') {
          _NamaVisibility(true);
          if (_sudutCompVis == true || _font == 'Polos') {
            _NamaVisibility(false);
          }

          _namaType = _specification[0][_listWcc[i]]['type'];

          if (_namaType == 'select') {
            _namaChoices = _specification[0][_listWcc[i]]['choices'];
            _listNama = _namaChoices.split(';');

            for (int i = 0; i < _listNama.length; i++) {
              if (_listNama[i] != '') {
                _listNamaKeys.add(_listNama[i].split('|'));
              }
              _listNama[i] =
                  _listNama[i].substring(_listNama[i].indexOf('|') + 1);
            }

            _listNama.removeWhere((item) => item == '');
            _listNamaKeys.removeWhere((item) => item == '');

            product.cetak = '';
            name_controller.text = _listNama[0];
          } else if (_namaType == 'text') {
            _fontResult = _specification[0][_listWcc[i + 1]]['label'];
          }

          _fontPrintLabel = _specification[0][_listWcc[i]]['label'];
          _fontPrintHint = _specification[0][_listWcc[i]]['placeholder'];

          product.cetak = '';
        }
      }
    }
    _isLoading = false;
  }

  void _MaterialVisibility(bool visibility) {
    _mateVis = visibility;
  }

  void _UkuranVisibility(bool visibility) {
    _ukuranVis = visibility;
  }

  void _SudutCompVisibility(bool visibility) {
    _sudutCompVis = visibility;
  }

  void _FontVisibility(bool visibility) {
    _fontVis = visibility;
  }

  void _NamaVisibility(bool visibility) {
    _namaVis = visibility;
  }

  // void _CetakVisibility(bool visibility) {
  //   _cetakVis = visibility;
  // }

  void _BoxVisibility(bool visibility) {
    _boxVis = visibility;
  }
  //END NEW by Luthfan Alwafi

  /// Get product variants
  Future<void> getProductVariations() async {
    await services.widget.getProductVariations(
        context: context,
        product: product,
        onLoad: (
            {Product productInfo,
            List<ProductVariation> variations,
            Map<String, String> mapAttribute,
            ProductVariation variation}) {
          setState(() {
            if (productInfo != null) {
              product = productInfo;
            }
            this.variations = variations;
            this.mapAttribute = mapAttribute;
            if (variation != null) {
              productVariation = variation;
              Provider.of<ProductModel>(context, listen: false)
                  .changeProductVariation(productVariation);
            }
          });
          return;
        });
  }

  @override
  void initState() {
    super.initState();
    getProductVariations();

    //START NEW by Luthfan ALwafi
    getSpecification();
    //END NEW by Luthfan Alwafi
    
  }

  @override
  void dispose() {
    FlashHelper.dispose();
    super.dispose();
  }

  /// Support Affiliate product
  void openWebView() {
    if (product.affiliateUrl == null || product.affiliateUrl.isEmpty) {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return Scaffold(
          appBar: AppBar(
            leading: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: const Icon(Icons.arrow_back_ios),
            ),
          ),
          body: Center(
            child: Text(S.of(context).notFound),
          ),
        );
      }));
      return;
    }

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => WebView(
                  url: product.affiliateUrl,
                  title: product.name,
                )));
  }

  /// Add to Cart & Buy Now function
  void addToCart([bool buyNow = false, bool inStock = false]) {
    services.widget.addToCart(context, product, quantity, productVariation,
        mapAttribute, buyNow, inStock);

    //START NEW by Luthfan Alwafi
    setState(() {});
    //END NEW by Luthfan Alwafi
    
  }

  /// check limit select quality by maximum available stock
  int getMaxQuantity() {
    int limitSelectQuantity = kCartDetail['maxAllowQuantity'] ?? 100;
    if (productVariation != null) {
      if (productVariation.stockQuantity != null) {
        limitSelectQuantity = math.min(
            productVariation.stockQuantity, kCartDetail['maxAllowQuantity']);
      }
    } else if (product.stockQuantity != null) {
      limitSelectQuantity =
          math.min(product.stockQuantity, kCartDetail['maxAllowQuantity']);
    }
    return limitSelectQuantity;
  }

  /// Check The product is valid for purchase
  bool couldBePurchased() {
    return services.widget
        .couldBePurchased(variations, productVariation, product, mapAttribute);
  }

  void onSelectProductVariant({
    ProductAttribute attr,
    String val,
    List<ProductVariation> variations,
    Map<String, String> mapAttribute,
    Function onFinish,
  }) {
    services.widget.onSelectProductVariant(
      attr: attr,
      val: val,
      variations: variations == null ? this.variations : variations,
      mapAttribute: mapAttribute,
      onFinish: (Map<String, String> mapAttribute, ProductVariation variation) {
        setState(() {
          this.mapAttribute = mapAttribute;
        });
        productVariation = variation;
        Provider.of<ProductModel>(context, listen: false)
            .changeProductVariation(variation);
      },
    );
  }

  List<Widget> getProductAttributeWidget() {
    final lang = Provider.of<AppModel>(context, listen: false).langCode ?? 'en';
    return services.widget.getProductAttributeWidget(
        lang, product, mapAttribute, onSelectProductVariant, variations);
  }

  List<Widget> getBuyButtonWidget() {

    //START NEW by Luthfan Alwafi
    // try
    // if (_free && freeQty < 1){
    //   var abc = freeQty;
    // }

    if (_specification.length > 0.0) {
      var customFields = Map<String, dynamic>.from(_specification[0]);
      setState(() {
        // checkCart();

        Map<String, dynamic> _itemsData = {};

        //Custom field wccpf_bahan
        var fieldInfoBahan = customFields["wccpf_bahan"];
        if (fieldInfoBahan != null) {
          _itemsData['wccpf_bahan'] = {
            'fname': fieldInfoBahan['name'],
            'ftype': fieldInfoBahan['type'],
            'user_val': fieldInfoBahan['placeholder'],
            'fee_rules': [],
            'pricing_rules': fieldInfoBahan['pricing_rules'],
            'format': ''
          };
        }

        if (product.material == null) {
          _material = '';
        }
        product.material = _material;

        //Custom field wccpf_finishing
        var fieldInfoFinishing = customFields["wccpf_finishing"];
        if (fieldInfoFinishing != null) {
          _itemsData['wccpf_finishing'] = {
            'fname': fieldInfoFinishing['name'],
            'ftype': fieldInfoFinishing['type'],
            'user_val': fieldInfoFinishing['placeholder'],
            'fee_rules': [],
            'pricing_rules': fieldInfoFinishing['pricing_rules'],
            'format': ''
          };
        }

        //Custom field wccpf_ukuran
        product.ukuran = _ukuran == '' ? '' : _ukuranLabel + ' : ' + _ukuran;
        var userValUkuran = '';
        if (_ukuran != '') {
          int ukuranIndex = _listUkuranKeys.indexWhere((f) => f[1] == _ukuran);
          userValUkuran = _listUkuranKeys[ukuranIndex][0];
        } else {
          userValUkuran = '';
        }

        var fieldInfoUkuran = customFields["wccpf_ukuran"];
        if (fieldInfoUkuran != null) {
          _itemsData['wccpf_ukuran'] = {
            'fname': fieldInfoUkuran['name'],
            'ftype': fieldInfoUkuran['type'],
            'user_val': userValUkuran,
            'fee_rules': [],
            'pricing_rules': fieldInfoUkuran['pricing_rules'],
            'format': ''
          };
        }

        //Custom field wccpf_sudut
        product.sudut = _sudut == '' ? '' : _sudutLabel + ' : ' + _sudut;
        var userValSudut = '';
        if (_sudut != '') {
          int sudutIndex = _listSudutKeys.indexWhere((f) => f[1] == _sudut);
          userValSudut = _listSudutKeys[sudutIndex][0];
        } else {
          userValSudut = '';
        }

        var fieldInfoSudut = customFields["wccpf_sudut"];
        if (fieldInfoSudut != null) {
          _itemsData['wccpf_sudut'] = {
            'fname': fieldInfoSudut['name'],
            'ftype': fieldInfoSudut['type'],
            'user_val': userValSudut,
            'fee_rules': [],
            'pricing_rules': fieldInfoSudut['pricing_rules'],
            'format': ''
          };
        }

        //Custom field wccpf_font
        product.font = _fontLabel + ' : ' + _font;

        product.cetak = '"' + name_controller.text + '"';
        if (_sudut == 'Tanpa Sudut' || _sudut == 'Polos') {
          product.font = '';
          product.cetak = '';
        }
        if (_font == 'Polos') {
          product.cetak = '';
        }
        if (_fontVis == false) {
          product.font = '';
        }

        var fieldInfoFont = customFields["wccpf_font"];
        var userValFont =
            _sudut == '' || _sudut == 'Tanpa Sudut' || _sudut == 'Polos'
                ? ''
                : _font;
        if (product.categoryId == '96') {
          userValFont = _font;
        }
        if (userValFont != '') {
          int fontIndex = _listFontKeys.indexWhere((f) => f[1] == userValFont);
          if (fontIndex > 0) {
            userValFont = _listFontKeys[fontIndex][0];
          }
        }
        if (fieldInfoFont != null) {
          _itemsData['wccpf_font'] = {
            'fname': fieldInfoFont['name'],
            'ftype': fieldInfoFont['type'],
            'user_val': userValFont,
            'fee_rules': [],
            'pricing_rules': fieldInfoFont['pricing_rules'],
            'format': ''
          };
        }

        var fieldInfoCn = customFields["wccpf_cetak_nama"];
        var userValCn = _sudut == 'Tanpa Sudut' || _sudut == 'Polos'
            ? ''
            : name_controller.text;
        if (fieldInfoCn != null) {
          _itemsData['wccpf_cetak_nama'] = {
            'fname': fieldInfoCn['name'],
            'ftype': fieldInfoCn['type'],
            'user_val': userValCn,
            'fee_rules': [],
            'pricing_rules': fieldInfoCn['pricing_rules'],
            'format': ''
          };
        }

        // Custom field wccpf_get_free
        var userValFree;
        var fieldInfoFree = customFields["wccpf_get_free"];
        if (fieldInfoFree != null) {
          if (_free) {
            userValFree = fieldInfoFree['pricing_rules'][0]['expected_value'];
          }
          _itemsData['wccpf_get_free'] = {
            'fname': fieldInfoFree['name'],
            'ftype': fieldInfoFree['type'],
            'user_val': userValFree,
            'fee_rules': [],
            'pricing_rules': fieldInfoFree['pricing_rules'],
            'format': ''
          };
        }

        //Custom field wccpf_deal_today
        var field_deal_today = customFields["wccpf_deal_today"];
        if (field_deal_today != null) {
          _itemsData['wccpf_deal_today'] = {
            'fname': field_deal_today['name'],
            'ftype': field_deal_today['type'],
            'user_val': field_deal_today['placeholder'],
            'fee_rules': [],
            'pricing_rules': field_deal_today['pricing_rules'],
            'format': ''
          };
        }

        product.itemsData = Map.from(_itemsData);
        // product.itemsData = {};
        product.productToken = product.id +
            '.' +
            product.dealToday +
            product.material +
            product.finishing +
            _ukuran +
            _sudut +
            product.font +
            product.cetak +
            product.box;
      });
    } else {
      setState(() {
        product.productToken = product.id;
      });
    }

    return services.widget.getBuyButtonWidget(
      context, 
      productVariation,
      product, 
      mapAttribute, 
      _free && product.productPrice == '0' ? freeQty : getMaxQuantity(), 
      quantity,
      addToCart,
      (val) {
        setState(() {
          quantity = val;
        });
      },
      variations,
      // _free,
    );
    //END NEW by Luthfan Alwafi

    // return services.widget.getBuyButtonWidget(context, productVariation,
    //     product, mapAttribute, getMaxQuantity(), quantity, addToCart, (val) {
    //   setState(() {
    //     quantity = val;
    //   });
    // }, variations);
  }

  List<Widget> getProductTitleWidget() {
    return services.widget
        .getProductTitleWidget(context, productVariation, product);
  }

  @override
  Widget build(BuildContext context) {
    FlashHelper.init(context);

    //START NEW by Luthfan Alwafi
    final ThemeData theme = Theme.of(context);
    final currency = Provider.of<AppModel>(context).currency;
    final rates = Provider.of<AppModel>(context).currencyRate;
    final regularPrice = productVariation != null
        ? productVariation.regularPrice
        : product.regularPrice;
    var onSale = _free
        ? true
        : productVariation != null
            ? productVariation.onSale
            : product.onSale;
    final price = productVariation != null
        ? productVariation.price
        : isNotBlank(product.price)
            ? product.price
            : product.regularPrice;
    String _price = Tools.getCurrencyFormatted(price, rates, currency: currency);
    int sale = 100;

    final config = Provider.of<AppModel>(context).appConfig;
    final event = config['event'];
    final eventName = event['name'] != null ? event['name'] : '';

    int buyItem = 0, freeItem = 0, getFree = 0;
    String buyCategory = '0', freeCategory = '0';
    String buyItemID = '0', freeItemID = '0';
    String categoryName = '', itemName = '';
    List categoryList = [];
    var categoryModel = Provider.of<CategoryModel>(context);
    var cartModel = Provider.of<CartModel>(context, listen: false);
    List<dynamic> productInCart = cartModel.item.keys.map((key) {
      String productId = Product.cleanProductID(key);
      return productId;
    }).toList();

    if (regularPrice.isNotEmpty && double.parse(regularPrice) > 0) {
      sale = (100 - (double.parse(price) / double.parse(regularPrice)) * 100).toInt();
    }

    if (event != null) {
      if (eventName == 'buyCateX_GetCateX' || eventName == 'buyCateX_GetCateY') {
        buyCategory = event['category'] != null ? event['category'] : event['categoryX'];
        freeCategory = event['category'] != null ? event['category'] : event['categoryY'];
        categoryName = categoryModel.categoryList[freeCategory] != null 
          ? categoryModel.categoryList[freeCategory].name 
          : '';
        for (int i = 0; i < product.categories.length; i++) {
          if (categoryModel.categoryList[product.categories[i]] != null) {
            categoryList.add(categoryModel.categoryList[product.categories[i]].parent); 
          }
          categoryList = categoryList.toSet().toList();
          categoryList.removeWhere((item) => item == '0');
          categoryList.add(product.categories[i]);
        }
        product.categories = categoryList;
        for (int i = 0; i < productInCart.length; i++) {
          var list = cartModel.item[productInCart[i]];
          var spec = list.spec[productInCart[i]];
          if (list.categoryId.contains(freeCategory)) {
            freeItem = spec['Harga'] == '0'
              ? freeItem + cartModel.productsInCart[productInCart[i]]
              : freeItem;
          }
          if (list.categoryId.contains(buyCategory)) {
            if (event['buySize'] != null && spec['Ukuran'].contains(event['buySize'])
              && spec['Harga'] != '0') {
              buyItem = buyItem + cartModel.productsInCart[productInCart[i]];
            } else if (event['buySize'] == null && spec['Harga'] != '0') {
              buyItem = buyItem + cartModel.productsInCart[productInCart[i]];
            } else {
              buyItem = buyItem;
            }
          }
          getFree = buyItem >= event['buy'] ? 0 + event['get'] - freeItem : 0;
          if (event['multiply']) {
            getFree = (buyItem ~/ event['buy']) * event['get'] - freeItem;
          }
        }
      }
      else if (eventName == 'buyX_GetX' || eventName == 'buyX_GetY') {
        buyItemID = event['item'] != null ? event['item'] : event['itemX'];
        freeItemID = event['item'] != null ? event['item'] : event['itemY'];
        itemName = product.id == freeItemID ? product.name : '';
        for (int i = 0; i < productInCart.length; i++) {
          var list = cartModel.item[productInCart[i]];
          var spec = list.spec[productInCart[i]];
          if (freeItemID == list.id) {
            freeItem = spec['Harga'] == '0'
              ? freeItem + cartModel.productsInCart[productInCart[i]]
              : freeItem;
          }
          if (buyItemID == list.id) {
            if (event['buySize'] != null && spec['Ukuran'].contains(event['buySize'])
              && spec['Harga'] != '0') {
              buyItem = buyItem + cartModel.productsInCart[productInCart[i]];
            } else if (event['buySize'] == null && spec['Harga'] != '0') {
              buyItem = buyItem + cartModel.productsInCart[productInCart[i]];
            } else {
              buyItem = buyItem;
            }
          }
          getFree = buyItem >= event['buy'] ? 0 + event['get'] - freeItem : 0;
          if (event['multiply']) {
            getFree = (buyItem ~/ event['buy']) * event['get'] - freeItem;
          }
          if (getFree == 0) {
            _free = false;
          }
        }
      }

      freeQty = getFree;

      if (getFree == 0 && buyItem >= 0) {
        if (_priceList.length.toString() != '0') {
          for (int a = 0; a < _priceList.length; a++) {
            if (_ukuran.contains(_priceList[a]['expected_value'])) {
              if (_boxChecked) {
                product.productPrice = (int.parse(_priceList[a]['amount']) +
                  5000).toString();
              } else {
                product.productPrice = _priceList[a]['amount'];
              }
            }
          }
        }
        else if (product.productPrice == '0') {
          product.productPrice = product.price;
        }
        if (!product.onSale) {
          onSale = false;
        }
        _free = false;
      }
      if ((product.price == '0' || product.price == null) 
        && product.productPrice == '0' || product.productPrice == null) {
        product.price = product.initialPrice;
      }

      if (_free && (event['buySize'] == null || event['buySize'] == '')) {
        sale = 100;
      }
      else if (_free && !_ukuran.contains(event['getSize'])) {
        sale = 0;
        onSale = false;
      }
    } else if (regularPrice.isNotEmpty && double.parse(regularPrice) > 0) {
      sale = (100 - (double.parse(price) / double.parse(regularPrice)) * 100).toInt();
    }
    

    final Harga = Container(
      alignment: Alignment.centerLeft,
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              // Text(
              //   product.productPrice == null || product.productPrice.isEmpty
              //   ? widget.product.type != 'grouped'
              //     ? _price
              //     : Provider.of<ProductModel>(context).detailPriceRange
              //   : Tools.getCurrencyFormatted(product.productPrice, rates, currency: currency),
              //   style: Theme.of(context).textTheme.subtitle1.copyWith(
              //         fontSize: 25,
              //         fontWeight: FontWeight.bold,
              //         color: theme.accentColor,
              //       ),
              // ),
              AnimatedDefaultTextStyle(
                duration: const Duration(milliseconds: 500),
                child: Text(
                  product.productPrice == null || product.productPrice.isEmpty
                  ? widget.product.type != 'grouped'
                    ? _price
                    : Provider.of<ProductModel>(context).detailPriceRange
                  : Tools.getCurrencyFormatted(product.productPrice, rates, currency: currency),
                ), 
                style: isChanged
                ? Theme.of(context).textTheme.subtitle1.copyWith(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                        color: Colors.red,
                )
                : Theme.of(context).textTheme.subtitle1.copyWith(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                        color: theme.accentColor,
                ),
                onEnd: () {
                  setState(() {
                    isChanged = false;
                  });
                },
              ),
              if (onSale && widget.product.type != 'grouped')
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    const SizedBox(width: 5),
                    Text(
                        Tools.getCurrencyFormatted(regularPrice, rates,
                            currency: currency),
                        style: Theme.of(context).textTheme.subtitle1.copyWith(
                            fontSize: 14,
                            color:
                                Theme.of(context).accentColor.withOpacity(0.6),
                            fontWeight: FontWeight.w400,
                            decoration: TextDecoration.lineThrough)),
                    const SizedBox(width: 5),
                    Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 2),
                      decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(5)),
                      child: Text(
                        S.of(context).sale('$sale'),
                        style: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontSize: 14),
                      ),
                    )
                  ],
                )
            ],
          ),
          const SizedBox(height: 30.0)
        ],
      ),
    );

    final Material = _mateVis
        ? Column(
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(top: 5.0, bottom: 10.0),
                padding: const EdgeInsets.only(left: 15.0),
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  color: Theme.of(context).focusColor,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    const SizedBox(height: 15.0),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        '$_material',
                        style: const TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    const SizedBox(height: 8.0),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        '$_finishing',
                        style: const TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    const SizedBox(height: 15.0),
                  ],
                ),
              ),
              const SizedBox(
                height: 20.0,
              ),
            ],
          )
        : Container();

    final Ukuran = _ukuranVis
        ? Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Text(
                      '$_ukuranLabel',
                      style: const TextStyle(fontWeight: FontWeight.w900),
                    ),
                  ),
                  Container(
                    width: 230.0,
                    height: 38.0,
                    padding: const EdgeInsets.only(left: 10.0),
                    decoration: kDropDown,
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                        hint: Text('$_ukuranHint'),
                        value: _ukuran,
                        elevation: 5,
                        items: _listUkuran.map((item) {
                          return DropdownMenuItem(
                            child: Text(item),
                            value: item,
                          );
                        }).toList(),
                        onChanged: (value) {
                          isChanged = true;
                          setState(() {
                            Future.delayed(const Duration(milliseconds: 3000), () {
                              isChanged = !isChanged;
                            });
                            _ukuran = value;
                            if (_free && (event['getSize'] == null || event['getSize'] == '')) {
                              for (int a = 0; a < _priceList.length; a++) {
                                if (_ukuran.contains(
                                  _priceList[a]['expected_value'])) {
                                  if (_boxChecked) {
                                    product.regularPrice =
                                      (int.parse(_priceList[a]['amount']) +
                                            5000).toString();
                                  } else {
                                    product.regularPrice = _priceList[a]['amount'];
                                  }
                                }
                              }
                              product.productPrice = '0';
                            } else if (event['getSize'] != null) {
                              if (_ukuran.contains(event['getSize'])) {
                                for (int a = 0; a < _priceList.length; a++) {
                                  if (_ukuran.contains(
                                    _priceList[a]['expected_value'])) {
                                    if (_boxChecked) {
                                      product.regularPrice =
                                        (int.parse(_priceList[a]['amount']) +
                                              5000).toString();
                                    } else {
                                      product.regularPrice = _priceList[a]['amount'];
                                    }
                                  }
                                }
                                product.productPrice = '0';
                              }
                              
                            } else if (_priceList.length.toString() != '0') {
                              for (int a = 0; a < _priceList.length; a++) {
                                if (_ukuran.contains(
                                    _priceList[a]['expected_value'])) {
                                  if (_boxChecked) {
                                    product.productPrice =
                                        (int.parse(_priceList[a]['amount']) +
                                                5000)
                                            .toString();
                                  } else {
                                    product.productPrice = _priceList[a]['amount'];
                                  }
                                }
                              }
                            }
                          });
                        },
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10.0,
              ),
            ],
          )
        : Container();

    final Sudut = _sudutCompVis
        ? Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Text(
                      '$_sudutLabel',
                      style: const TextStyle(fontWeight: FontWeight.w900),
                    ),
                  ),
                  Container(
                    width: 230.0,
                    height: 38.0,
                    padding: const EdgeInsets.only(left: 10.0),
                    decoration: kDropDown,
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                        hint: Text('$_sudutHint'),
                        value: _sudut,
                        elevation: 5,
                        items: _listSudut.map((value) {
                          return DropdownMenuItem(
                            child: Text(value),
                            value: value,
                          );
                        }).toList(),
                        onChanged: (value) {
                          setState(() {
                            isChanged = false;
                            _sudut = value;
                            // _sudut = _sudutLabel + ' : ' + _sudut;
                            if (_sudut == 'Tanpa Sudut' || _sudut == "Polos") {
                              _FontVisibility(false);
                              _NamaVisibility(false);
                              // product.font = '';
                              // product.cetak = '';
                            } else {
                              _FontVisibility(true);
                              _NamaVisibility(true);
                              // if (product.font != '' || product.cetak != '') {
                              //   product.font = _fontLabel + ' : ' + _font;
                              //   product.cetak =
                              //       '"' + name_controller.text + '"';
                              // }
                            }
                            // product.sudutState = _sudutLabel + ' : ' + _sudut;
                            // product.spec = product.id + '.' + _ukuran + _sudut + _font + name_controller.text;
                          });
                        },
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10.0,
              ),
            ],
          )
        : Container();

    final Font = _fontVis
        ? Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Text(
                      '$_fontLabel',
                      style: const TextStyle(fontWeight: FontWeight.w900),
                    ),
                  ),
                  Container(
                    width: 230.0,
                    height: 38.0,
                    padding: const EdgeInsets.only(left: 10.0),
                    decoration: kDropDown,
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                        hint: Text('$_fontHint'),
                        value: _font,
                        elevation: 5,
                        items: _listFont.map((value) {
                          return DropdownMenuItem(
                            child: Text(value),
                            value: value,
                          );
                        }).toList(),
                        onChanged: (value) {
                          setState(() {
                            isChanged = false;
                            _font = value;
                            if (_font == 'Polos') {
                              value = '';
                              _NamaVisibility(false);
                            } else {
                              _NamaVisibility(true);
                              try {
                                GoogleFonts.getFont(_font);
                              } catch (error) {
                                String errormsg = error.toString();
                                showDialog(
                                    context: context,
                                    barrierDismissible: false,
                                    builder: (BuildContext context) {
                                      AlertDialog alert = AlertDialog(
                                        title: const Text('Ooops!'),
                                        content: Text(
                                          errormsg,
                                        ),
                                        actions: <Widget>[
                                          FlatButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                              _font = _listFont[2];
                                            },
                                            child: const Text('OK'),
                                          ),
                                        ],
                                      );
                                      return alert;
                                    });
                                rethrow;
                              }
                            }

                            // _font = _fontLabel + ' : ' + _font;
                            // product.fontState = _fontLabel + ' : ' + _font;
                            // product.spec = product.id + '.' + _ukuran + _sudut + _font + name_controller.text;
                          });
                        },
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10.0,
              ),
              // Jenis Huruf
            ],
          )
        : Container();

    final Nama = _namaVis
        ? Column(
            children: <Widget>[
              // Cetak Nama
              _namaType == 'text'
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: const EdgeInsets.only(left: 10.0, top: 12.0),
                          child: Text(
                            '$_fontPrintLabel',
                            style: const TextStyle(fontWeight: FontWeight.w900),
                          ),
                        ),
                        Container(
                          width: 230.0,
                          height: 62.0,
                          alignment: Alignment.centerLeft,
                          child: TextField(
                            controller: name_controller,
                            maxLength: 15,
                            maxLines: 1,
                            cursorColor: Colors.blue,
                            decoration: kTextField.copyWith(
                                hintText: '$_fontPrintHint',
                                contentPadding: const EdgeInsets.symmetric(
                                    horizontal: 10.0)),
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(RegExp(
                                  "[a-z A-Z 0-9 `~,<.>/?;:!@#\$%^&*()_=+{}|]"))
                            ],
                            onSubmitted: (value) {
                              setState(() {
                                isChanged = false;
                                name_controller.text = value;
                                // name_controller.text = '"' + name_controller.text + '"';
                                // product.cetakState = '"' + name_controller.text + '"';
                                // product.spec = product.id + '.' + _ukuran + _sudut + _font + name_controller.text;
                              });
                            },
                            // onChanged: (value) {
                            //   setState(() {
                            //     name_controller.text = value;
                            //     name_controller.selection = TextSelection(baseOffset: value.length, extentOffset: value.length);
                            //   });
                            // },
                          ),
                        ),
                      ],
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Text(
                            '$_fontPrintLabel',
                            style: const TextStyle(fontWeight: FontWeight.w900),
                          ),
                        ),
                        Container(
                          width: 230.0,
                          height: 38.0,
                          padding: const EdgeInsets.only(left: 10.0),
                          decoration: kDropDown,
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              hint: Text('$_fontPrintHint'),
                              value: name_controller.text,
                              elevation: 5,
                              items: _listNama.map((value) {
                                return DropdownMenuItem(
                                  child: Text(value),
                                  value: value,
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  name_controller.text = value;
                                });
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
              const SizedBox(
                height: 20.0,
              ),
            ],
          )
        : Container();

    final PrintNama = _fontVis && _namaVis
        ? Column(
            children: <Widget>[
              Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 10.0,
                  horizontal: 15.0,
                ),
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  color: Theme.of(context).hoverColor,
                ),
                height: 480.0,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Column(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.topLeft,
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: Row(
                          children: [
                            Text(
                              '$_fontResult : ',
                              style: const TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            const SizedBox(
                              width: 10.0,
                            ),
                            Text(
                              name_controller.text,
                              style: GoogleFonts.getFont(
                                _font == 'Polos' ? 'Roboto' : _font,
                                textStyle: const TextStyle(
                                  fontSize: 28.0,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        width: 10.0,
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                        padding: const EdgeInsets.all(10.0),
                        height: 350.0,
                        width: 350.0,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Theme.of(context).accentColor,
                            width: 5.0,
                          )
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  _sudut == '4 Sudut' ? RotatedBox(
                                    quarterTurns: 1,
                                    child: Container(
                                      padding: const EdgeInsets.only(left: 5.0),
                                      child: Text(
                                        name_controller.text,
                                        style: GoogleFonts.getFont(
                                          _font == 'Polos' ? 'Roboto' : _font,
                                          fontSize: font_size,
                                        ),
                                      ),
                                    ),
                                  ) : Container(),
                                  _sudut == '2 Sudut' || _sudut == '4 Sudut' ? RotatedBox(
                                    quarterTurns: 2,
                                    child: Text(
                                      name_controller.text,
                                      style: GoogleFonts.getFont(
                                        _font == 'Polos' ? 'Roboto' : _font,
                                        fontSize: font_size,
                                      ),
                                    ),
                                  ) : Container(),
                                ],
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Container(
                                  alignment: Alignment.bottomLeft,
                                  child: Text(
                                    name_controller.text,
                                    style: GoogleFonts.getFont(
                                      _font == 'Polos' ? 'Roboto' : _font,
                                      fontSize: font_size,
                                    ),
                                  ),
                                ),
                                _sudut == '4 Sudut' ? RotatedBox(
                                  quarterTurns: 3,
                                  child: Container(
                                    padding: const EdgeInsets.only(left: 5.0),
                                    child: Text(
                                      name_controller.text,
                                      style: GoogleFonts.getFont(
                                        _font == 'Polos' ? 'Roboto' : _font,
                                        fontSize: font_size,
                                      ),
                                    ),
                                  ),
                                ) : Container(),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )
        : Container();
        // ? Column(
        //     children: <Widget>[
        //       const SizedBox(
        //         height: 10.0,
        //       ),
        //       Container(
        //         padding: const EdgeInsets.symmetric(horizontal: 15.0),
        //         alignment: Alignment.centerLeft,
        //         decoration: BoxDecoration(
        //           borderRadius: BorderRadius.circular(5.0),
        //           color: Theme.of(context).hoverColor,
        //         ),
        //         height: 58.0,
        //         child: SingleChildScrollView(
        //           scrollDirection: Axis.horizontal,
        //           child: Row(
        //             children: <Widget>[
        //               Text(
        //                 '$_fontResult : ',
        //                 style: const TextStyle(
        //                   fontSize: 16.0,
        //                   fontWeight: FontWeight.w600,
        //                 ),
        //               ),
        //               const SizedBox(
        //                 width: 10.0,
        //               ),
        //               Text(
        //                 name_controller.text,
        //                 style: GoogleFonts.getFont(
        //                   _font == 'Polos' ? 'Roboto' : _font,
        //                   textStyle: const TextStyle(
        //                     fontSize: 28.0,
        //                     fontWeight: FontWeight.w600,
        //                   ),
        //                 ),
        //               ),
        //             ],
        //           ),
        //         ),
        //       ),
        //     ],
        //   )
        // : Container();

    final BoxPengaman = _boxVis
        ? Container(
            alignment: Alignment.centerRight,
            child: Column(
              children: <Widget>[
                Container(
                  width: 150.0,
                  child: ListTileTheme(
                    contentPadding: const EdgeInsets.all(0),
                    child: CheckboxListTile(
                      title: Text(_boxPengamanLabel + ' : '),
                      activeColor: Theme.of(context).primaryColor,
                      value: _boxChecked,
                      selected: false,
                      onChanged: (newValue) {
                        isChanged = true;
                        setState(() {
                          _boxChecked = newValue;
                          if (_boxChecked) {
                            product.productPrice =
                                (int.parse(product.productPrice) + 5000).toString();
                            _boxPengaman = _boxPengamanLabel +
                                ' : ' +
                                (_specification[0]['wccpf_safety_box']
                                        ['choices'])
                                    .split('|')[1];
                            if (_boxPengaman.contains(';')) {
                              _boxPengaman = _boxPengaman.substring(
                                  0, _boxPengaman.indexOf(';'));
                            }
                            product.box = _boxPengaman;
                          } else {
                            product.productPrice =
                                (int.parse(product.productPrice) - 5000).toString();
                            product.box = '';
                          }
                        });
                      },
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30.0,
                ),
              ],
            ),
          )
        : Container();

    final FreeItemCheckBox = 
      ((categoryList.contains(freeCategory) && freeItemID == '0') 
      || (product.id == freeItemID && freeCategory == '0'))
      && getFree > 0
        ? Container(
            child: Column(
              children: [
                Container(
                  padding: const EdgeInsets.only(left: 10.0, top: 30.0),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    categoryName != ''
                      ? S.of(context).congratulation +  ', you get $getFree free $categoryName!'
                      : S.of(context).congratulation + ', you get $getFree free $itemName!',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600,
                      color: Theme.of(context).primaryColor,
                      height: 2.0,
                    ),
                  ),
                ),
                const SizedBox(height: 10.0),
                ListTileTheme(
                  contentPadding: const EdgeInsets.all(0),
                  child: CheckboxListTile(
                    title: Text(
                      S.of(context).takeMyFree(categoryName),
                      style: const TextStyle(
                        fontSize: 14.0,
                      ),
                    ),
                    activeColor: Theme.of(context).primaryColor,
                    value: _free,
                    onChanged: (newValue) {
                      setState(() {
                        isChanged = true;
                        _free = newValue;
                        if (_free) {
                          if (event['getSize'] != null) {
                            if (_ukuran.contains(event['getSize'])) {
                              product.regularPrice = product.productPrice;
                              product.productPrice = '0';
                            } 
                          } else {
                            product.regularPrice = product.productPrice;
                            product.productPrice = '0';
                          }
                        } else {
                          if (_priceList.isNotEmpty) {
                            for (int a = 0; a < _priceList.length; a++) {
                              if (_ukuran
                                  .contains(_priceList[a]['expected_value'])) {
                                if (_boxChecked) {
                                  product.productPrice =
                                      (int.parse(_priceList[a]['amount']) +
                                              5000)
                                          .toString();
                                } else {
                                  product.productPrice = _priceList[a]['amount'];
                                }
                              }
                            }
                          } else {
                            product.productPrice = product.regularPrice;
                          }
                        }
                      });
                    },
                    controlAffinity: ListTileControlAffinity.leading,
                  ),
                ),
              ],
            ),
          )
        : Container();

    final GetProductSpecificationWidget = Container(
      child: Column(
        children: <Widget>[
          const SizedBox(
            height: 20.0,
          ),
          Container(
            alignment: Alignment.topLeft,
            child: Column(
              children: <Widget>[
                Material,
                Ukuran,
                Sudut,
                Font,
                Nama,
                PrintNama,
                FreeItemCheckBox,
                const SizedBox(
                  height: 20.0,
                ),
              ],
            ),
          ),
          Harga,
          BoxPengaman,
        ],
      ),
    );
    //END NEW by Luthfan Alwafi

    // return Column(
    //   children: <Widget>[
    //     ...getProductTitleWidget(),
    //     ...getProductAttributeWidget(),
    //     ...getBuyButtonWidget(),
    //   ],
    // );

    //START New by Luthfan Alwafi
    return _isLoading == true
        ? Container(child: kLoadingWidget(context))
        : Column(
            children: <Widget>[
              // ...getProductTitleWidget(),
              ...getProductAttributeWidget(),
              GetProductSpecificationWidget,
              ...getBuyButtonWidget(),
            ],
          );
    //END NEW by Luthfan Alwafi
  }
}
