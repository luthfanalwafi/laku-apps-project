import 'package:flutter/material.dart';

import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../generated/l10n.dart';
import 'package:intl/intl.dart';

enum StatusOrder {
  // pendding,
  // onHold,
  // failed,
  // cancelled,
  // processing,
  // completed,
  // refunded

  //START NEW by Luthfan Alwafi
  onHold,
  pendding,
  waiting,
  failed,
  trash,
  cancelled,
  processing,
  completed,
  refunded,
  delivered,
  //END NEW by Luthfan Alwafi

}

class TimelineTracking extends StatefulWidget {
  final Axis axisTimeLine;
  final String status;
  final DateTime createdAt;
  final DateTime dateModified;

  //START NEW by Luthfan Alwafi
  final String trackingstatus;
  //END NEW by Luthfan Alwafi

  TimelineTracking(
      {Key key,
      this.axisTimeLine = Axis.vertical,
      this.status,
      this.createdAt,

      //START NEW by Luthfan Alwafi
      this.trackingstatus,
      //END NEW by Luthfan Alwafi

      this.dateModified})
      : super(key: key);
  @override
  _TimelineTrackingState createState() => _TimelineTrackingState();
}

class _TimelineTrackingState extends State<TimelineTracking> {
  // var statusOrderSuccessNotFail = [
  //   StatusOrder.pendding,
  //   StatusOrder.onHold,
  //   StatusOrder.processing,
  //   StatusOrder.completed
  // ];

  // var statusOrderSuccessIsFail = [
  //   StatusOrder.pendding,
  //   StatusOrder.failed,
  //   StatusOrder.processing,
  //   StatusOrder.completed
  // ];

  // var statusOrderSuccessRefunded = [
  //   StatusOrder.pendding,
  //   StatusOrder.onHold,
  //   StatusOrder.processing,
  //   StatusOrder.completed,
  //   StatusOrder.refunded
  // ];

  // var statusOrderCancel = [
  //   StatusOrder.pendding,
  //   StatusOrder.failed,
  //   StatusOrder.cancelled
  // ];

  //START NEW by Luthfan Alwafi
  var statusOrderPending = [
    StatusOrder.onHold,
    StatusOrder.pendding,
  ];

  var statusOrderHold = [
    StatusOrder.onHold,
  ];

  var statusOrderWaiting = [
    StatusOrder.onHold,
    StatusOrder.pendding,
    StatusOrder.waiting,
  ];

  var statusOrderProcessing = [
    StatusOrder.onHold,
    StatusOrder.pendding,
    StatusOrder.processing,
  ];

  var statusOrderonTrack = [
    StatusOrder.onHold,
    StatusOrder.pendding,
    StatusOrder.processing,
    StatusOrder.completed,
  ];

  var statusOrderSuccessNotFail = [
    StatusOrder.onHold,
    StatusOrder.pendding,
    StatusOrder.processing,
    StatusOrder.completed,
    StatusOrder.delivered,
  ];

  var statusOrderSuccessIsFail = [
    StatusOrder.onHold,
    StatusOrder.pendding,
    StatusOrder.processing,
    StatusOrder.completed,
    StatusOrder.failed,
  ];

  var statusOrderSuccessRefunded = [
    StatusOrder.onHold,
    StatusOrder.pendding,
    StatusOrder.processing,
    StatusOrder.completed,
    StatusOrder.delivered,
    StatusOrder.refunded,
  ];

  var statusOrderCancel = [
    StatusOrder.onHold,
    StatusOrder.pendding,
    StatusOrder.cancelled,
  ];

  var statusOrderTrash = [
    StatusOrder.onHold,
    StatusOrder.pendding,
    StatusOrder.trash,
  ];
  //END NEW by Luthfan Alwafi

  get isAxisVertical => widget.axisTimeLine == Axis.vertical;

  @override
  Widget build(BuildContext context) {
    Widget renderMain;
    if (isAxisVertical) {
      // renderMain = Column(children: _renderStatus(widget.status));

      //START NEW by Luthfan Alwafi
      if (widget.trackingstatus == 'DELIVERED'){
        renderMain = Column(children: _renderStatus('delivered'));
      }
      else {
        renderMain = Column(children: _renderStatus(widget.status));
      }
      //END NEW by Luthfan Alwafi

    } else {
      // renderMain = Row(children: _renderStatus(widget.status));

      //START NEW by Luthfan Alwafi
      if (widget.trackingstatus == 'DELIVERED'){
        renderMain = Row(children: _renderStatus('delivered'));
      }
      else {
        renderMain = Row(children: _renderStatus(widget.status));
      }
      //END NEW by Luthfan Alwafi

    }

    return Center(
      child: Container(
          width: MediaQuery.of(context).size.width,
          // padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 15),

          //START NEW by Luthfan Alwafi
          padding: const EdgeInsets.only(left: 5.0, right : 5.0, top: 20.0),
          //END NEW by Luthfan Alwafi

          color: Theme.of(context).primaryColor.withOpacity(0.1),
          child: SingleChildScrollView(
            child: renderMain,
            scrollDirection: isAxisVertical ? Axis.vertical : Axis.horizontal,
          )),
    );
  }

  // String converTime(DateTime datetime) {
  //   String month =
  //       datetime.month < 10 ? "0${datetime.month}" : "${datetime.month}";
  //   return "${datetime.day}.$month.${datetime.year}";
  // }

  //START NEW by Luthfan Alwafi
  String convertDate(DateTime datetime) {
    final dateFormat = DateFormat('MMM');
    String month = dateFormat.format(datetime);
        // datetime.month < 10 ? "0${datetime.month}" : "${datetime.month}";
    return ", ${datetime.day} $month ${datetime.year}";
  }

  String convertTime(DateTime datetime) {
    String hour = datetime.hour < 10 ? "0${datetime.hour}" : "${datetime.hour}";
    String minute =
        datetime.minute < 10 ? "0${datetime.minute}" : "${datetime.minute}";
    String second =
        datetime.second < 10 ? "0${datetime.second}" : "${datetime.second}";
    return "$hour:$minute:$second ${datetime.timeZoneName}";
  }
  //END NEW by Luthfan Alwafi

  Widget _renderItem(
      {int index,
      DateTime time,
      String title,
      String description,
      StatusOrder status,
      StatusOrder statusCurrent,
      bool isActive,
      bool showLine = true}) {

    Widget times = SizedBox(
      width: isAxisVertical ? MediaQuery.of(context).size.width * 0.2 : null,
      height: 25.0,
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(
          time != null ? convertTime(time) : "",
          textAlign: TextAlign.end,
          style: const TextStyle(fontSize: 12),
        ),
      ),
    );

    //START NEW by Luthfan Alwafi
    String date = time != null ? convertDate(time) : '';

    Image processImg = Image.network(
      'https://laku.com/wp-content/uploads/2020/08/check-solid.png',
      height: 18.0,
      fit: BoxFit.contain,
    );

    Image completedImg = Image.network(
      'https://laku.com/wp-content/uploads/2020/08/truck-solid.png',
      height: 18.0,
      fit: BoxFit.contain,
    );

    Image failedImg = Image.network(
      'https://laku.com/wp-content/uploads/2020/08/times-solid.png',
      height: 18.0,
      fit: BoxFit.contain,
    );
    //END NEW by Luthfan Alwafi

    List<Widget> contentInLine = [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5.0),
        child: Center(
          child: Container(
            // width: 25,
            // height: 25,

            //START NEW by Luthfan Alwafi
            width: 38,
            height: 38,
            //END NEW by Luthfan Alwafi

            decoration: BoxDecoration(
                color:
                    // isActive ? Theme.of(context).primaryColor : Colors.black54,
                    Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(20)),
            child: Center(
              // child: Text(
              //   "${index + 1}",
              //   style: const TextStyle(
              //     fontSize: 12,
              //     fontWeight: FontWeight.w700,
              //     color: Colors.white,
              //   ),
              // ),

              //START NEW by Luthfan Alwafi
              child:
                  status == StatusOrder.completed ? completedImg : 
                  status == StatusOrder.failed || status == StatusOrder.cancelled ? failedImg :
                  processImg,
              //END NEW by Luthfan Alwafi

            ),
          ),
        ),
      ),
      showLine
          ? _buildLine(true, isActive && status != statusCurrent)
          : const SizedBox()
    ];

    Widget content = Container(

      //START NEW by Luthfan Alwafi
      padding: const EdgeInsets.only(top: 10.0),
      //END NEW by Luthfan Alwafi

      width: MediaQuery.of(context).size.width * 0.5,
      height: description.isEmpty ? 25.0 : null,
      child: Align(
        alignment: Alignment.centerLeft,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              // title,
              // style: const TextStyle(fontSize: 12),

              //START NEW by Luthfan Alwafi
              title + date,
              style: TextStyle(
                  fontSize: MediaQuery.of(context).size.width * 0.025,
                  fontWeight: FontWeight.w800
                ),
              //END NEW by Luthfan Alwafi

            ),
            description.isEmpty
            ? Container(
                width: 0.0,
                height: 0.0,
              )
            : Text(
                description,
                style: const TextStyle(fontSize: 10),
              )
          ],
        ),
      ),
    );

    if (isAxisVertical) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // date,
          // Container(
          //   child: Column(
          //     children: contentInLine,
          //   ),
          // ),
          // const SizedBox(width: 4),
          // content

          //START NEW by Luthfan Alwafi
          Container(
            child: Column(
              children: contentInLine,
            ),
          ),
          const SizedBox(width: 5),
          content,
          times,
          //END NEW by Luthfan Alwafi

        ],
      );
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 120,
            child: content,
          ),
          Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              child: Row(children: contentInLine)),
          // date

          //START NEW by Luthfan Alwafi
          times
          //END NEW by Luthfan Alwafi

        ],
      );
    }
  }

  Widget _buildLine(bool visible, bool isActive) {
    return Container(
      width: isAxisVertical ? (visible ? 2.0 : 0.0) : double.infinity,
      height: isAxisVertical ? 30 : (visible ? 2.0 : 0.0),
      // color: isActive ? Theme.of(context).primaryColor : Colors.grey.shade400,

      //START NEW by Luthfan Alwafi
      color: Theme.of(context).primaryColor,
      //END NEW by Luthfan Alwafi

    );
  }

  List<Widget> _renderStatus(String _status) {
    List<Widget> listStatus = List();
    StatusOrder statusOrder;
    List<StatusOrder> flowHandleStatus = statusOrderSuccessNotFail;

    switch (_status) {
      // case "on-hold": //Thể hiện timeline : Pendding(active) -> On-Hold(active) -> Processing -> Completed
      //   statusOrder = StatusOrder.onHold;
      //   flowHandleStatus = statusOrderSuccessNotFail;
      //   break;
      // case "pendding": //Thể hiện timeline : Pendding(active) -> On-Hold -> Processing -> Completed
      //   statusOrder = StatusOrder.pendding;
      //   flowHandleStatus = statusOrderSuccessNotFail;
      //   break;
      // case "processing": //Thể hiện timeline : Pendding(active) -> On-Hold(active) -> Processing(active) -> Completed
      //   statusOrder = StatusOrder.processing;
      //   flowHandleStatus = statusOrderSuccessNotFail;
      //   break;
      // case "cancelled": //Thể hiện timeline : Pendding(active) -> Failed(active) -> Cancelled(active)
      //   statusOrder = StatusOrder.cancelled;
      //   flowHandleStatus = statusOrderCancel;
      //   break;
      // case "refunded": //Thể hiện timeline : Pendding(active) -> On-Hold(active) -> Processing(active) -> Completed(active) -> Refunded(active)
      //   statusOrder = StatusOrder.refunded;
      //   flowHandleStatus = statusOrderSuccessRefunded;
      //   break;

      // case "completed": //Thể hiện timeline : Pendding(active) -> On-Hold(active) -> Processing(active) -> Completed(active)
      //   statusOrder = StatusOrder.completed;
      //   flowHandleStatus = statusOrderSuccessNotFail;
      //   break;

      // case "failed": //Thể hiện timeline : Pendding(active) -> Failed(active) -> Processing -> Completed
      //   statusOrder = StatusOrder.failed;
      //   flowHandleStatus = statusOrderSuccessIsFail;
      //   break;
      // default:
      //   statusOrder = StatusOrder.pendding;
      //   flowHandleStatus = statusOrderSuccessNotFail;

      //START NEW by Luthfan Alwafi
      case "on-hold": //Thể hiện timeline : Pendding(active) -> On-Hold(active) -> Processing -> Completed
        statusOrder = StatusOrder.onHold;
        flowHandleStatus = statusOrderHold;
        break;
      case "waiting-confirm": //Thể hiện timeline : Pendding(active) -> On-Hold(active) -> waiting(active) -> Processing -> Completed
        statusOrder = StatusOrder.waiting;
        flowHandleStatus = statusOrderWaiting;
        break;
      case "pending": //Thể hiện timeline : Pendding(active) -> On-Hold -> Processing -> Completed
        statusOrder = StatusOrder.pendding;
        flowHandleStatus = statusOrderPending;
        break;
      case "processing": //Thể hiện timeline : Pendding(active) -> On-Hold(active) -> Processing(active) -> Completed
        statusOrder = StatusOrder.processing;
        flowHandleStatus = statusOrderProcessing;
        break;
      case "cancelled": //Thể hiện timeline : Pendding(active) -> Failed(active) -> Cancelled(active)
        statusOrder = StatusOrder.cancelled;
        flowHandleStatus = statusOrderCancel;
        break;
      case "refunded": //Thể hiện timeline : Pendding(active) -> On-Hold(active) -> Processing(active) -> Completed(active) -> Refunded(active)
        statusOrder = StatusOrder.refunded;
        flowHandleStatus = statusOrderSuccessRefunded;
        break;
      case "completed": //Thể hiện timeline : Pendding(active) -> On-Hold(active) -> Processing(active) -> Completed(active)
        statusOrder = StatusOrder.completed;
        flowHandleStatus = statusOrderonTrack;
        break;
      case "delivered": //Thể hiện timeline : Pendding(active) -> On-Hold(active) -> Processing(active) -> Completed(active)
        statusOrder = StatusOrder.delivered;
        flowHandleStatus = statusOrderSuccessNotFail;
        break;
      case "failed": //Thể hiện timeline : Pendding(active) -> Failed(active) -> Processing -> Completed
        statusOrder = StatusOrder.failed;
        flowHandleStatus = statusOrderSuccessIsFail;
        break;
      case "trash": //Thể hiện timeline : Pendding(active) -> Failed(active) -> Processing -> Completed
        statusOrder = StatusOrder.failed;
        flowHandleStatus = statusOrderSuccessIsFail;
        break;
      default:
        statusOrder = StatusOrder.pendding;
        flowHandleStatus = statusOrderSuccessNotFail;
      //END NEW by Luthfan Alwafi

    }

    for (var i = 0; i < flowHandleStatus.length; i++) {
      listStatus.add(_renderItem(
          index: i,
          statusCurrent: statusOrder,
          isActive: i <= flowHandleStatus.indexOf(statusOrder),
          title: getTitleStatus(flowHandleStatus[i]),
          showLine: i < (flowHandleStatus.length - 1),
          status: flowHandleStatus[i],
          time: i == 0
              ? widget.createdAt
              : (i == flowHandleStatus.indexOf(statusOrder)
                  ? widget.dateModified
                  : null),
          description: ""));
    }
    return listStatus;
  }

  Color getColor(StatusOrder status) {
    switch (status) {
      // case StatusOrder.onHold:
      //   return HexColor(kOrderStatusColor["on-hold"]);
      // case StatusOrder.pendding:
      //   return HexColor(kOrderStatusColor["pendding"]);
      // case StatusOrder.failed:
      //   return HexColor(kOrderStatusColor["failed"]);
      // case StatusOrder.completed:
      //   return HexColor(kOrderStatusColor["completed"]);
      // case StatusOrder.cancelled:
      //   return HexColor(kOrderStatusColor["cancelled"]);
      // case StatusOrder.refunded:
      //   return HexColor(kOrderStatusColor["refunded"]);
      // case StatusOrder.processing:
      //   return HexColor(kOrderStatusColor["processing"]);
      // default:
      //   return Colors.white;

      //START NEW by Luthfan Alwafi
      case StatusOrder.onHold:
        return HexColor(kOrderStatusColor["on-hold"]);
      case StatusOrder.waiting:
        return HexColor(kOrderStatusColor["waiting"]);
      case StatusOrder.pendding:
        return HexColor(kOrderStatusColor["pendding"]);
      case StatusOrder.failed:
        return HexColor(kOrderStatusColor["failed"]);
      case StatusOrder.trash:
        return HexColor(kOrderStatusColor["trash"]);
      case StatusOrder.completed:
        return HexColor(kOrderStatusColor["completed"]);
      case StatusOrder.delivered:
        return HexColor(kOrderStatusColor["shipped"]);
      case StatusOrder.cancelled:
        return HexColor(kOrderStatusColor["cancelled"]);
      case StatusOrder.refunded:
        return HexColor(kOrderStatusColor["refunded"]);
      case StatusOrder.processing:
        return HexColor(kOrderStatusColor["processing"]);
      default:
        return Colors.white;
      //END NEW by Luthfan Alwafi

    }
  }

  String getTitleStatus(StatusOrder status) {
    switch (status) {
      // case StatusOrder.onHold:
      //   return S.of(context).orderStatusOnHold;
      // case StatusOrder.pendding:
      //   return S.of(context).orderStatusPendingPayment;
      // case StatusOrder.failed:
      //   return S.of(context).orderStatusFailed;
      // case StatusOrder.processing:
      //   return S.of(context).orderStatusProcessing;
      // case StatusOrder.completed:
      //   return S.of(context).orderStatusCompleted;
      // case StatusOrder.cancelled:
      //   return S.of(context).orderStatusCancelled;
      // case StatusOrder.refunded:
      //   return S.of(context).orderStatusRefunded;
      // default:
      //   return "";
      
      //START NEW by Luthfan Alwafi
      case StatusOrder.onHold:
        return S.of(context).orderStatusOnHold;
      case StatusOrder.waiting:
        return S.of(context).orderStatusWaiting;
      case StatusOrder.pendding:
        return S.of(context).orderStatusPendingPayment;
      case StatusOrder.failed:
        return S.of(context).orderStatusFailed;
      case StatusOrder.trash:
        return S.of(context).orderStatusTrash;
      case StatusOrder.processing:
        return S.of(context).orderStatusProcessing;
      case StatusOrder.completed:
        return S.of(context).orderStatusCompleted;
      case StatusOrder.delivered:
        return S.of(context).orderStatusDelivered;
      case StatusOrder.cancelled:
        return S.of(context).orderStatusCancelled;
      case StatusOrder.refunded:
        return S.of(context).orderStatusRefunded;
      default:
        return "";
      //END NEW by Luthfan Alwafi
    }
  }
}
