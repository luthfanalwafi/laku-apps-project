import 'package:flutter/material.dart';
import 'package:fstore/app.dart';
import 'package:fstore/models/cart/cart_base.dart';
import 'package:provider/provider.dart';

import '../../common/config/products.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../generated/l10n.dart' show S;
import '../../models/index.dart' show AppModel, Product, ProductVariation;
import '../../services/index.dart';
import 'product_variant.dart';


class ShoppingCartRow extends StatelessWidget {
  ShoppingCartRow({
    @required this.product,
    @required this.quantity,
    this.onRemove,
    this.onChangeQuantity,
    this.variation,
    this.options,

    //START NEW by Luthfan Alwafi
    this.token,
    //END NEW by Luthfan Alwafi

  });

  final Product product;
  final ProductVariation variation;
  final Map<String, dynamic> options;
  final int quantity;
  final Function onChangeQuantity;
  final VoidCallback onRemove;

  //START NEW by Luthfan Alwafi
  final String token;
  //END NEW by Luthfan Alwafi

  @override
  Widget build(BuildContext context) {
    String currency = Provider.of<AppModel>(context).currency;
    final currencyRate = Provider.of<AppModel>(context).currencyRate;

    // final price = Services()
    //     .widget
    //     .getPriceItemInCart(product, variation, currencyRate, currency);
    final imageFeature = variation != null && variation.imageFeature != null
        ? variation.imageFeature
        : product.imageFeature;
    int maxQuantity = kCartDetail['maxAllowQuantity'] ?? 100;
    int totalQuantity = variation != null
        ? (variation.stockQuantity ?? maxQuantity)
        : (product.stockQuantity ?? maxQuantity);
    int limitQuantity =
        totalQuantity > maxQuantity ? maxQuantity : totalQuantity;

    ThemeData theme = Theme.of(context);

    //START NEW by Luthfan Alwafi
    product.productToken = token;
    product.price = product.spec[product.productToken]['Harga'];
    final price = Services().widget.getPriceItemInCart(product, variation, currencyRate, currency);
    String _session = store.get("session").toString();
    // String _session = 'WEB210218';
    String _material = '';
    if (product.dealToday == '' || _session == product.dealToday) {
      _material = product.spec[product.productToken]['Material'];
    }
    else {
      _material = 'Expired';
    }
    // String _material = product.dealToday == '' ? product.spec[product.productToken]['Material'] :
    //                    _session == product.dealToday ? product.spec[product.productToken]['Material'] : 'Expired';
    String _finishing = _material == 'Expired' ? '' : product.spec[product.productToken]['Finishing'];
    String _ukuran = _material == 'Expired' ? '' : product.spec[product.productToken]['Ukuran'];
    String _sudut = _material == 'Expired' ? '' : product.spec[product.productToken]['Sudut'];
    String _font = _material == 'Expired' ? '' : product.spec[product.productToken]['Font'];
    String _cetak = _material == 'Expired' ? '' : product.spec[product.productToken]['Cetak'];
    String _box = _material == 'Expired' ? '' : product.spec[product.productToken]['Box'];
    if (_material != 'Expired') {
      product.spec[product.productToken]['IsExpired'] = false;
    }
    else {
      product.spec[product.productToken]['IsExpired'] = true;
    }
    // product.spec[product.productToken]['Ukuran'] = _material == 'Expired' ? 'Expired' : product.spec[product.productToken]['Ukuran'];
    // product.spec[product.productToken]['Sudut'] = _material == 'Expired' ? 'Expired' : product.spec[product.productToken]['Sudut'];
    // product.spec[product.productToken]['Font'] = _material == 'Expired' ? 'Expired' : product.spec[product.productToken]['Font'];
    // product.spec[product.productToken]['Cetak'] = _material == 'Expired' ? 'Expired' : product.spec[product.productToken]['Cetak'];
    //END NEW by Luthfan Alwafi

    return LayoutBuilder(
      builder: (context, constraints) {
        return Column(
          children: [
            Row(
              key: ValueKey(product.id),
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                if (onRemove != null)
                  IconButton(
                    icon: const Icon(Icons.remove_circle_outline),
                    onPressed: onRemove,
                  ),
                Expanded(

                  //START BEW by Luthfan Alwafi
                  child: Padding(
                    padding: const EdgeInsets.only(right: 16.0),
                  //END NEW by Luthfan Alwafi

                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Column(
                          children: [
                            Container(
                              width: constraints.maxWidth * 0.25,
                              // height: constraints.maxWidth * 0.3,

                              //START NEW by Luthfan Alwafi
                              height: constraints.maxWidth * 0.32,
                              //END NEW by Luthfan Alwafi
                              
                              child: Tools.image(url: imageFeature),
                            ),

                            //START NEW by Luthfan Alwafi
                            Container(
                              decoration: BoxDecoration(
                                border: Border.all(width: 1.0, color: kGrey200),
                                color: Theme.of(context).backgroundColor,
                                borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(2.0)),
                              ),
                              child: QuantitySelection(
                                enabled: onChangeQuantity != null,
                                width: constraints.maxWidth * 0.25,
                                height: 32,
                                color: Theme.of(context).accentColor,
                                limitSelectQuantity: limitQuantity,
                                value: quantity,
                                onChanged: onChangeQuantity,
                                useNewDesign: false,
                                token: token,
                              ),
                            ),
                            //END NEW by Luthfan Alwafi

                          ],
                        ),
                        const SizedBox(width: 16.0),
                        Expanded(
                          child: SingleChildScrollView(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [

                                //START NEW by Luthfan ALwafi
                                const SizedBox(height: 12),
                                //END NEW by Luthfan Alwafi
                                
                                Text(
                                  product.name,
                                  style: TextStyle(
                                    color: theme.accentColor,

                                    //START NEW by Luthfan ALwafi
                                    fontWeight: FontWeight.bold,
                                    //END NEW by Luthfan Alwafi

                                  ),
                                  maxLines: 4,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                const SizedBox(height: 7),
                                Text(
                                  price,
                                  style: TextStyle(
                                    color: theme.accentColor, fontSize: 14,

                                    //START NEW by Luthfan ALwafi
                                    fontWeight: FontWeight.bold,
                                    //END NEW by Luthfan Alwafi

                                  ),
                                ),

                              //START NEW by Luthfan Alwafi
                              _material != ''
                                  ? Column(
                                      children: <Widget>[
                                        const SizedBox(height: 12),
                                        Text(
                                          '$_material',
                                          style: TextStyle(
                                              color: theme.accentColor,
                                              fontSize: 12),
                                        ),
                                      ],
                                    )
                                  : Container(),

                              _material != ''
                                  ? Column(
                                      children: <Widget>[
                                        const SizedBox(height: 7),
                                        Text(
                                          '$_finishing',
                                          style: TextStyle(
                                              color: theme.accentColor,
                                              fontSize: 12),
                                        ),
                                      ],
                                    )
                                  : Container(),

                              _ukuran != ''
                                  ? Column(
                                      children: <Widget>[
                                        const SizedBox(height: 7),
                                        Text(
                                          '$_ukuran',
                                          style: TextStyle(
                                              color: theme.accentColor,
                                              fontSize: 12),
                                        ),
                                      ],
                                    )
                                  : Container(),

                              _sudut != ''
                                  ? Column(
                                      children: <Widget>[
                                        const SizedBox(height: 7),
                                        Text(
                                          '$_sudut',
                                          style: TextStyle(
                                              color: theme.accentColor,
                                              fontSize: 12),
                                        ),
                                      ],
                                    )
                                  : Container(),

                              _sudut.contains('Polos') || _sudut.contains('Tanpa Sudut' ) || _font == ''
                                  ? Container()
                                  : Column(
                                      children: <Widget>[
                                        const SizedBox(height: 7),
                                        Text(
                                          '$_font',
                                          style: TextStyle(
                                              color: theme.accentColor,
                                              fontSize: 12),
                                        ),
                                      ],
                                    ),

                              _sudut.contains('Polos') || _sudut.contains('Tanpa Sudut')
                                  ? Container()
                                  : Column(
                                      children: <Widget>[
                                        const SizedBox(height: 7),
                                        Text(
                                          '$_cetak',
                                          style: TextStyle(
                                              color: theme.accentColor,
                                              fontSize: 12),
                                        ),
                                      ],
                                    ),
                                 
                              _box != ''
                                  ? Column(
                                      children: <Widget>[
                                        const SizedBox(height: 7),
                                        Text(
                                          '$_box',
                                          style: TextStyle(
                                              color: theme.accentColor,
                                              fontSize: 12),
                                        ),
                                      ],
                                    )
                                  : Container(),
                              //END NEW by Luthfan Alwafi

                                const SizedBox(height: 10),
                                variation != null || options != null
                                    ? Services()
                                      .widget
                                      .renderVariantCartItem(variation, options)
                                    : Container(),
                                // QuantitySelection(
                                //   enabled: onChangeQuantity != null,
                                //   width: 60,
                                //   height: 32,
                                //   color: Theme.of(context).accentColor,
                                //   limitSelectQuantity: limitQuantity,
                                //   value: quantity,
                                //   onChanged: onChangeQuantity,
                                //   useNewDesign: false,
                                // ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(width: 16.0),
              ],
            ),
            const SizedBox(height: 10.0),
            const Divider(color: kGrey200, height: 1),
            const SizedBox(height: 10.0),
          ],
        );
      },
    );
  }
}
