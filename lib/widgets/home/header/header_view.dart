import 'package:flutter/material.dart';
import 'package:fstore/common/constants/colors.dart';

import '../../../generated/l10n.dart';
import '../../common/countdown_timer.dart';

class HeaderView extends StatelessWidget {
  final String headerText;
  final VoidCallback callback;
  final bool showSeeAll;
  final bool showCountdown;
  final Duration countdownDuration;
  final double margin;

  HeaderView({
    this.headerText,
    this.showSeeAll = false,
    Key key,
    this.callback,
    this.margin = 15.0,
    this.showCountdown = false,
    this.countdownDuration = const Duration(),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Container(
      width: screenSize.width,
      color: Theme.of(context).backgroundColor,
      child: Container(
        width: screenSize.width / (2 / (screenSize.height / screenSize.width)),
        margin: EdgeInsets.only(top: margin),
        padding: const EdgeInsets.only(
          left: 17.0,
          right: 15.0,
          // top: margin,
          // bottom: margin,
          
          //START NEW by Luthfan Alwafi
          top: 15.0,
          bottom: 20.0,
          //END NEW by Luthfan Alwafi

        ),
        child: Row(
          textBaseline: TextBaseline.alphabetic,
          crossAxisAlignment: CrossAxisAlignment.baseline,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    headerText ?? '',
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  // if (showCountdown)
                    // Row(
                    //   children: [
                    //     Text(
                    //       S.of(context).endsIn("").toUpperCase(),
                    //       style: Theme.of(context)
                    //           .textTheme
                    //           .subtitle1
                    //           .copyWith(
                    //             color: Theme.of(context)
                    //                 .accentColor
                    //                 .withOpacity(0.8),
                    //           )
                    //           .apply(fontSizeFactor: 0.6),
                    //     ),
                    //     CountDownTimer(countdownDuration),
                    //   ],
                    // ),

                  //START NEW by Luthfan Alwafi
                  if (showCountdown)
                  const SizedBox(height: 10.0),
                  CountDownTimer(
                    countdownDuration, 
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                  ),
                  //END NEW by Luthfan Alwafi

                ],
              ),
            ),
            if (showSeeAll)
              InkResponse(
                onTap: callback,
                child: Text(
                  S.of(context).seeAll,
                  // style: Theme.of(context)
                  //     .textTheme
                  //     .bodyText1
                      // .copyWith(color: Theme.of(context).primaryColor),

                  //START NEW by Luthfan Alwafi
                  style: const TextStyle(
                      fontWeight: FontWeight.w400,
                      // color: Theme.of(context).primaryColor),
                      color: kColorSecondary,
                  ),
                  //END NEW by Luthfan Alwafi

                ),
              ),
          ],
        ),
      ),
    );
  }
}
