import 'package:flutter/material.dart';
import 'package:fstore/screens/index.dart';
import 'package:fstore/widgets/home/search/home_search_page.dart';
import '../../../common/constants/route_list.dart';
import '../../../common/tools.dart';


class HeaderSearch extends StatefulWidget {
  final config;

  HeaderSearch({this.config, Key key}) : super(key: key);

  @override
  _HeaderSearchState createState() => _HeaderSearchState();
}

class _HeaderSearchState extends State<HeaderSearch> {

  //START NEW By Luthfan Alwafi
  var sugestionList;
  String sugestion;
  int sugestNum;

  @override
  void initState() {
    super.initState();

    sugestionList = widget.config["searchSuggestion"] ?? '';
    if(sugestionList != '') {
      sugestionList.shuffle();
      sugestion = sugestionList[0];
      sugestNum = 0;
    }
  }

  @override
  void dispose() {
    super.dispose();
  }
  //END NEW By Luthfan Alwafi

  @override
  Widget build(BuildContext context) {
    final text = widget.config["text"] ?? '';

    return Container(
      padding: EdgeInsets.all(Tools.formatDouble(widget.config['padding'] ?? 20.0)),
      width: MediaQuery.of(context).size.width,
      height: Tools.formatDouble(widget.config['height'] ?? 85.0),
      child: SafeArea(
        bottom: false,
        top: widget.config['isSafeArea'] == true,
        child: InkWell(
          onTap: () {
            // Navigator.of(context, rootNavigator: true).pushNamed(RouteList.homeSearch);
            
            //START NEW By Luthfan Alwafi
            Navigator.of(context, rootNavigator: true).pushNamed(
              RouteList.homeSearch,
              arguments: <String, String> {
                'hint': sugestion,
              }
            );

            setState(() {
              if (sugestionList != '') {
                sugestNum++;
                if (sugestNum < sugestionList.length) {
                  sugestNum = sugestNum;
                  sugestion = sugestionList[sugestNum];
                }
                else {
                  sugestNum = 0;
                  sugestion = sugestionList[sugestNum];
                };
              }
            });
            //END NEW By Luthfan Alwafi

          },
          child: Container(
            padding: const EdgeInsets.only(left: 15.0, right: 15.0),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColorLight,
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  blurRadius: Tools.formatDouble(widget.config['shadow'] ?? 15.0),
                  offset:
                      Offset(0, Tools.formatDouble(widget.config['shadow'] ?? 10.0)),
                ),
              ],
              borderRadius: BorderRadius.circular(
                Tools.formatDouble(widget.config['radius'] ?? 30.0),
              ),
              border: Border.all(
                width: 1.0,
                color: Colors.black.withOpacity(0.05),
              ),
            ),
            child: Row(
              children: <Widget>[
                const Icon(Icons.search, size: 24),
                const SizedBox(
                  width: 12.0,
                ),
                // Text(text)

                //START NEW By Luthfan Alwafi
                Flexible(
                  child: Container(
                    padding: const EdgeInsets.only(right: 15.0),
                    child: Opacity(
                      opacity: 0.5,
                      child: Text(
                        sugestion == null
                          ? text
                          : "Search : " + sugestion,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                ),
                //END NEW By Luthfan Alwafi

              ],
            ),
          ),
        ),
      ),
    );
  }
}
