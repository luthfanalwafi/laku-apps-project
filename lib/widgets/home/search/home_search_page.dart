import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:fstore/screens/search/widgets/search_result.dart';
import 'package:provider/provider.dart';

import '../../../app.dart';
import '../../../common/constants.dart';
import '../../../generated/l10n.dart';
import '../../../models/index.dart'
    show
        AppModel,
        // CategoryModel,
        // FilterAttributeModel,
        // FilterTagModel,
        SearchModel;
import '../../../screens/index.dart'
    show
        // FilterSearch,
        RecentSearchesCustom,
        SearchBox,
        SearchResultsCustom;
import '../../common/auto_hide_keyboard.dart';

class HomeSearchPage extends StatefulWidget {
  HomeSearchPage();
  @override
  State<StatefulWidget> createState() => _HomeSearchPageState();
}

class _HomeSearchPageState<T> extends State<HomeSearchPage> {
  // This node is owned, but not hosted by, the search page. Hosting is done by
  // the text field.
  final _searchFieldNode = FocusNode();
  final _searchFieldController = TextEditingController();

  bool isVisibleSearch = false;
  bool _showResult = false;
  List<String> _suggestSearch;

  SearchModel get _searchModel =>
      Provider.of<SearchModel>(context, listen: false);

  String get _searchKeyword => _searchFieldController.text;

  //
  List<String> get suggestSearch =>
      _suggestSearch
          ?.where((s) => s.toLowerCase().contains(_searchKeyword.toLowerCase()))
          ?.toList() ??
      <String>[];

  @override
  void initState() {
    super.initState();
    _searchFieldNode.addListener(() {
      if (_searchKeyword.isEmpty && !_searchFieldNode.hasFocus) {
        _showResult = false;
      } else {
        _showResult = !_searchFieldNode.hasFocus;
      }
    });
  }

  @override
  void dispose() {
    _searchFieldNode?.dispose();
    _searchFieldController.dispose();
    // _searchModel.dispose();
    super.dispose();
  }

  void _onSearchTextChange(String value) {
    if (value.isEmpty) {
      _showResult = false;
      setState(() {});
      return;
    }

    // if (_searchFieldNode.hasFocus) {
    //   if (suggestSearch.isEmpty) {
    //     setState(() {
    //       _showResult = true;
    //       _searchModel.loadProduct(name: value);
    //     });
    //   } else {
    //     setState(() {
    //       _showResult = false;
    //     });
    //   }
    // }

    //START NEW by Luthfan Alwafi
    else {
      setState(() {
        _showResult = true;
        _searchModel.loadProduct(name: value);
      });
    }
    //END NEW by LuthfanAlwafi
  }

  @override
  Widget build(BuildContext context) {
    assert(debugCheckHasMaterialLocalizations(context));
    var theme = Theme.of(context);
    theme = Theme.of(context).copyWith(
      primaryColor: Colors.white,
      primaryIconTheme: theme.primaryIconTheme.copyWith(color: Colors.grey),
      primaryColorBrightness: Brightness.light,
      primaryTextTheme: theme.textTheme,
    );
    final String searchFieldLabel =
        MaterialLocalizations.of(context).searchFieldLabel;
    final List<String> suggestSearch = List<String>.from(
        Provider.of<AppModel>(context).appConfig['searchSuggestion'] ?? ['']);

    String routeName = isIos ? '' : searchFieldLabel;

    _suggestSearch = List<String>.from(
        Provider.of<AppModel>(context).appConfig['searchSuggestion'] ?? ['']);

    //START NEW by Luthfan Alwafi
    final Map list = ModalRoute.of(context).settings.arguments;
    String hint = list != null ? list['hint'] : '';
    //END NEW by Luthfan Alwafi

    return Semantics(
      explicitChildNodes: true,
      scopesRoute: true,
      namesRoute: true,
      label: routeName,
      child: Scaffold(
        backgroundColor: theme.backgroundColor,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: theme.backgroundColor,
          iconTheme: theme.primaryIconTheme,
          textTheme: theme.primaryTextTheme,
          brightness: theme.primaryColorBrightness,
          titleSpacing: 0,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios, size: 20),
            onPressed: close,
          ),
          title: SearchBox(
            showSearchIcon: false,
            showCancelButton: false,
            autoFocus: true,
            controller: _searchFieldController,
            focusNode: _searchFieldNode,
            onChanged: _onSearchTextChange,
            onSubmitted: _onSubmit,

            //START NEW by Luthfan Alwafi
            hint: hint,
            //END NEW by Luthfan Alwafi
          ),
          actions: _buildActions(),
        ),
        body: AutoHideKeyboard(
          child: Column(
            children: [
              // Align(
              //   alignment: Alignment.topLeft,
              //   child: Padding(
              //     padding: const EdgeInsets.only(left: 15, top: 8),
              //     child: Container(
              //       height: 32,
              //       child: FilterSearch(
              //         onChange: (searchFilter) {
              //           _searchModel.searchByFilter(
              //             searchFilter,
              //             _searchKeyword,
              //           );
              //         },
              //       ),
              //     ),
              //   ),
              // ),

              //START NEW by Luthfan Alwafi
              _searchFieldController.text == null ||
                      _searchFieldController.text == ''
                  ? Container()
                  : const Padding(
                      padding: EdgeInsets.only(top: 20.0),
                      child: Divider(
                        height: 2.0,
                        thickness: 3.0,
                      ),
                    ),
              _searchFieldController.text == null ||
                      _searchFieldController.text == ''
                  ? Container()
                  : Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: Container(
                        child: ListTile(
                          onTap: () {
                            print(_searchFieldController.text);
                            Navigator.of(context, rootNavigator: true).pushNamed(
                              RouteList.searchResult,
                            );
                            store.set('searchKeyword', _searchFieldController.text);
                          },
                          leading: const Padding(
                            padding: EdgeInsets.only(right: 10.0),
                            child: Icon(
                              Icons.search,
                              size: 50.0,
                            ),
                          ),
                          title: Container(
                            height: 2.0,
                            child: TextField(
                              controller: _searchFieldController,
                              style: const TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                              readOnly: true,
                              decoration: const InputDecoration(
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                          subtitle: Opacity(
                            opacity: 0.7,
                            child: Text(S.of(context).searchForItems),
                          ),
                          trailing: const Icon(
                            Icons.arrow_forward_ios,
                            size: 20.0,
                          ),
                        ),
                      ),
                    ),
              _searchFieldController.text == null ||
                      _searchFieldController.text == ''
                  ? Container()
                  : const Divider(
                      height: 2.0,
                      thickness: 3.0,
                    ),
              //END NEW by Luthfan Alwafi

              const SizedBox(height: 8),
              Expanded(
                child: AnimatedSwitcher(
                  duration: const Duration(milliseconds: 300),
                  reverseDuration: const Duration(milliseconds: 300),
                  child: _showResult
                      ? buildResult()
                      // : Align(
                      //     alignment: Alignment.topCenter,
                      //     child: Consumer<FilterTagModel>(
                      //       builder: (context, tagModel, child) {
                      //         return Consumer<CategoryModel>(
                      //           builder: (context, categoryModel, child) {
                      //             return Consumer<FilterAttributeModel>(
                      //               builder: (context, attributeModel, child) {
                      //                 if (tagModel.isLoading ||
                      //                     categoryModel.isLoading ||
                      //                     attributeModel.isLoading) {
                      //                   return kLoadingWidget(context);
                      //                 }
                      //                 var child = _buildRecentSearch();

                      //                 if (_searchFieldNode.hasFocus &&
                      //                     suggestSearch.isNotEmpty) {
                      //                   child = _buildSuggestions();
                      //                 }

                      //                 return child;
                      //               },
                      //             );
                      //           },
                      //         );
                      //       },
                      //     ),
                      //   ),

                      //START NEW by Luthfan Alwafi
                      : Align(
                          alignment: Alignment.topCenter,
                          child: SingleChildScrollView(
                            child: Column(
                              children: [
                                suggestSearch.isNotEmpty
                                    ? _buildSuggestions()
                                    : Container(),
                                _buildRecentSearch(),
                              ],
                            ),
                          ),
                        ),
                      //END NEW by Luthfan Alwafi
                      
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildRecentSearch() {
    return RecentSearchesCustom(onTap: _onSubmit);
  }

  // Widget _buildSuggestions() {
  //   return Card(
  //     elevation: 0,
  //     margin: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
  //     color: Theme.of(context).primaryColorLight,
  //     child: ListView.builder(
  //       shrinkWrap: true,
  //       padding: const EdgeInsets.only(
  //         left: 10,
  //         right: 10,
  //       ),
  //       itemCount: suggestSearch.length,
  //       itemBuilder: (_, index) {
  //         final keyword = suggestSearch[index];
  //         return GestureDetector(
  //           onTap: () => _onSubmit(keyword),
  //           child: ListTile(
  //             title: Text(keyword),
  //           ),
  //         );
  //       },
  //     ),
  //   );
  // }

  //START NEW by Luthfan Alwafi
  Widget _buildSuggestions() {
    final length = _suggestSearch.length;
    return Column(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          margin: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 8),
          child: Text(
            S.of(context).searchSuggestion,
            style: const TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        Container(
          alignment: Alignment.centerLeft,
          margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          child: Wrap(
            alignment: WrapAlignment.start,
            children: List.generate(
              length,
              (index) {
                final keyword = _suggestSearch[index];
                return GestureDetector(
                  onTap: () {
                    Navigator.of(context, rootNavigator: true).pushNamed(
                      RouteList.searchResult,
                    );
                    store.set('searchKeyword', keyword);
                  },
                  child: Card(
                    elevation: 0,
                    margin:
                        const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                    color: Theme.of(context).primaryColorLight,
                    child: Container(
                      margin: const EdgeInsets.all(10.0),
                      child: Text(
                        keyword,
                        style: const TextStyle(
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
            // Card(
            //   elevation: 0,
            //   margin: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
            //   color: Theme.of(context).primaryColorLight,
            // child: ListView.builder(
            //   shrinkWrap: true,
            //   padding: const EdgeInsets.only(
            //     left: 10,
            //     right: 10,
            //   ),
            //   itemCount: suggestSearch.length,
            //   itemBuilder: (_, index) {
            //     final keyword = suggestSearch[index];
            //     return GestureDetector(
            //       onTap: () => _onSubmit(keyword),
            //       child: ListTile(
            //         title: Text(keyword),
            //       ),
            //     );
            //   },
            // ),
            // ),
          ),
        ),
      ],
    );
  }
  //END NEW by Luthfan ALwafi

  Widget buildResult() {
    return SearchResultsCustom(
      name: _searchKeyword,
    );
  }

  List<Widget> _buildActions() {
    return <Widget>[
      _searchFieldController.text.isEmpty
          ? IconButton(
              tooltip: 'Search',
              icon: const Icon(Icons.search),
              onPressed: () {},
            )
          : IconButton(
              tooltip: 'Clear',
              icon: const Icon(Icons.clear),
              onPressed: () {
                _searchFieldController.clear();
                _searchFieldNode.requestFocus();
              },
            ),
    ];
  }

  void _onSubmit(String name) {
    _searchFieldController.text = name;
    // setState(() {
    //   _showResult = true;
    //   _searchModel.loadProduct(name: name);
    // });

    // FocusScopeNode currentFocus = FocusScope.of(context);
    // if (!currentFocus.hasPrimaryFocus) {
    //   currentFocus.unfocus();
    // }

    //START NEW by Luthfan ALwafi
    Navigator.of(context, rootNavigator: true).pushNamed(
      RouteList.searchResult,
    );
    store.set('searchKeyword', name);
    //END NEW by Luthfan Alwafi

  }

  void close() {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    Navigator.of(context).pop();
  }
}
