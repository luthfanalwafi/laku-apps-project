import 'package:flutter/material.dart';


class Ongkir extends StatelessWidget {
  final config;

  Ongkir({
    Key key,
    @required this.config,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final isRotate = screenSize.width > screenSize.height;

    return Builder(
      builder: (context) {
        return Container(
          width: screenSize.width /
              ((isRotate ? 1.25 : 2) /
                  (screenSize.height / screenSize.width)),
          constraints: const BoxConstraints(
            minHeight: 40.0,
          ),
          child: Padding(
            padding: const EdgeInsets.only(
              top: 30.0,
              bottom: 10.0,
              left: 30.0,
              right: 10.0,
            ),
            child: Text(
              config["message"],
              style: TextStyle(
                color: Theme.of(context).accentColor,
                fontSize: screenSize.width * 0.03,
              ),
            ),
          ),
        );
      },
    );
  }
}
