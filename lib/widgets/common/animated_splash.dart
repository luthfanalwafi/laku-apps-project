import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fstore/common/tools.dart';

Function _next;
Function _customFunction;
String _imagePath;
int _duration;
AnimatedSplashType _runfor;
bool _isPushNext;

//START NEW by Luthfan Alwafi
// String _backgroundColor;
//END NEW by Luthfan Alwafi

enum AnimatedSplashType { StaticDuration, BackgroundProcess }

Map<dynamic, Widget> _outputAndHome = {};

class AnimatedSplash extends StatefulWidget {
  AnimatedSplash({
    @required String imagePath,
    @required Function next,

    //START NEW by Luthfan Alwafi
    // @required String backgroundColor,
    //END NEW by Luthfan Alwafi

    Function customFunction,
    @required int duration,
    AnimatedSplashType type,
    Map<dynamic, Widget> outputAndHome,
    bool isPushNext = true,
  }) {
    assert(duration != null);
    assert(imagePath != null);
    _next = next;
    _duration = duration;
    _customFunction = customFunction;
    _imagePath = imagePath;

    //START NEW by Luthfan Alwafi
    // _backgroundColor = backgroundColor;
    //END NEW by Luthfan Alwafi

    _runfor = type;
    _outputAndHome = outputAndHome;
    _isPushNext = isPushNext;
  }

  @override
  _AnimatedSplashState createState() => _AnimatedSplashState();
}

class _AnimatedSplashState extends State<AnimatedSplash>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation _animation;

  @override
  void initState() {
    super.initState();
    if (_duration < 1000) _duration = 2000;
    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 800));
    _animation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: _animationController, curve: Curves.easeInCirc));

    //START NEW By Luthfan Alwafi
    // _animation = Tween<double>(begin: 10.0, end: 500.0).animate(_animationController);
    //END NEW By Luthfan Alwafi
    
    _animationController.forward();

    

  }

  @override
  void dispose() {
    super.dispose();
    _animationController.reset();
  }

  navigator(home) {
    _next();
  }

  @override
  Widget build(BuildContext context) {
    if (_isPushNext) {
      _runfor == AnimatedSplashType.BackgroundProcess
          ? Future.delayed(Duration.zero).then((value) {
              var res = _customFunction();
              //print("$res+${_outputAndHome[res]}");
              Future.delayed(Duration(milliseconds: _duration)).then((value) {
                Navigator.of(context).pushReplacement(CupertinoPageRoute(
                    builder: (BuildContext context) => _outputAndHome[res]));
              });
            })
          : Future.delayed(Duration(milliseconds: _duration)).then((value) {
              _next();
            });
    }

    return Scaffold(
        backgroundColor: Colors.white,
        body: FadeTransition(
            opacity: _animation,
            child: Center(
              child: Image.asset(_imagePath),
            )));

    //START NEW By Luthfan Alwafi
    // return Scaffold(
    //   backgroundColor: _backgroundColor == null ? Colors.white : HexColor(_backgroundColor),
    //   body: AnimatedBuilder(
    //     animation: _animation,
    //     builder: (context, widget) {
    //       return Container(
    //         // decoration: BoxDecoration(
    //         //   image: DecorationImage(
    //         //     image: AssetImage(
    //         //       _imagePath),
    //         //     fit: BoxFit.cover,
    //         //   ),
    //         // ),
    //         alignment: Alignment.center,
    //         child: _imagePath != ''
    //             ? Image.asset(
    //                 _imagePath,
    //                 height: _animation.value,
    //               )
    //             : Container(),
    //       );
    //     },
    //   ),
    // );
    //END NEW By Luthfan Alwafi

  }
}
