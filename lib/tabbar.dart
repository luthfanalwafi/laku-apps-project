import 'dart:async';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart' as firebase_auth;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fstore/screens/orders/orders.dart';
import 'package:launch_review/launch_review.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

import 'common/config.dart';
import 'common/constants.dart';
import 'common/tools.dart';
import 'generated/l10n.dart';
import 'menu.dart';
import 'models/app_model.dart';
import 'models/cart/cart_model.dart';
import 'models/search_model.dart';
import 'route.dart';
import 'screens/base.dart';
import 'screens/index.dart'
    show
        CartScreen,
        PostScreen,
        CategoriesScreen,
        WishListScreen,
        HomeScreen,
        NotificationScreen,
        StaticSite,
        UserScreen,
        WebViewScreen;
import 'screens/pages/index.dart';
import 'services/index.dart';
import 'widgets/blog/slider_list.dart';
import 'widgets/common/auto_hide_keyboard.dart';
import 'widgets/icons/feather.dart';
import 'widgets/layout/adaptive.dart';
import 'widgets/layout/main_layout.dart';

const int tabCount = 3;
const int turnsToRotateRight = 1;
const int turnsToRotateLeft = 3;

//START NEW by Luthfan Alwafi
NotificationAppLaunchDetails notificationAppLaunchDetails;
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();
final BehaviorSubject<ReceivedNotification> didReceiveLocalNotificationSubject =
    BehaviorSubject<ReceivedNotification>();

final BehaviorSubject<String> selectNotificationSubject =
    BehaviorSubject<String>();
//END NEW by Luthfan Alwafi

class MainTabControlDelegate {
  int index;
  Function(String nameTab) changeTab;
  Function(int index) tabAnimateTo;

  static MainTabControlDelegate _instance;

  static MainTabControlDelegate getInstance() {
    return _instance ??= MainTabControlDelegate._();
  }

  MainTabControlDelegate._();
}

class MainTabs extends StatefulWidget {
  MainTabs({Key key}) : super(key: key);

  @override
  MainTabsState createState() => MainTabsState();
}

class MainTabsState extends BaseScreen<MainTabs>
    with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  final _auth = FirebaseAuth.instance;
  final GlobalKey<ScaffoldState> _scaffoldKey =
      GlobalKey(debugLabel: 'Dashboard');
  final List<Widget> _tabView = [];
  final navigators = Map<int, GlobalKey<NavigatorState>>();

  var tabData;

  Map saveIndexTab = Map();

  firebase_auth.User loggedInUser;

  TabController tabController;

  bool isAdmin = false;
  bool isFirstLoad = false;
  bool isShowCustomDrawer = false;

  StreamSubscription _subOpenNativeDrawer;
  StreamSubscription _subCloseNativeDrawer;
  StreamSubscription _subOpenCustomDrawer;
  StreamSubscription _subCloseCustomDrawer;

  bool get isDesktopDisplay => isDisplayDesktop(context);

  @override
  void afterFirstLayout(BuildContext context) {
    loadTabBar(context);
  }

  Widget tabView(Map<String, dynamic> data) {
    switch (data['layout']) {
      case 'category':
        return CategoriesScreen(
          key: const Key("category"),
          layout: data['categoryLayout'],
          categories: data['categories'],
          images: data['images'],
          showChat: data['showChat'],
          showSearch: data['showSearch'] ?? true,
        );
      case 'search':
        {
          return AutoHideKeyboard(
            child: ChangeNotifierProvider<SearchModel>(
              create: (context) => SearchModel(),
              child: Services().widget.renderSearchScreen(
                    context,
                    showChat: data['showChat'],
                  ),
            ),
          );
        }

      //START NEW by Luthfan Alwafi
      case 'order':
        return MyOrders();
      case 'notif':
        return NotificationScreen();
      //END NEW by Luthfan Alwafi

      case 'cart':
        return CartScreen(showChat: data['showChat']);
      case 'profile':
        return UserScreen(
            settings: data['settings'],
            background: data['background'],
            showChat: data['showChat']);
      case 'blog':
        return HorizontalSliderList(config: data);
      case 'wishlist':
        return WishListScreen(canPop: false, showChat: data['showChat']);
      case 'page':
        return WebViewScreen(
            title: data['title'], url: data['url'], showChat: data['showChat']);
      case 'html':
        return StaticSite(data: data['data'], showChat: data['showChat']);
      case 'static':
        return StaticPage(data: data['data'], showChat: data['showChat']);
      case 'postScreen':
        return PostScreen(
            pageId: data['pageId'],
            pageTitle: data['pageTitle'],
            isLocatedInTabbar: true,
            showChat: data['showChat']);

      /// Story Screen
      // case 'story':
      //   return StoryWidget(
      //     config: data,
      //     isFullScreen: true,
      //     onTapStoryText: (cfg) {
      //       Utils.onTapNavigateOptions(context: context, config: cfg);
      //     },
      //   );

      /// vendor screens
      case 'vendors':
        return Services().widget.renderVendorCategoriesScreen(data);

      case 'map':
        return Services().widget.renderMapScreen();

      /// Default Screen
      case 'dynamic':
      default:
        return const HomeScreen();
    }
  }

  void changeTab(String nameTab) {
    if (saveIndexTab[nameTab] != null) {
      tabController?.animateTo(saveIndexTab[nameTab]);
    } else {
      Navigator.of(context, rootNavigator: true).pushNamed("/$nameTab");
    }
  }

  void loadTabBar(context) {
    tabData = List.from(
        Provider.of<AppModel>(context, listen: false).appConfig['TabBar']);

    for (var i = 0; i < tabData.length; i++) {
      Map<String, dynamic> _dataOfTab = Map.from(tabData[i]);
      saveIndexTab[_dataOfTab['layout']] = i;
      navigators[i] = GlobalKey<NavigatorState>();
      _tabView.add(Navigator(
        key: navigators[i],
        onGenerateRoute: (RouteSettings settings) {
          if (settings.name == Navigator.defaultRouteName) {
            return MaterialPageRoute(
              builder: (context) => tabView(_dataOfTab),
              fullscreenDialog: true,
              settings: settings,
            );
          }
          return Routes.getRouteGenerate(settings);
        },
      ));
    }

    setState(() {
      tabController = TabController(length: _tabView.length, vsync: this);
    });

    if (MainTabControlDelegate.getInstance().index != null) {
      tabController.animateTo(MainTabControlDelegate.getInstance().index);
    } else {
      MainTabControlDelegate.getInstance().index = 0;
    }

    // Load the Design from FluxBuilder
    tabController.addListener(() {
      eventBus.fire('tab_${tabController.index}');
      MainTabControlDelegate.getInstance().index = tabController.index;
    });
  }

  Future<void> getCurrentUser() async {
    try {
      //Provider.of<UserModel>(context).getUser();
      final user = await _auth.currentUser;
      if (user != null) {
        setState(() {
          loggedInUser = user;
        });
      }
    } catch (e) {
      printLog("[tabbar] getCurrentUser error ${e.toString()}");
    }
  }

  bool checkIsAdmin() {
    if (loggedInUser.email == adminEmail) {
      isAdmin = true;
    } else {
      isAdmin = false;
    }
    return isAdmin;
  }

  @override
  void initState() {
    printLog("[Dashboard] init");
    if (!kIsWeb) {
      getCurrentUser();
    }
    setupListenEvent();
    MainTabControlDelegate.getInstance().changeTab = changeTab;
    MainTabControlDelegate.getInstance().tabAnimateTo = (int index) {
      tabController?.animateTo(index);
    };
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    //START NEW by Luthfan Alwafi
    initNotif();
    _requestIOSPermissions();
    _configureSelectNotificationSubject();
    _configureDidReceiveLocalNotificationSubject();
    //END NEW by Luthfan Alwafi
  }

  @override
  void didChangeDependencies() {
    isShowCustomDrawer = isDesktopDisplay;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    tabController?.dispose();
    WidgetsBinding.instance.removeObserver(this);
    _subOpenNativeDrawer?.cancel();
    _subCloseNativeDrawer?.cancel();
    _subOpenCustomDrawer?.cancel();
    _subCloseCustomDrawer?.cancel();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      // went to Background
    }
    if (state == AppLifecycleState.resumed) {
      // came back to Foreground
      final appModel = Provider.of<AppModel>(context, listen: false);
      if (appModel.deeplink?.isNotEmpty ?? false) {
        if (appModel.deeplink['screen'] == 'NotificationScreen') {
          appModel.deeplink = null;
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => NotificationScreen()),
          );
        }
      }
    }

    super.didChangeAppLifecycleState(state);
  }

  void setupListenEvent() {
    _subOpenNativeDrawer = eventBus.on<EventOpenNativeDrawer>().listen((event) {
      if (!_scaffoldKey.currentState.isDrawerOpen) {
        _scaffoldKey.currentState.openDrawer();
      }
    });
    _subCloseNativeDrawer =
        eventBus.on<EventCloseNativeDrawer>().listen((event) {
      if (_scaffoldKey.currentState.isDrawerOpen) {
        _scaffoldKey.currentState.openEndDrawer();
      }
    });
    _subOpenCustomDrawer = eventBus.on<EventOpenCustomDrawer>().listen((event) {
      setState(() {
        isShowCustomDrawer = true;
      });
    });
    _subCloseCustomDrawer =
        eventBus.on<EventCloseCustomDrawer>().listen((event) {
      setState(() {
        isShowCustomDrawer = false;
      });
    });
  }

  Future<bool> handleWillPopScopeRoot() {
    // Check pop navigator current tab
    final currentNavigator = navigators[tabController.index];
    if (currentNavigator.currentState.canPop()) {
      currentNavigator.currentState.pop();
      return Future.value(false);
    }
    // Check pop root navigator
    if (Navigator.of(context).canPop()) {
      Navigator.of(context).pop();
      return Future.value(false);
    }
    if (tabController.index != 0) {
      tabController.animateTo(0);
      return Future.value(false);
    } else {
      return showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Text(S.of(context).areYouSure),
              content: Text(S.of(context).doYouWantToExitApp),
              actions: <Widget>[
                FlatButton(
                  onPressed: () => Navigator.of(context).pop(false),
                  child: Text(S.of(context).no),
                ),
                FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: Text(S.of(context).yes),
                ),
              ],
            ),
          ) ??
          false;
    }
  }

  //START NEW by Luthfan Alwafi
  void initNotif() async {
    notificationAppLaunchDetails =
        await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();

    var initializationSettingsAndroid =
        const AndroidInitializationSettings('@mipmap/ic_launcher');
    // Note: permissions aren't requested here just to demonstrate that can be done later using the `requestPermissions()` method
    // of the `IOSFlutterLocalNotificationsPlugin` class
    var initializationSettingsIOS = IOSInitializationSettings(
        requestAlertPermission: false,
        requestBadgePermission: false,
        requestSoundPermission: false,
        onDidReceiveLocalNotification:
            (int id, String title, String body, String payload) async {
          didReceiveLocalNotificationSubject.add(ReceivedNotification(
              id: id, title: title, body: body, payload: payload));
        });
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (String payload) async {
      if (payload != null) {
        debugPrint('notification payload: ' + payload);
      }
      selectNotificationSubject.add(payload);
    });
  }

  void _requestIOSPermissions() {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );
  }

  void _configureDidReceiveLocalNotificationSubject() {
    didReceiveLocalNotificationSubject.stream
        .listen((ReceivedNotification receivedNotification) async {
      await showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: receivedNotification.title != null
              ? Text(receivedNotification.title)
              : null,
          content: receivedNotification.body != null
              ? Text(receivedNotification.body)
              : null,
          actions: [
            CupertinoDialogAction(
              isDefaultAction: true,
              child: const Text('Ok'),
              onPressed: () async {
                // final user = await _auth.currentUser();
                final user = await _auth.currentUser;
                if (user != null) {
                  Navigator.of(context, rootNavigator: true).pop();
                  await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MyOrders(),
                    ),
                  );
                }
              },
            )
          ],
        ),
      );
    });
  }

  void _configureSelectNotificationSubject() async {
    // final user = await _auth.currentUser();
    final user = await _auth.currentUser;
    if (user != null) {
      selectNotificationSubject.stream.listen((String payload) async {
        await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => MyOrders(),
          ),
        );
      });
    }
  }

  updateApps(BuildContext context) async {
    await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        final config = Provider.of<AppModel>(context).appConfig;
        return AlertDialog(
          title: Text(S.of(context).updateApp),
          content: Text(config['AppsVersion']['message']),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 15.0),
              child: FlatButton(
                child: Text(S.of(context).update),
                onPressed: () {
                  LaunchReview.launch(
                    writeReview: false,
                    androidAppId: 'com.laku.kainprinting',
                    iOSAppId: '1460654977',
                  );
                },
              ),
            ),
          ],
        );
      },
    );
  }
  //END NEW by Luthfan Alwafi

  @override
  Widget build(BuildContext context) {
    printLog('[tabbar] ============== tabbar.dart DASHBOARD ==============');
    printLog(
        '[Resolution Screen]: ${MediaQuery.of(context).size.width} x ${MediaQuery.of(context).size.height}');
    Utils.setStatusBarWhiteForeground(false);

    if (_tabView.isEmpty) {
      return Container(
        color: Colors.white,
      );
    }

    return renderBody(context);
  }

  Widget renderBody(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    // final appSetting = Provider.of<AppModel>(context).appConfig['Setting'];

    //START NEW by Luthfan Alwafi
    final config = Provider.of<AppModel>(context).appConfig;
    final appSetting = config['Setting'];
    String updateFrom = config['AppsVersion'] != null
        ? config['AppsVersion']['updateFrom']
        : '';
    var verList = [kcurrentlyVer, updateFrom];
    verList.sort((a, b) => a.compareTo(b));
    int newIndex = verList.indexWhere((f) => f == updateFrom);
    int currentIndex = verList.indexWhere((f) => f == kcurrentlyVer);
    //END NEW by Luthfan ALwafi

    final colorTabbar = appSetting['ColorTabbar'] != null
        ? HexColor(appSetting['ColorTabbar'])
        : false;

    final tabBar = TabBar(
      controller: tabController,
      onTap: (index) {
        setState(() {});
      },
      tabs: renderTabbar(),
      isScrollable: false,
      labelColor:
          colorTabbar == false ? Theme.of(context).primaryColor : Colors.white,
      indicatorSize: TabBarIndicatorSize.label,
      indicatorPadding: const EdgeInsets.all(4.0),
      indicatorColor:
          colorTabbar == false ? Theme.of(context).primaryColor : Colors.white,
    );

    //START NEW By Luthfan Alwafi
    Widget renderDrawer() {
      if (config['HorizonLayout'][0]['showMenu'] != null) {
        if (config['HorizonLayout'][0]['showMenu']) {
          if (isDesktopDisplay) {
            return null;
          } else {
            return Drawer(child: MenuBar());
          }
        } else {
          return null;
        }
      } else {
        return null;
      }
    }

    if (updateFrom == kcurrentlyVer || currentIndex < newIndex) {
      Future.delayed(Duration.zero, () => updateApps(context));
    }
    //END NEW By Luthfan Alwafi

    // TabBarView
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Theme.of(context).backgroundColor,
      resizeToAvoidBottomPadding: false,
      body: WillPopScope(
        onWillPop: handleWillPopScopeRoot,
        child: MainLayout(
          menu: MenuBar(),
          content: ListenableProvider.value(
            value: tabController,
            child: Consumer(
              builder: (context, TabController controller, child) {
                // return Stack(
                //   fit: StackFit.expand,
                //   children: List.generate(
                //     _tabView.length,
                //     (index) {
                //       final active = controller.index == index;
                //       return Offstage(
                //         offstage: !active,
                //         child: TickerMode(
                //           child: _tabView[index],
                //           enabled: active,
                //         ),
                //       );
                //     },
                //   ).toList(),
                // );

                // START NEW by Luthfan Alwafi
                return TabBarView(
                  controller: tabController,
                  physics: const NeverScrollableScrollPhysics(),
                  children: List.generate(
                    _tabView.length,
                    (index) {
                      final active = controller.index == index;
                      return Offstage(
                        offstage: !active,
                        child: TickerMode(
                          child: _tabView[index],
                          enabled: active,
                        ),
                      );
                    },
                  ).toList(),
                );
                //END NEW by Luthfan Alwafi
                
              },
            ),
          ),
        ),
      ),

      // drawer: isDesktopDisplay ? null : Drawer(child: MenuBar()),

      //START NEW by Luthfan Alwafi
      drawer: renderDrawer(),
      //END NEW by Luthfan Alwafi

      bottomNavigationBar: Container(
        color: colorTabbar == false
            ? Theme.of(context).backgroundColor
            : colorTabbar,
        child: SafeArea(
          top: false,
          child: AnimatedContainer(
            duration: const Duration(milliseconds: 600),
            curve: Curves.easeInOutQuint,
            height: isShowCustomDrawer ? 0 : null,
            constraints: const BoxConstraints(
              maxHeight: 50,
            ),
            decoration: BoxDecoration(
              border: const Border(
                top: BorderSide(color: Colors.black12, width: 0.5),
              ),

              //STARRT NEW by Luthfan Alwafi
              color: Theme.of(context).backgroundColor,
              boxShadow: [
                const BoxShadow(
                  color: Colors.grey,
                  offset: Offset(0.0, -2.0), //(x,y)
                  blurRadius: 3.0,
                ),
              ],
              //END NEW by Luthfan Alwafi
            ),
            width: screenSize.width,
            child: !isBigScreen(context)
                ? SizedBox(
                    child: tabBar,
                    width: MediaQuery.of(context).size.width,
                  )
                : FittedBox(
                    child: Container(
                      width: screenSize.width /
                          (1.8 / (screenSize.height / screenSize.width)),
                      child: tabBar,
                    ),
                  ),
          ),
        ),
      ),
    );
  }

  List<Widget> renderTabbar() {
    final isTablet = Tools.isTablet(MediaQuery.of(context));
    var totalCart = Provider.of<CartModel>(context).totalCartQuantity;
    final tabData = Provider.of<AppModel>(context, listen: false)
        .appConfig['TabBar'] as List;

    final appSetting = Provider.of<AppModel>(context).appConfig['Setting'];
    final colorIcon = appSetting['TabBarIconColor'] != null
        ? HexColor(appSetting['TabBarIconColor'])
        : Theme.of(context).accentColor;

    final activeColorIcon = appSetting['ActiveTabBarIconColor'] != null
        ? HexColor(appSetting['ActiveTabBarIconColor'])
        : Theme.of(context).primaryColor;

    List<Widget> list = [];

    int index = 0;

    tabData.forEach((item) {
      final isActive = tabController.index == index;
      var icon = !item["icon"].contains('/')
          ? Icon(
              featherIcons[item["icon"]],
              color: isActive ? activeColorIcon : colorIcon,
              size: 22,
            )
          : (item["icon"].contains('http')
              ? Image.network(
                  item["icon"],
                  color: isActive ? activeColorIcon : colorIcon,
                  width: 24,
                )
              : Image.asset(
                  item["icon"],
                  color: isActive ? activeColorIcon : colorIcon,
                  width: 24,
                ));

      if (item["layout"] == "cart") {
        icon = Stack(
          children: <Widget>[
            Container(
              width: 30,
              height: 25,
              padding: const EdgeInsets.only(right: 6.0, top: 4),
              child: icon,
            ),
            if (totalCart > 0)
              Positioned(
                right: 0,
                top: 0,
                child: Container(
                  padding: const EdgeInsets.all(1),
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  constraints: const BoxConstraints(
                    minWidth: 16,
                    minHeight: 16,
                  ),
                  child: Text(
                    totalCart.toString(),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: isTablet ? 14 : 12,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              )
          ],
        );
      }

      if (item["label"] != null) {
        list.add(Tab(
          icon: icon,
          iconMargin: EdgeInsets.zero,
          text: item["label"],
        ));
      } else {
        list.add(Tab(icon: icon));
      }
      index++;
    });

    return list;
  }
}

//START NEW by Luthfan Alwafi
class ReceivedNotification {
  final int id;
  final String title;
  final String body;
  final String payload;

  ReceivedNotification({
    @required this.id,
    @required this.title,
    @required this.body,
    @required this.payload,
  });
}
//END NEW by Luthfan Alwafi
